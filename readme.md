# LeadingAge Flagship Website: www.leadingage.org

CMS: Drupal 7
Theme: Custom
Developer of Record: NJI Media

## Branching Workflow
There are 4 branches: master, develop, env/dev, env/prd

**Environment Branches**
* `env/prd` This branch is meant to be configured to auto-deploy to production. The branch should ALWAYS be identical to the live production state.
* `env/dev` This branch is meant to be configured to auto-deploy to dev. The branch should never not be identical to the dev site state.

**Working Branches**
* `master` This is the only branch that should ever merge into an environment branch.
* `develop` This branch should be used for all development work in progess.

**Workflow**
All developers should pull the develop branch. Work should be done locally
on a feature branch, then merged to develop and push up. Develop branch should
merge to master branch or pull request should be issued.
Master branch can be deployed by merging into env/dev or env/prd.

## Installation
**PHP dependency management**
Use [composer](https://getcomposer.org/) to manage dependencies
including Drupal Core and contributed modules. Because composer
manages these files, they should not be committed to version 
control. The deploy process installs these files at build time.
Custom theme and modules are not publicly hosted, and thus are
committed to version control. Those paths are protected and will
not be overwritten by `composer install`.

**Drupal Settings**
Settings are environment-specific. Use the phpdotenv library to
configure settings for various environments, including local
development. Rename .env.example to .env to use. Note that Drupal
default settings.php file is included in version control, because
it has special configuration to take advantage of environment
variables.

## Development Tools
BrowserSync is configured for theme development. Theme uses Sass
for styles. Gulp watches and compiles. To install, from the project
root, run `npm install`. To start BrowserSync and begin watching
and running tasks, run `npm run gulp`.