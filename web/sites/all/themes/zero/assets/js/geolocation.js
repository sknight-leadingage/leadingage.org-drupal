(function ($) {  
$( window ).load(function() {

    var x = document.getElementById("currentlocation");
    if (x != null) {
      if (window.navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(writeCoordinates);
        x.innerHTML = "<p>" + document.getElementById('UserLatitude').value + " " + document.getElementById('UserLongitude').value + "</p>";
        if((typeof document.getElementById('GeoEnabledBrowser') !== 'undefined') && (document.getElementById('GeoEnabledBrowser') !== null))
        document.getElementById('GeoEnabledBrowser').value = "true";
      }
      else {
        x.innerHTML = "<p>Geolocation is not supported by this browser.</p>";
        document.getElementById('GeoEnabledBrowser').value = "false";
      }
    }
    initialize()
});

function writeCoordinates(position) {
  var longCoord = position.coords.longitude;
  var latCoord = position.coords.latitude;
  // var longCoordField = document.getElementById('UserLongitude');
  // var latCoordField = document.getElementById('UserLatitude');
  var longCoordField = document.getElementById('UserLongitude');
  var latCoordField = document.getElementById('UserLatitude');
  longCoordField.value = longCoord;
  latCoordField.value = latCoord;
}

  function onChangeLocation() {
    var x = document.getElementById("currentlocation");
    if (x != null) {
      x.innerHTML = "<p>" + document.getElementById('UserLatitude').value + " " + document.getElementById('UserLongitude').value + "</p>";
    }
  }
    //retrieve lat/long from search fields
    //based on: https://developers.google.com/maps/documentation/javascript/geocoding
    var geocoder;

    function initialize() {
          console.log("initialize");
        geocoder = new google.maps.Geocoder();
    }
$('#edit-field-shipping-state-value').on('change', function() {
codeAddress();

});
$('#edit-p-code').on('change', function() {
codeAddress();
});
$('#edit-city').on('change', function() {
codeAddress();
});


    function codeAddress() {
        var address = formatAddress();
        if (address != "") {
            document.getElementById('GeoSearch').value = "true";
            //retrieve geocode results asynchronously before page is submitted
            geocoder.geocode({ 'address': address }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    //use first result, output to console, and update hidden input tags
                    var latlng = results[0].geometry.location;
                    var longCoord1 = latlng.lng();
                    var latCoord1 = latlng.lat();

                    console.log("Longitude Equals (search)  " + longCoord1);
                    console.log("Latitude Equals (search)  " + latCoord1);

                    var longCoordField1 = document.getElementById('SearchedLongitude');
                    var latCoordField1 = document.getElementById('SearchedLatitude');

                    longCoordField1.value = longCoord1;
                    latCoordField1.value = latCoord1;
                    document.getElementById('edit-lat').value = latCoord1;
                    document.getElementById('edit-lon').value = longCoord1;


                } else {
                    console.log("Geocode was not successful for the following reason: " + status);
                    document.getElementById('GeoSearch').value = "false";
                }
            });
        }
        else {
            document.getElementById('GeoSearch').value = "false";
        }
    }

    function formatAddress() {
                console.log("formatAddress");
        var address = "";
        var zipField = document.getElementById('edit-p-code').value;
        var cityField = document.getElementById('edit-city').value;
        var stateField = document.getElementById('#edit-field-shipping-state-value');
        var stateSelected = $('#edit-field-shipping-state-value').children("option:selected").val();
console.log(stateSelected);
        //build the search string
        if (zipField != "") {
            address = zipField + ", US";
        }
        else {
            if (cityField != "") {
                address = cityField;
                if (stateSelected != "") {
                    address = address + ", " + stateSelected + ", US";
                }
                else {
                    address = address + ", US";
                }
            }
            //State-wide searches will be alphabetical only
            //else {
            //    if (stateSelected != "") {
            //        address = stateSelected + ", US";
            //    }
            //}
        } 
        console.log("Formatted search address: " + address + "//" + zipField + "/" + cityField + "/" + stateSelected + "/");
        return address;
    }


}(jQuery));
