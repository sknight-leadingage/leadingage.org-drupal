jQuery(function($){
						   
	// Homepage Slideshow
	if (jQuery('#slideshow').length > 0) {
		jQuery('#slideshow').slidertron({
			viewerSelector:			'.viewer',
			reelSelector:			'.viewer .reel',
			slidesSelector:			'.viewer .reel .slide',
			linkSelector:			'.link',
			navPreviousSelector:	'.previous',
			navNextSelector:		'.next',
			speed:					'slow'
		});
	}

});
