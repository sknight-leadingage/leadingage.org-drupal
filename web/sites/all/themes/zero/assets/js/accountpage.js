$(function() {
  /*  map at account page - Directory Services Search - alternate to find member*/
  google.maps.event.addDomListener(window, 'load', function(){
    var myLatlng = new google.maps.LatLng($('#lat-for-map').val(),$('#lon-for-map').val());
    var mapOptions = {
      zoom: 9,
      center: myLatlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map($('#map-new')[0], mapOptions);
    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: $('#name-for-map').val(),
      animation: google.maps.Animation.DROP,
    });
  });

  // $('#findmember-print').click(function(e){e.preventDefault();
  //   window.print();
  // });
  // $('#findmember-Back').click(function(e){e.preventDefault();
  // window.history.back();
  // });
  
});