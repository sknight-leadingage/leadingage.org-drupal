"use strict";

/**
 * @doc driver
 * @name Nav
 * @description
 *   Nav DOM effects / interactions.
 */
var Nav = (function (mainMenuData, $, _) {

  // Surface all jQuery selectors (maybe?)
  var $primaryNav = $("#nav"),
      $context = $('body'),
      $secondaryNav = $("#secondary-nav"),
      $logo = $('#brand'),
      $searchForm= $('#search-block-form'),
      $mobile = false,
      $searchToggle = $('.nav.-social.group'),
      tpls = {
        secondary: $("#secondary-nav-template").html(),
        tertiary: $("#tertiary-nav-template").html()
      };

  // Convert menu to mobile version
  function resizeSite(){
    // $('.-on, .-alt, .-show, .-exploring').removeClass('-on').removeClass('-show').removeClass('-exploring').removeClass('-alt');
    if(mobile()){
      if(!$mobile){
        $('body').addClass('mobile');
        $mobile=true;
      }
    }else{
      if($mobile){
        $('body').removeClass('mobile');
        $mobile=false;
      }
    }
  }

  function mobile(){
    if($(window).width()<1180){
      return true;
    }
  }
  resizeSite();
  $(window).resize(resizeSite);

  /**
   * Makes clones have equal height.
   */

  var cloneHeights = function(){
    var cloneParent=$('.clone.-parent');
    cloneParent.each(function(){
      var minHeight=0;
      var clones = $(this).find('.clone.-height');
      clones.each(function(){
        var cloneHeight = $(this).css('min-height', 0).outerHeight();
        if(cloneHeight> minHeight){
          minHeight = cloneHeight;
        }
      });

      clones.each(function(){
        $(this).css('min-height', minHeight);
      });
    });
  }
  if($('.clone.-height').length > 1){
    cloneHeights();
    $(window).resize(cloneHeights);
  }

  /**
   * Mobile Menu Opening
   */

  $('.item-list, ul.facetapi-facetapi-links, ul.facetapi-facetapi-links li.leaf').click(function(){
    $(this).toggleClass('activated');
  });
  $('.item-list, ul.facetapi-facetapi-links, ul.facetapi-facetapi-links li.leaf').mouseleave(function(){
    $(this).removeClass('activated');
  });

  /**
   * Open or close the secondary menu using a specific class.
   *
   * @param [obj] $nav
   *   jQuery navigation object to show or hide.
   * @param [bool] show
   *   True to show / false to hide.
   * @param [string] cssClass
   *   A class to add and remove that will show and hide the nav.
   */
  function toggleSecondary ($nav, show, cssClass) {
    var _class = cssClass || "-show";
    if (show) {
      $nav.addClass(_class);
    } else {
      $nav.removeClass(_class);
    }
    return;
  }

  /**
   * Toggle a nav item to explore. Or turn off all exploring items.
   * @param  [obj] $nav
   *   jQuery navigation object.
   * @param  [string] target
   *   Class for element to find in menu to remove explore.
   * @param  [obj] $this
   *   jQuery object to explore.
   * @param  [bool] toggleOn
   *   Should we explore $this.
   * @param  [string] cssClass
   *   CSS class to use.
   */
  function toggleExploreItem ($nav, target, $this, toggleOn, cssClass) {
    var _class = cssClass || "-exploring";
    $nav.find(target).removeClass(_class);
    if (toggleOn !== false || $this !== null) {
      $this.addClass(_class);
    }
    return;
  }

  /**
   * Nav Driver (Desktop)
   *
   * Create a Nav instance using mainMenuData object and templates.
   * @see
   *   html.tpl.php for mainMenuData
   */
  function driver (fn) {

    // Primary Navigation Effects.
    $primaryNav.on("click", ".nav__item a", function (e) {
      
      var $this = $(this);
      if($this.parent().parent().hasClass('group')){
        e.preventDefault();
        e.stopPropagation();
        if(mobile() && $this.parent().children('#secondary-nav').length<1){
          $this.parent().append($secondaryNav);
        }else if(mobile()){
          $secondaryNav.detach();
          $this.parent().removeClass('-exploring');
        }
      }
    })
    .on("click", ".nav__item a", function (e) {
      var $this = $(this).parent(),
          $a = $this.find("a"),
          data = $a.data();
      // If secondary is not open, open it.
      toggleSecondary($secondaryNav, true, "-show");
      // Toggle which nav item is being explored.
      toggleExploreItem($primaryNav, ".nav__item", $this);

      if(mobile()){
      }else if($primaryNav.find('#secondary-nav').length>0){
        $('header').append($secondaryNav);
      }

      // Get menu HTML and place in the DOM.
      var secondaryMenuHtml = fn.getTemplatedMenu(tpls.secondary, fn.getMenuData(mainMenuData, data.ref));
      $("#secondary-nav-list").html(secondaryMenuHtml);
      $("#tertiary-nav-lists").html("<ul class='span4'><li class='nav__item -aside f'><a>Select an item on the left.</a></li></ul>");
    });

    // Seconadry Navigation Effects.
    $secondaryNav.on("mouseenter", ".nav__item.-below", function (e) {
      var $this = $(this),
          $a = $this.find("a"),
          data = $a.data(),
          tertiaryMenuHtml = fn.getTemplatedMenu(tpls.tertiary, fn.getSplitData(fn.getMenuData(mainMenuData, data.ref), 3));

      toggleExploreItem($secondaryNav, ".nav__item.-below", $this);
      if (tertiaryMenuHtml === false) {
        $("#tertiary-nav-lists").html("<ul class='span4'><li class='nav__item -aside f'><a>Select an item on the left.</a></li></ul>");
      } else {
        $("#tertiary-nav-lists").html(tertiaryMenuHtml);
      }
    });

    var closeAll = function (e) {
      if ($secondaryNav.hasClass("-show")) {
        toggleSecondary($secondaryNav, false);
        toggleExploreItem($primaryNav, ".nav__item", null, false);
      }
      return;
    };

    $secondaryNav.mouseleave(closeAll);
    $context.click(closeAll);
    $('#hamburger').click(function(){
      if($context.hasClass('-alt')){
        $context.removeClass('-alt');
      }else{
        $context.toggleClass('-on');
      }
    });

    $searchToggle.click(function(){
      if(mobile){
        $context.addClass('-alt');
      }
    });

    $('.mobile.-plus').click(function(){
      $(this).parents('.nav__item').toggleClass('-open')
    });
  }

  return driver;

})(mainMenuData, jQuery, require("underscore"));

//Exports the page module for app.js to use
module.exports = Nav;
