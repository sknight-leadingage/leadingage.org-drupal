"use strict";
/**
 * @doc fn
 * @name Nav
 * @description
 *   Yes, the structure of the menu object is weird. Sue me.
 *   {key: {above: {..links}, below: {...links}}}
 */
var Nav = (function (_) {
  var Public = {},
      currentMenu = {};

  /**
   * Get menu markup to attach to a dom element.
   * @param  [string] tpl
   *   The template.
   * @param  [obj] data
   *   Data object in the correct for for the given template.
   *
   * @return [string]
   *   Menu markup.
   */
  Public.getTemplatedMenu = function(tpl, data) {
    if (typeof data === "undefined" || data === false) {
      return false;
    }
    return _.template(tpl)({data: data});
  };

  /**
   * Get menu data from the given menu object.
   */
  Public.getMenuData = function (data, key, set) {
    /**
     * Check if its a secondary menu key. If not check if it is a key in the
     * "below" object of the current menu.
     *
     * If getting a secondary menu set it to be the currentMenu. Unless explicitly
     * called with set === false
     */
    if (typeof key === "string" && typeof data[key] !== "undefined") {
      if (typeof set === "undefined" || set) {
        // Asure a below.
        data[key].below = data[key].below || {};
        currentMenu = data[key];
      }
      return data[key].above;
    } else if (typeof key === "string" && typeof currentMenu.below[key] !== "undefined") {
      return currentMenu.below[key];
    }
    return false;
  };

  /**
   * Split data into parts evenly given a max amount of columns.
   */
  Public.getSplitData = function (data, maxSplit) {
    var count = data.length,
      columnCount = (count > maxSplit) ? Math.ceil(count/maxSplit) : count,
      columns = [],
      itemCount = 0;
    for (var x = 0; x < maxSplit; x += 1) {
      columns[x] = [];
      for (var y = 0; y < columnCount; y += 1) {
        if (typeof data[itemCount] !== "undefined") {
          columns[x][y] = data[itemCount];
          itemCount += 1;
        }
      }
    }
    return columns;
  };
  return Public;
})(require("underscore"));

//Exports the page module for app.js to use
module.exports = Nav;
