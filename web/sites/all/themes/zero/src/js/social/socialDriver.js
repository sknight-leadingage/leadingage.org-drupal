"use strict";

/**
 * @doc driver
 * @name Social
 * @description
 *   Social DOM effects / interactions.
 */
var Social = (function ($, _) {
  if($('.social').length>0){
    window.fbAsyncInit = function() {
      FB.init({
        appId      : '1749289452022950',
        xfbml      : true,
        version    : 'v2.6'
      });
    };

    (function(d, s, id){
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {return;}
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    var windowHeight=260;
    var windowWidth=800;
    //popup box style
    var label, current, leftPosition, topPosition;
    //Allow for borders.
    leftPosition = (window.screen.width / 2) - ((windowWidth / 2) + 10);
    //Allow for title and status bars.
    topPosition = (window.screen.height / 2) - ((windowHeight / 2) + 50);
    var windowFeatures = "status=no,height=" + windowHeight + ",width=" + windowWidth + ",resizable=yes,left=" + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY=" + topPosition + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no";

    var regex=/<p>/gi;
    var regexTwo=/(<\/p>|<br \/>)/gi;
    var filterTags = function(text){
      text = encodeURIComponent(text.replace(regex, '').replace(regexTwo, '\n'));
      return text;
    };

    //facebook sharing
    $('.social a[class*="facebook"]').click(function(){
      var pageInfo = $(this).data("title");
      var pageLink = $(this).data('url');
      var imageLink= $(this).data('image');
      var pageDescription = $(this).data('text');
      FB.ui({
        method: 'feed',
        name: pageInfo,
        description: pageDescription,
        link: pageLink,
        picture:imageLink
      });
    });

    //twitter sharing
    $('.social a[class*="twitter"]').click(function(){
      var tweetMsg = filterTags($(this).data("title"));
      var twitterLink = "https://twitter.com/share?text=" + tweetMsg + "&via=LeadingAge";
      window.open((twitterLink),'Twitter', windowFeatures);
    });
  }

  var testfunction = function(){

  };
  return testfunction;
})(jQuery, require("underscore"));

//Exports the page module for app.js to use
module.exports = Social;