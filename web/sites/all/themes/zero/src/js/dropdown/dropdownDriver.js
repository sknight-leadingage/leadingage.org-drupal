"use strict";

/**
 * @doc driver
 * @name Dropdown
 * @description
 *   Dropdown DOM effects / interactions.
 */
var Dropdown = (function ($, _) {

  var $dropdownHover = $(".dropdown.-hover");

  /**
   * Dropdown Driver (Desktop)
   *
   * Create a Dropdown instance using mainMenuData object and templates.
   * @see
   *   html.tpl.php for mainMenuData
   */
   function mobile(){
     if($(window).width()<1180){
       return true;
     }
   }
  function driver (fn) {
    // Set href of parent on li hover
    $dropdownHover.on("mouseenter", "ul li", function (e) {
      var $this = $(this),
          data = $this.data();
      $this.closest(".btn").attr("href", data.target);
    });

    $dropdownHover.children('a').click(function(e){
      if (mobile() && !($(this).hasClass('opened'))){
        e.preventDefault();
        $(this).toggleClass('opened');
      }
    });
  }



  return driver;

})(jQuery, require("underscore"));

//Exports the page module for app.js to use
module.exports = Dropdown;
