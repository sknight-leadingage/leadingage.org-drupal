"use strict";

/**
 * @doc fn
 * @name Dropdown
 * @description
 *   Dropdown functions.
 */
var Dropdown = (function (_) {

  var Public = {};
  return Public;

})(require("underscore"));

//Exports the page module for app.js to use
module.exports = Dropdown;
