"use strict";

var _ = require("underscore");
var drivers = {
      nav: require('./nav/navDriver'),
      dropdown: require('./dropdown/dropdownDriver'),
      social: require('./social/socialDriver')
    };
var fns = {
      nav: require('./nav/navFn'),
      dropdown: require('./dropdown/dropdownFn'),
      social: require('./social/socialFn')
    };

_.each(drivers, function (driver, key) {
  driver(fns[key]);
});
