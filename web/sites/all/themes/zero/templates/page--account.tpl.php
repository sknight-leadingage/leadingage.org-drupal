
<!-- <link  rel="stylesheet" type="text/css" href="https://hweb.leadingage.org/css/directoryprint.css" /> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
 <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyC_tY7zB65hqHw4lHJoWAPG9x6hPyTI2uk"></script>
<script type="text/javascript">var addthis_config = { "data_track_addressbar": true };</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4dd40dda04964093"></script>
<style type="text/css">
	.rslides {
  position: relative;
  list-style: none;
  overflow: hidden;
  width: 100%;
  padding: 0;
  margin: 0;
  }

.rslides li {
  -webkit-backface-visibility: hidden;
  position: absolute;
  display: none;
  width: 100%;
  left: 0;
  top: 0;
  }

.rslides li:first-child {
  position: relative;
  display: block;
  float: left;
  }

.rslides img {
  display: block;
  height: auto;
  float: left;
  width: 100%;
  border: 0;
  }
  .directory-detailed-left h5 {
    color: #55601c;
}
.print-photo{
	display: none;
}
@media print {
	.rslides_nav.rslides1_nav{
		background: none;
	}
	.findmember-pagination{
		display: none;
	}
	body{
		font-size: 10px;
	}
	.bot-search-directory, .directory-detailed-right{
		border: 1px solid black;
	}
	.slider-new{
		display: none;
	}
	.print-photo{
	display: block;
	    width: 100%;
	    /*height: 2px;*/
}
.top-search-directory{
	display: flex;
	border: 1px solid black;
	height: 300px;
}
.directory-detailed li, .directory-detailed p, .directory-detailed h5, .directory-address li, .directory-address .phone-num {
    font-size: 12px;
}
.small-link, .site__footer, .phone-img{
	display: none;
}
}
</style>
<!-- AddThis Button END -->   
<!--     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript" src="https://hweb.leadingage.org/js/asd/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="https://hweb.leadingage.org/js/asd/jquery.slidertron-0.2.1.js"></script>
<script type="text/javascript" src="https://hweb.leadingage.org/js/asd/init.js"></script> -->
<?php
	print $messages;
	if ( $tabs && ! empty( $tabs['#primary'] ) ) : ?>
	<div class='tabs'><?php print render( $tabs ); ?></div>
	<?php
	endif;
	// print render( $page['help'] );
?>
<div id="main" class="clearfix container row--main">
	<div class="section">
		<article class='account-search-directory'>
		<!-- <header class="section -marked">
		<h1>Aging Services Directory</h1>

		<div style="padding: 0 0 24px 0;">Part of: <a href="/planning-and-paying">Planning and Paying</a></div>
		</header> -->
			<h5>Not-For-Profit Provider of Aging Services</h5>
			<input type="hidden" id="lat-for-map" name="latiId" value="<?php print $fields['field_latitude']; ?>">
		  	<input type="hidden" id="lon-for-map" name="longiId" value="<?php print $fields['field_longitude']; ?>">
		  	<input type="hidden" id="name-for-map" name="nameId" value="<?php print $fields['field_member_name']; ?>">

		<!-- 		<span id="lat-for-map"><?php print $fields['field_latitude']; ?></span>
				<span id="lon-for-map"><?php print $fields['field_longitude']; ?></span>
				<span id="name-for-map"><?php print $fields['field_member_name']; ?></span> -->
		<div class="findmember-pagination">
		    <div class="paging">
		        <ul>
		            <li><a id="findmember-Back-to-results" href="javascript:void(0);">Back to Search Results</a></li>
		            <li><a id="findmember-back-to-searchpage" href="javascript:void(0);">Modify Your Results</a></li>  
		             <li><a id="dirsearch-new-search" href="/sample1">New Search</a></li>
		            <li><a id="dirsearch-findmember-print" href="#">Print Page</a></li>
		        </ul>
		    </div>
		</div>



			<div class='top-search-directory'>
		<div class="top-search-directory top-search-directory-left">
		<img src="https://my.leadingage.org/Custom/Company/GetDirectoryImage.ashx?id=<?php print $fields['field_leadingage_id_c']; ?>_01" alt="" class="print-photo">
		<div class="slider-new">
		<ul class="rslides">
		  <li><img src="https://my.leadingage.org/Custom/Company/GetDirectoryImage.ashx?id=<?php print $fields['field_leadingage_id_c']; ?>_01" alt=""></li>
		  <li><img src="https://my.leadingage.org/Custom/Company/GetDirectoryImage.ashx?id=<?php print $fields['field_leadingage_id_c']; ?>_02" alt=""></li>
		  <li><img src="https://my.leadingage.org/Custom/Company/GetDirectoryImage.ashx?id=<?php print $fields['field_leadingage_id_c']; ?>_03" alt=""></li>
		  <li><img src="https://my.leadingage.org/Custom/Company/GetDirectoryImage.ashx?id=<?php print $fields['field_leadingage_id_c']; ?>_04" alt=""></li>
		</ul>
		</div>

		<!-- 				<div id="slideshow" class="ctnr" style="position: relative">
							<div class="viewer">
								<div class="reel">
		                            <div class='slide'><div style='width:510px; height:254px; background-position: center center; background-repeat: no-repeat; overflow: hidden; background-size: cover; background-image: url(https://my.leadingage.org/Custom/Company/GetDirectoryImage.ashx?id=<?php print $fields['field_leadingage_id_c']; ?>_01);' /></div></div>
		                            <div class='slide'><div style='width:510px; height:254px; background-position: center center; background-repeat: no-repeat; overflow: hidden; background-size: cover; background-image: url("https://my.leadingage.org/Custom/Company/GetDirectoryImage.ashx?id=<?php print $fields['field_leadingage_id_c']; ?>_02");' /></div></div>
		                            <div class='slide'><div style='width:510px; height:254px; background-position: center center; background-repeat: no-repeat; overflow: hidden; background-size: cover; background-image: url("https://my.leadingage.org/Custom/Company/GetDirectoryImage.ashx?id=<?php print $fields['field_leadingage_id_c']; ?>_03");' /></div></div>
		                            <div class='slide'><div style='width:510px; height:254px; background-position: center center; background-repeat: no-repeat; overflow: hidden; background-size: cover; background-image: url("https://my.leadingage.org/Custom/Company/GetDirectoryImage.ashx?id=<?php print $fields['field_leadingage_id_c']; ?>_04");' /></div></div>
								</div>
							</div>
							<a href="#" class="previous"><img src="/sites/all/themes/zero/assets/img/left_arrow.png"></a> 
							<a href="#" class="next"><img src="/sites/all/themes/zero/assets/img/right_arrow.png"></a>
						</div> -->
					</div>
						<div class="top-search-directory top-search-directory-right">
						<div class="frame-map">
						<div id="map-new" style="height: 100%; width: 100%"></div></div>
		<!-- 				<?php echo "<iframe class='frame-map' frameborder='0' style='border:0' src='https://maps.google.com/maps?q=".$fields['field_latitude'].",".$fields['field_longitude']."&output=embed'>
						</iframe>";	?>	 -->
						</div>			
		</div>
		<div class="bot-search-directory">
			<div class='bot-left-search-directory search-directory directory-primaryinfo' style="clear: both;">
				<h2 class="field-header"><?php print render($fields['field_member_name']); ?></h2>

				<ul class='directory-address' >
				<?php if($fields['field_shipping_street']!= NULL) { ?>
					<li><?php print render($fields['field_shipping_street']); ?></li> <?php } ?>
					<li><?php if($fields['field_shipping_city']!= NULL) { print render($fields['field_shipping_city']); ?>, <?php } if($fields['field_shipping_state']!= NULL) { print render($fields['field_shipping_state']); ?>, <?php } print render($fields['field_shipping_postalcode']); ?></li>
		<!-- 		<?php if($fields['field_shipping_country']!= NULL) { ?>			
					<li><?php print render($fields['field_shipping_country']); ?></li> <?php } ?> -->
				<?php if($fields['field_phonenumber']!= NULL) { ?>			
					<li><div class="findmember-phone"><img src="/sites/all/themes/zero/assets/img/phone-grey.png" class="phone-img"><p class="phone-num"><?php print render($fields['field_phonenumber']); ?></div></li> <?php } ?></p>
					<li class="small-link"><?php
					if($fields['field_facebookaccount_c'] != NULL){
					echo "<a href='https://".$fields['field_facebookaccount_c']."'><img src='/sites/all/themes/zero/assets/img/Facebook.png'></a>";}
					if($fields['field_twitteraccount_c'] != NULL){
					echo "<a href='https://".$fields['field_twitteraccount_c']."'><img src='/sites/all/themes/zero/assets/img/twitter.png'></a>";}
					if($fields['field_linkedinaccount_c'] != NULL){
					echo "<a href='https://".$fields['field_linkedinaccount_c']."'><img src='/sites/all/themes/zero/assets/img/LinkedIn.png'></a>"; }  ?></li>
				</ul>
			</div>
				<div class="bot-right-search-directory directory-more-info search-directory">
				<div class="get-direction"><?php
				$addr = $fields['field_shipping_street'].', '.$fields['field_shipping_city'].', '.$fields['field_shipping_country'].', '.$fields['field_shipping_postalcode'];
				echo " <a href='https://maps.google.com/maps?saddr=Current+Location&daddr=".$addr."' class='directory-directions'>Get Directions</a>"; ?></div>
				<?php if (!($fields['field_website'] == NULL)):
					if(!(substr( $fields['field_website'], 0, 4 ) == "http")):
						$fields['field_website'] = "http://".$fields['field_website'];
					endif;
				echo "<div class='get-direction'><a href='".$fields['field_website']."'class='directory-website' target='_blank'>Visit Website</a></div>"; 
				endif; ?>
				<?php if (!($fields['field_directory_email'] == NULL)):
				echo "<div class='get-direction'><a class='directory-email' href='mailto:".$fields['field_directory_email']."'>Request Information</a></div>";				
				endif; ?>

			 <div class="get-direction">
			 <a class="addthis_button_compact directory-share"><span>Share</span></a>
			 </div>

				</div>
			</div>
			<div class="directory-detailed">
				<div class="directory-detailed-left">

					<?php 
					if($fields['field_directory_headline_c']!= NULL): ?>	
						<h5><?php print render($fields['field_directory_headline_c']); ?></h5> <?php 
					endif; 
					if($fields['field_directory_subheadline_c']!= NULL): ?>		
						<h5><?php print render($fields['field_directory_subheadline_c']);?></h5> <?php 
					endif; 
					if($fields['field_directory_copy_c']!= NULL ): 
						print render($fields['field_directory_copy_c']); 
					endif; 
				if($fields['field_directory_services_offered'] != NULL): ?>
					<h5>Services Offered</h5>
					<ul>
						<?php 
						$services_list = explode(";", $fields['field_directory_services_offered']);
						foreach ($services_list as $key => $value) { ?> <li> <?php 	print ($value); ?> </li> <?php }  ?>
					</ul> <?php 
				endif; ?>

				<?php if($fields['field_directory_features'] != NULL) {?>
				<h5>Features Include</h5>
				<?php print render($fields['field_directory_features']); } ?>
				</div>
				<div class="directory-quotes directory-detailed-right">
						<?php if (!($fields['field_directory_quote_c'] == NULL)):
						echo "<span class='find-directory-quote'>".$fields['field_directory_quote_c']."</span>";				
						endif; ?>
						<?php if (!($fields['field_directory_quote_name_c'] == NULL)):
						echo "<p class='find-directory-quote-name'>—".$fields['field_directory_quote_name_c']."</p>";		
						endif; ?>
				</div>	
		</div>
		</div>
		</article>
	</div>
</div>
	<?php 
 // $distance_meters = 100;
	// //get the distance 
	// $lat = 37;
	// $lon = -122;	

 /*	$query->add_where_expression(0, "((ACOS(SIN($lat  PI() / 180)  SIN(field_data_field_latitude.field_latitude_value  PI() / 180) + COS($lat  PI() / 180)  COS(field_data_field_latitude.field_latitude_value  PI() / 180)  COS(($lon - field_data_field_longitude.field_longitude_value)  PI() / 180))  180 / PI())  60 * 1.1515) < ".$distance_meters);
*/
// $field_latitude = $fields['field_latitude'];

// $field_longitude = $fields['field_longitude'];

//  print $formula = 3956 * 2 * ASIN(SQRT(pow(SIN(($lat - abs($field_latitude))*pi()/180/2),2) + COS($lat * pi()/180) * COS(abs($field_latitude) * pi()/180) * pow(SIN(($lon - $field_longitude) * pi()/180/2),2)));

// print"<br>";

	// if($fields['field_latitude'] != NULL && $fields['field_longitude'] != NULL) {
// echo "<iframe frameborder='0' style='border:0' src='https://www.google.com/maps/embed/v1/place ?key=AIzaSyC_tY7zB65hqHw4lHJoWAPG9x6hPyTI2uk &location=".$fields['field_latitude'].",".$fields['field_longitude']."&heading=210 &pitch=10 &fov=35'></iframe><div id='map-canvas'></div>";	}
  ?>

<!-- https://www.google.com/maps/embed/v1/streetview
  ?key=YOUR_API_KEY
  &location=46.414382,10.013988
  &heading=210
  &pitch=10
  &fov=35 -->
<!-- https://www.google.com/maps/embed/v1/view ?key=YOUR_API_KEY &center=-33.8569,151.2152 &zoom=18 &maptype=satellite
https://www.google.com/maps/embed/v1/search?key=YOUR_API_KEY&q=record+stores+in+Seattle" -->
<!-- https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyC_tY7zB65hqHw4lHJoWAPG9x6hPyTI2uk -->

<!-- <iframe frameborder='0' style='border:0' src='https://www.google.com/maps/embed/v1/view ?key=AIzaSyC_tY7zB65hqHw4lHJoWAPG9x6hPyTI2uk &center=".$fields['field_latitude'].",".$fields['field_longitude']." &zoom=18 &maptype=satellite'>
				</iframe> -->
