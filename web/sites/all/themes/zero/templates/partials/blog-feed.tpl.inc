<?php
/**
 * @file
 * Filterable feed partial.
 */
global $base_url;
$filterableFeedHeader = $filterableFeedIntro = '';
if ( ! empty( $wrapper->value()->field_select_text['und'][0]['safe_value'] ) ){
	$selectText = $wrapper->value()->field_select_text['und'][0]['safe_value'];
}
if ( ! empty( $wrapper->value()->field_section_heading['und'][0]['safe_value'] ) ) {
	$header = $wrapper->value()->field_section_heading['und'][0]['safe_value'];
	$filterableFeedHeader = "<h2>$header</h2>";
}
if ( ! empty( $wrapper->value()->field_intro['und'][0]['safe_value'] ) ) {
	$intro = strip_tags( $wrapper->value()->field_intro['und'][0]['safe_value'] );
	$filterableFeedIntro = "<p class='intro'>$intro</p>";
}
if ( isset( $fields['field_is_blog'] ) && $fields['field_is_blog'] && ! empty( $fields['field_associated_blog'] ) ) :
	$term_id = $fields['field_associated_blog']->tid;
	$type = 'blog_post';
	$query = db_select( 'node', 'n' );
	$query->join( 'taxonomy_index', 'ti', 'ti.nid=n.nid' );
	$result = $query->fields( 'n', array( 'nid', 'type', 'created', 'status', 'title' ) )
		->fields( 'ti', array( 'nid', 'tid' ) )
		->condition( 'tid', $term_id )
		->condition( 'status', 1 )
		->condition( 'type', $type )
		->orderBy( 'created', 'DESC' )
		->execute();
	$results_array = array();
	foreach ( $result as $key => $row ) {
		array_push( $results_array, $row );
	}
	$most_recent_post = node_load( array_shift( $results_array )->nid );
	$result_count = count( $results_array );
	if ( isset( $fields['field_heading'] ) ) :
		?>
<h2 class='-section'><?php print $fields['field_heading']; ?></h2>
		<?php
	endif;
	?>
<a href='/<?php print drupal_get_path_alias( 'node/' . $most_recent_post->nid ); ?>' class='box -main -feature'>
	<header class='box__header'>
		<h2 class='box__heading'><span><?php print $most_recent_post->title; ?></span></h2>
	</header>
	<div class='box__body'>
	<?php if ( ! empty( $most_recent_post->field_intro['und'][0]['safe_value'] ) ) :?>
		<p class='box__text'><?php print $most_recent_post->field_intro['und'][0]['safe_value']; ?></p>
	<?php elseif ( ! empty( $most_recent_post->body['und'][0]['safe_value'] ) ) :?>
		<p class='box__text'><?php print trim_it( strip_tags( $most_recent_post->body['und'][0]['safe_value'] ) ); ?></p>
	<?php else : ?>
		<p class='box__text'>&nbsp;</p>
	<?php endif; ?>
	</div>
	<footer class='box__footer'>
		<span class='meta'>Read More</span>
	</footer>
</a>
<?php
endif;
include drupal_get_path( 'theme', 'zero' ) . '/templates/partials/collections/base-feed.tpl.inc';