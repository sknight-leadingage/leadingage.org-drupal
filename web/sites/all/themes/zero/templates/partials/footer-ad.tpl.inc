<?php
if ( ! empty( $footer_ads_fields ) && ! empty( $footer_ads_fields['field_ads'] ) ) :
	?>
<section class='site__prefooter'>
	<div class='container group'>
	<?php
	foreach ( $footer_ads_fields['field_ads'] as $key => $ad ) :
		if ( ! empty( $ad->field_image ) ) :
			?>
		<div class='ad-container-footer <?php ( $key === 0 ) ? print ' f' : print ' l'; ?>' style='background-color: #<?php print field_get_items( 'node', $ad, 'field_background_color' )[0]['jquery_colorpicker']; ?>; '>
			<a class='ad -footer<?php ( $key === 0 ) ? print ' f' : print ' l'; ?>' href='<?php print field_get_items( 'node', $ad, 'field_url' )[0]['value']; ?>'>
				<div class='ad-img-footer'>
					<img src='<?php print image_style_url( field_get_items( 'node', $ad, 'field_image' )[0]['uri'] ); ?>' alt='<?php print field_get_items( 'node', $ad, 'title' )[0]['value']; ?>' />
				</div>
				<p class='ad-text--footer'><?php print field_get_items( 'node', $ad, 'field_ad_text' )[0]['value']; ?></p>
			</a>
		</div>
			<?php
		endif;
	endforeach;
	?>
	</div>
</section>
	<?php
endif;
?>