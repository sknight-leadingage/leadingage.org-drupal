<?php
/**
 * @file
 * Menu item item.
 */
if ( empty( $link['nav_class'] ) ){
	$link['nav_class'] = '';
}

//BEGIN Hide menu with empty titles
if ( ! empty( $link['title'] ) ){

?>

<li class='nav__item <?php print $link['nav_class']; ?>'>
  <?php
      $linktarget = '';
      if ( stripos( $link['path'] , 'neworleanscvb.com/leadingage/' ) > 0 ) {
        $linktarget = 'target="_blank"';
      }
  ?>
	<a title="<?php print $link['title']; ?>" href="<?php print $link['path']; ?>"<?php print $linktarget; ?>><?php print $link['title']; ?></a>
<?php

if ( 2 == $link['depth'] && ! empty( $link['children'] ) ) {
	include drupal_get_path( 'theme', 'zero' ) . '/templates/partials/collections/sub-menu-collection.tpl.inc';
	print '<div class="mobile -plus"></div>';
}

} //END Hide menu with empty titles
?>
</li>