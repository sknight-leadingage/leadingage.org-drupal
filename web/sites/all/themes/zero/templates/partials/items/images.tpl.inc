<?php
if ( ! empty( $imageData->info['und'][0]['uri'] ) ) : // Shows image if there is one
	?>
<div class="image <?php print $imageData->classes; ?>" title="<?php print $imageData->info['und'][0]['title']; ?>" alt="<?php print $imageData->info['und'][0]['alt']; ?>" style="background-image: url( <?php print file_create_url( $imageData->info['und'][0]['uri'] ); ?> )"></div>
	<?php
endif;