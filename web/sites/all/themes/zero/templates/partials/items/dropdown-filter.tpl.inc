<nav class="dropdown -hover">
	<a href="#" class="btn -main">
		<span class="dropdown__selected"><?php print $filter_name; ?></span>
		<i class="arrow -down"></i>
		<ul class="list-group -main">
			<li data-target="<?php print Nil\Theme::getNewsroomFilterUrl( 'all', $filter['entity']->machine_name ).'#filter'; ?>">All</li>
	<?php

	foreach ( $terms as $index => $term ) :
		?>
			<li data-target="<?php print Nil\Theme::getNewsroomFilterUrl( $term->tid, $filter['entity']->machine_name ).'#filter'; ?>"><?php print $term->name; ?></li>
		<?php
	endforeach;
	?>
		</ul>
	</a>
</nav>