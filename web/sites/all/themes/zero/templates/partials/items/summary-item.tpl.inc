<?php
/**
 * @file
 * Summary item.
 */
$aStart = $aEnd = $img = '';
if ( ! empty( $summary['img'] ) ) {
	$img = '<img class="summary__image" alt="'.$summary['img-alt'].'" title="'.$summary['img-title'].'" src="'.$summary['img'].'"/>';
}
if ( ! empty( $summary['url'] ) ) {
	$aStart = "<a href='".$summary['url']."'>";
	$aEnd = '</a>';
}
if ( empty( $additionalClasses ) ) {
	$additionalClasses = '';
}
?>
<div class="summary <?php print $additionalClasses; ?>">
	<?php print $aStart; ?>
	<?php print $img; ?>
	<div class="summary__text">
		<h4 class="summary__heading"><span><?php print $summary['title']; ?></span></h4>
		<p><?php print $summary['info']; ?></p>
	</div>
	<?php print $aEnd; ?>
</div>