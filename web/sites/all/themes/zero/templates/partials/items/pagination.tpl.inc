<?php
if ( $result_count > $pagination_size ) :
	$url_append = '';
	if ( ! empty( $query_params ) ){
		foreach ( $query_params as $key => $value ) {
			if ( 'p' !== $key ){
				$url_append .= "&$key=$value";
			}
		}
	}
	$url_append .= '#filter';
	if ( 0 === $view_current_page ) {
		$prev = '';
	} else {
		$url = $base_url.'/'.drupal_get_path_alias( 'node/' . $wrapper->getIdentifier() ) . '?p=' . ( $view_current_page ).$url_append;
		$prev = '<li class="pagination__item f"><a href="'.$url.'" data-page="prev">Prev</a></li>';
	}
	if ( $lastPage ) {
		$next = '';
	} else {
		$url = $base_url.'/'.drupal_get_path_alias( 'node/' . $wrapper->getIdentifier() ) . '?p=' . ( $view_current_page + 2).$url_append;
		$next = '<li class="pagination__item l"><a href="'.$url.'" data-page="next">Next</a></li>';
	}
	$url_append = '#filter';
	$feed_pagination_items = get_pagination_items( $view_current_page, $result_count, $pagination_size );
	$feed_pagination_items_count = count( $feed_pagination_items );
	?>
<ul class="pagination">
	<?php
	print $prev;
	foreach ( $feed_pagination_items as $page => $label ) :
		// Make pagination URL.
		if ( 0 !== $page ) {
			$pagination_url = $base_url.'/'.drupal_get_path_alias( 'node/' . $wrapper->getIdentifier() );
		} else {
			$pagination_url = $base_url.'/'.drupal_get_path_alias( 'node/' . $wrapper->getIdentifier() );
		}
		$n = $page;
		$pagination_url .= '?';
		$page_number = $n + 1;
		$pagination_url .= 'p='.$page_number;
		if ( ! empty( $query_params ) ){
			foreach ( $query_params as $key => $value ) {
				if ( 'p' !== $key ){
					$pagination_url .= "&$key=$value";
				}
			}
		}
		$x = $view_current_page;
		$pagination_url .= $url_append;
		if ( $x > $feed_pagination_items_count - 3 && $n > $x - $pagination_size ) :
			if ( 3 + ( $feed_pagination_items_count - $x - 1 ) === $x - $n || 4 + ( $feed_pagination_items_count - $x - 1 ) === $x - $n ) :
				?>
	<li class='pagination__item
				<?php
				if ( $view_current_page === $page ) {
					print ' active';
				}
				?>'>
		<a href='<?php print $pagination_url; ?>' data-page='<?php print $page; ?>'><?php print $label; ?></a></li>
				<?php
			endif;
		endif;
		if ( 1 === $x - $n || 2 === $x - $n || 1 === $n - $x || 2 === $n - $x || $x === $n ) :
			?>
	<li class='pagination__item
			<?php
			if ( $view_current_page === $page ) {
				print ' active';
			}
			?>'>
		<a href='<?php print $pagination_url; ?>' data-page='<?php print $page; ?>'><?php print $label; ?></a>
	</li>
			<?php
		elseif ( $x < 2 ) :
			if ( 3 - $x === $n - $x || 4 - $x === $n - $x ) :
				?>
	<li class='pagination__item
				<?php
				if ( $page === $view_current_page ) {
					print ' active';
				} ?>'>
		<a href='<?php print $pagination_url; ?>' data-page='<?php print $page; ?>'><?php print $label; ?></a>
	</li>
				<?php
			endif;
		endif;
	endforeach;
	print $next;
	?>
</ul>
	<?php
endif;