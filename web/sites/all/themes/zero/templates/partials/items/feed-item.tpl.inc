<?php
global $user;

$header_class = $info = $intro = $href = $summary = '';
$node = node_load( $results_array[ $i ]->nid );
$href = '/'.drupal_get_path_alias( 'node/'.$node->nid );
$title = $node->title;

$requireMember = $userIsMember = false;
if (isset($user) && in_array('Member', $user->roles)) {
  $userIsMember = true;
}
if ( ! empty( field_get_items( 'node', $node, 'field_members_only' )[0]['value'] ) && field_get_items( 'node', $node, 'field_members_only' )[0]['value'] == '1' ) {
  $requireMember = true;
}

if ( ! empty( field_get_items( 'node', $node, 'body' )[0]['summary'] ) ){
  if (($requireMember === FALSE) || ($requireMember === TRUE && $userIsMember === TRUE)) {
    $summary = strip_tags( field_get_items( 'node', $node, 'body' )[0]['summary'] );
    if ( $node->created < 1480723200 ) { // Limits legacy content's summary length. Going forward we want to allow the client to write summaries of whatever length they prefer.
      $summary = trim_it( $summary );
    }
    $summary = '<p>'.$summary.'</p>';
  } else if ($requireMember === TRUE && $userIsMember === FALSE) {
    if ( ! empty( field_get_items( 'node', $node, 'field_intro' )[0]['value'] ) ) {
      $summary = '<p>'.strip_tags( trim_it( field_get_items( 'node', $node, 'field_intro' )[0]['value'] ) ).'</p>';
    } else {
      $summary = '<p><i>This item contains member-only content.</i></p>';
    }
  }
} elseif ( ! empty( field_get_items( 'node', $node, 'body' )[0]['value'] ) ) {
  if (($requireMember === FALSE) || ($requireMember === TRUE && $userIsMember === TRUE)) {
    $summary = '<p>'.strip_tags( trim_it( field_get_items( 'node', $node, 'body' )[0]['value'] ) ).'</p>';
  } else if ($requireMember === TRUE && $userIsMember === FALSE) {
    if ( ! empty( field_get_items( 'node', $node, 'field_intro' )[0]['value'] ) ) {
      $summary = '<p>'.strip_tags( trim_it( field_get_items( 'node', $node, 'field_intro' )[0]['value'] ) ).'</p>';
    } else {
      $summary = '<p><i>This item contains member-only content.</i></p>';
    }
  }
}
if ( 'event' === $node->type ) {
	$header_class = 'title';
	date_default_timezone_set( field_get_items( 'node', $node, 'field_date' )[0]['timezone'] );
	$date = date( 'F d, Y', field_get_items( 'node', $node, 'field_date' )[0]['value'] );
	$time = date( 'g:i A T', field_get_items( 'node', $node, 'field_date' )[0]['value'] );
	if ( ! empty( field_get_items( 'node', $node, 'field_location' )[0]['value'] ) ){
		$location = '<div class="line"><div class="image -location"></div>'.field_get_items( 'node', $node, 'field_location' )[0]['value'].'</div>';
	} else {
		$location = '';
	}
	$info = "<div class='info'><h6 class='label color -brand'><div class='line'><div class='image -date'></div>$date$location</div><div class='line'><div class='image -time'></div>$time</div></h6></div>";
	$title = $node->title;
	$summary = '<div class="introduction">'.$summary.'</div>';
}
?>
<li>
	<a class="<?php print $header_class; ?>" href="<?php print $href; ?>">
		<h4 class="summary__heading"><span><?php print $title; ?></span></h4>
	</a>
<?php print $info.$summary; ?>
</li>