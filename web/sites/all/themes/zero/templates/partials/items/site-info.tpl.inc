<?php
if ( ! empty( $site_info_fields['field_social_links'] ) ) :
	?>
<ul class='nav -social group'>
	<?php
	foreach ( $site_info_fields['field_social_links'] as $social_link_key => $social_link ) :
		?>
		<li class='nav__item'><a href='<?php print $social_link['field_url']; ?>' target='_blank'><i class='icon -<?php print strtolower( $social_link['field_heading'] ); ?>'><?php print ucwords( $social_link['field_heading'] ); ?></i></a></li>
		<?php
	endforeach;
	?>
</ul>
<?php
endif;