<?php
/**
 * @file
 * Content footer item.
 */
$relativeLink = $fields['field_buckets'][0]['field_url'];
?>
<footer class="box__footer">
	<a href="<?php print $relativeLink; ?>" class="meta">Read More</a>
</footer>