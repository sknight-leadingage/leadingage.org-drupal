<?php
/**
 * @file
 * Pager Item.
 */
global $base_url;
$link = $base_url.'/'.drupal_get_path_alias( 'node/'.$node->nid );
?>
<a href="<?php print $link; ?>" class="box -main">
	<div class="box__inner">
		<div class="box__body">
			<p class="box__text label"><?php print $node->title; ?></p>
		</div>
	</div>
	<footer class="box__footer">
		<span href="#" class="meta"><?php print "$direction $contentLabel"; ?></span>
	</footer>
</a>