<?php
/**
 * @file
 * Buckets partial.
 */
$doubleRows = $trippleRows = $quadRows = 0;

if ( 0 === $sr_wf_topline_count % 4 ) {
  $quadRows = $sr_wf_topline_count / 4;
} else if ( 0 === $sr_wf_topline_count % 3 ) {
	$trippleRows = $sr_wf_topline_count / 3;
} elseif ( $sr_wf_topline_count > 3 ) {
	$trippleRows = floor( $sr_wf_topline_count / 3 );
	if ( 1 == $sr_wf_topline_count - ( $trippleRows * 3 ) ) {
		$trippleRows--;
		$doubleRows = 2;
	} else {
		$doubleRows = 1;
	}
} else {
	$doubleRows = 1;
}

$doubleRowItems = $doubleRows * 2;
$trippleRowItems = $trippleRows * 3;
$quadRowItems = $quadRows * 4;

?>
<section id='featured-content' style='padding-top: 26px;'>
<?php if ( ! empty( $fields['type'] ) && 'home_page' !== $fields['type'] ) : ?>
	<div class='container -pre-buckets'>
	<?php if ( ! empty( $fields['field_wf_topline_blocks'] ) && ! empty( $fields['field_heading'] ) ) : ?>
		<h2 class='-section'><?php print $wrapper->field_heading->value(); ?></h2>
	<?php endif; ?>
	</div>
<?php endif;?>
	<div style="padding-top: 16px;">
		<div class='row--fwb container clone -parent'>
<?php
for ( $bucket_number = 0; $bucket_number < $sr_wf_topline_count; $bucket_number++ ) :
	$bucket = $wrapper->field_wf_topline_blocks->get( $bucket_number );
	$bucket_fields = $fields['field_wf_topline_blocks'][ $bucket_number ];
	$bucket_classes = ' clone -height ';
	if ( $doubleRowItems > 0 ) {
		$bucket_classes .= ' span6';
		$bucket_image_style = 'buckets_2_across';
		$bucket_image_default = '/'.drupal_get_path( 'theme', 'zero' ) . '/docs/assets/img/samples/box_image_halves.png';
		if ( 0 === $doubleRowItems % 2 ) {
			$bucket_classes .= ' f';
		} else {
			$bucket_classes .= ' l';
		}
		$doubleRowItems --;
	} elseif ( $trippleRowItems > 0 ) {
		$bucket_classes .= ' span4';
		$bucket_image_style = 'buckets_3_across';
		$bucket_image_default = '/'.drupal_get_path( 'theme', 'zero' ) . '/docs/assets/img/samples/box_image_thirds.png';
		if ( 0 === $trippleRowItems % 3 ) {
			$bucket_classes .= ' f';
		} elseif ( 0 === ( $trippleRowItems - 1 ) % 3 ) {
			$bucket_classes .= ' l';
		}
		$trippleRowItems--;
	} elseif ( $quadRowItems > 0 ) {
		$bucket_classes .= ' span3';
		$bucket_image_style = 'buckets_3_across';
		$bucket_image_default = '/'.drupal_get_path( 'theme', 'zero' ) . '/docs/assets/img/samples/box_image_thirds.png';
		if ( 0 === $quadRowItems % 4 ) {
			$bucket_classes .= ' f';
		} elseif ( 0 === ( $quadRowItems - 1 ) % 4 ) {
			$bucket_classes .= ' l';
		}
		$quadRowItems--;
	}
	if ( ! empty( $bucket_fields['field_url'] ) ) :
		// Find out if external and add target='_blank'.
		?>
			<a href='<?php print $bucket->field_url->value(); ?>' class='box -main<?php print $bucket_classes; ?>'>
		<?php
	else :
	?>
			<a href='<?php print '/' . drupal_get_path_alias( 'node/' . $bucket->field_feature->getIdentifier() ); ?>' class='box -main<?php print $bucket_classes; ?>'>
	<?php
	endif;
	?>
				<header class='box__header'><center>
	<?php
	if ( ! empty( $bucket_fields['field_image'] ) ) :
		print '<img src="' . image_style_url( $bucket_image_style, $bucket_fields['field_image']['uri'] ) . '" alt="Bucket" />';
	else :
		if ( isset( $bucket->field_feature->value()->field_image ) ) {
			$bucket_field_image_array = $bucket->field_feature->field_image->value();
			if ( isset( $bucket_field_image_array['uri'] ) ) {
				print '<img src="' . image_style_url( $bucket_image_style, $bucket_field_image_array['uri'] ) . '" alt="Bucket" />';
			} else {
				print '<img src="' . $bucket_image_default . '" alt="Bucket" />';
			}
		} else {
			print '<img src="' . $bucket_image_default . '" alt="Bucket" />';
		}
	endif;
	if ( ! empty( $bucket_fields['field_heading'] ) ) :
		?>
					<h2 class='box__heading'><span><?php print $bucket->field_heading->value(); ?></span></h2>
		<?php
	elseif ( ! empty( $bucket_fields['field_feature']->title ) ) :
		?>
					<h2 class='box__heading'><span><?php print $bucket->field_feature->title->value(); ?></span></h2>
		<?php
	endif;
	?>
				</center></header>
				<div class='box__body'>
	<?php
	if ( ! empty( $bucket_fields['field_intro'] ) ) : ?>
					<p class='box__text'><?php $thisBucketSummary = $bucket->field_intro->value(); if ( is_array($thisBucketSummary) ) { print $thisBucketSummary["value"]; } else { print $thisBucketSummary; } ?>
          </p>
	<?php elseif ( ! empty( $bucket->value()->field_feature ) ) :?>
					<p class='box__text'><?php print strip_tags( $bucket->field_feature->body->value()['summary'] ); ?></p>
	<?php endif; ?>
				</div>
			</a>
	<?php
endfor;
?>
		</div>
	</div>
</section>
