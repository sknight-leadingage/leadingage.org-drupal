<?php
/**
 * @file
 * Magazine Summary List partial.
 */
?>
<ul class='summary-list'>
<?php
foreach ( $article_nodes as $node_id => $article_node ) :
	$article_entity = new Nil\Entity( $article_node, 'magazine_article' );
	$article_wrapper = $article_entity->getWrapper();
	$article_fields = $article_entity->getFieldsArray();
	$authorMeta = '';
	if ( ! empty( $article_fields['field_author'] ) ){
		$authorMeta = '<h6 class="meta">By ';
		$i = 0;
		foreach ( $article_fields['field_author'] as $author ){
			if ( $i > 0 ){
				$authorMeta .= ', ';
			}
			$authorMeta .= $author->title;
			$i++;
		}
		foreach ( $article_fields['field_author_non_staff'] as $author ){
			if ( $i > 0 ){
				$authorMeta .= ', ';
			}
			$authorMeta .= $author;
			$i++;
		}
	}
	if ( empty ( $authorMeta ) && ! empty ( $article_fields['field_author_non_staff'] ) ) {
		$authorMeta = '<h6 class="meta">By ';
		$i = 0;
		foreach ( $article_fields['field_author_non_staff'] as $author ){
			if ( $i > 0 ){
				$authorMeta .= ', ';
			}
			$authorMeta .= $author;
			$i++;
		}
	}
	$srcset = image_style_url( 'large', $article_wrapper->field_image->value()['uri'] ).' 480w, '.image_style_url( 'medium', $article_wrapper->field_image->value()['uri'] ).' 220w'.image_style_url( 'summary_thumb', $article_wrapper->field_image->value()['uri'] ).' 148w'.image_style_url( 'thumbnail', $article_wrapper->field_image->value()['uri'] ).' 100w';
?>
	<li class='summary'>
		<a href='/<?php print drupal_get_path_alias( 'node/' . $article_wrapper->getIdentifier() ); ?>'>
	<?php if ( ! empty( $article_fields['field_image'] ) ) : ?>
			<img class='summary__image' sizes='( max-width: 1180px ) calc( 100vw - 420px ), 148px' src='<?php print image_style_url( 'summary_thumb', $article_wrapper->field_image->value()['uri'] ); ?>' srcset='<?php print $srcset; ?>' />
			<div class='summary__text'>
	<?php endif; ?>
				<h4 class='summary__heading'><span><?php print $article_wrapper->title->value(); ?></span></h4>
	<?php
	print $authorMeta.'</h6>';
	if ( ! empty( $article_fields['field_intro'] ) ) :
		print $article_wrapper->field_intro->value()['safe_value'];
	else :
		?>
				<p>&nbsp;</p>
		<?php
	endif;
	if ( ! empty( $article_fields['field_image'] ) ) :
		?>
			</div>
		<?php
	endif;
	?>
		</a>
	</li>
	<?php
endforeach;
?>
</ul>
