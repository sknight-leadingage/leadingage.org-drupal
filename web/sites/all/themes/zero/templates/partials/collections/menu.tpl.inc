<?php
/**
 * @file
 * Menu Collection.
 */
$secondary = false;
switch ( true ) {
	case $type === 'magazine_issue'|| $type === 'magazine_article':
		$expoMenu = array(
			'magazine'
		);
		$menu = 'magazine';
		break;
	case $type === 'special_event_page' || $type === 'special_event' || $type === 'special_event_grid_page':
		$menu = menu_get_active_trail()[1]['menu_name'];
		$mlid = menu_get_active_trail()[1]['mlid'];
		$expoMenu = array(
			$menu
		);
		$secondary = true;
		break;
	default:
		$expoMenu = array(
			'magazine'
		);
		$menu = 'magazine';
}
if ( $secondary ){
	$expoMenu = Nil\Theme::getSecondaryMenus( $expoMenu, $mlid );
} else {
	$expoMenu = Nil\Theme::getMenus( $expoMenu );
}
?>
<div class='event-menu section -marked'>
	<div class='container'>
		<ul class='nav'>
<?php
foreach ( $expoMenu[ $menu ]['links'] as $link ) :
	if ( empty( $link['depth'] ) ){
		$link['depth'] = 1;
	}
	if ( 1 == $link['depth'] || ( 2 == $link['depth'] ) ){
		$link['nav_class'] = '';
		require drupal_get_path( 'theme', 'zero' ) . '/templates/partials/items/menu-item.tpl.inc';
	}
endforeach;
?>
		</ul>
	</div>
</div>