<?php
/**
 * @file
 * Sidebar Advertizers Collection.
 */
if ( ! empty( $ads_fields['field_ads'] ) || ! empty( $article_ads_fields['field_ads'] ) ) {
	?>
<h2 class="-section -short">Advertisers</h2>
	<?php
	$articleAds = $ads = array();
	if ( ! empty( $article_ads_fields['field_ads'] ) ){
		$articleAds = $article_ads_fields['field_ads'];
	}
	$ads = $articleAds;
	$alreadyPrinted = array();
	foreach ( $ads as $ad ) {
    if (is_object($ad)) {
        if ( ! in_array( $ad->nid, $alreadyPrinted ) ) {
          array_push( $alreadyPrinted, $ad->nid );
          $nid = $ad->nid;
          $title = $ad->title;
          $ad_text = $ad->field_ad_text['und'][0]['safe_value'];
          $title_out = $ad_text ?: $title ?: '';
          $url = $ad->field_url['und'][0]['safe_value'];
          ?>
    <a href="<?php print $url; ?>" data-node="<?php print $nid ?>"><h5><?php print $title_out; ?></h5></a>
          <?php
        }
		}
	}
}
?>
