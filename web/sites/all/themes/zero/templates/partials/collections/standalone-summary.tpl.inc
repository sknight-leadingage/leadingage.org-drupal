<?php
/**
 * @file
 * Standalone summary collection.
 */
$relativeLink = $fields['field_buckets'][0]['field_url'];
$title = $fields['field_buckets'][0]['field_heading'];
$summary = $fields['field_buckets'][0]['field_intro'];
?>
<div class="summary">
	<a href="<?php print $relativeLink; ?>">
		<h4 class="summary__heading"><span><?php print $title; ?></span></h4>
		<p><?php print $summary; ?></p>
	</a>
</div>
