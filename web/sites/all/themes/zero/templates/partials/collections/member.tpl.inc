<?php
/**
 * @file
 * Member collection.
 */
$content = $wrapper->value();
?>
<article>
	<div class = 'bio section grid -two-columns -marked'>
<?php
if ( ! empty( $content->field_headshot ) ) :
	$classes = 'span-6 item';
	?>
		<div class="span-6 item image -headshot" title="<?php print $content->field_headshot['und'][0]['title']; ?>" alt="<?php print $content->field_headshot['und'][0]['alt']; ?>" style="background-repeat: no-repeat; background-size: contain; background-image: url( <?php print file_create_url( $content->field_headshot['und'][0]['uri'] ); ?> )">
		</div>
	<?php
else :
	$classes = '';
endif;
?>
		<div class="<?php print $classes; ?> info">
			<div class = 'member'>
				<h2><?php print $content->title; ?></h2>
				<p><large><?php print $content->field_job_title['und'][0]['safe_value']; ?></large></p>
			</div>
			<div class='contacts'>
<?php
if ( ! empty( $content->field_email['und'][0]['safe_value'] ) ) :
	?>
				<div class="data">
					<h6 class="label">Email:</h6>
          <p><a href="mailto:<?php print $content->field_email['und'][0]['safe_value']; ?>"><?php print $content->field_email['und'][0]['safe_value']; ?></a></p>
				</div>
	<?php
endif;
if ( ! empty( $content->field_phone['und'][0]['safe_value'] ) ) :
	?>
				<div class="data">
					<h6 class="label">Phone:</h6>
					<p class = 'color -brand'><?php print $content->field_phone['und'][0]['safe_value']; ?></p>
				</div>
	<?php
endif;
if ( ! empty( $content->field_twitter['und'][0]['safe_value'] ) || ! empty( $content->field_facebook['und'][0]['safe_value'] ) || ! empty( $content->field_linkedin['und'][0]['safe_value'] ) ) :
	?>
				<div class = 'connect data'>
					<h6 class="label">Connect</h6>
					<div class="grid -social">
	<?php
	if ( ! empty( $content->field_twitter['und'][0]['safe_value'] ) ) :
		?>
						<a href = 'http://twitter.com/<?php print $content->field_twitter['und'][0]['safe_value']; ?>' target="_blank" class = 'item social -twitter background -brand'></a>
		<?php
	endif;
	if ( ! empty( $content->field_facebook['und'][0]['safe_value'] ) ) :
		?>
						<a href = 'http://facebook.com/<?php print $content->field_facebook['und'][0]['safe_value']; ?>' target="_blank" class = 'item social -facebook background -brand'></a>
		<?php
	endif;
	if ( ! empty( $content->field_linkedin['und'][0]['safe_value'] ) ) :
		?>
						<a href = 'http://linkedin.com/<?php print $content->field_linkedin['und'][0]['safe_value']; ?>' target="_blank" class = 'item social -linkedin background -brand'></a>
		<?php
	endif;
	?>
				</div>
			</div>
	<?php
endif;
?>
			</div>
		</div>
	</div>
<?php
if ( ! empty( $content->body['und'][0]['safe_value'] ) ) :
	?>
	<div class="">
	<?php print $content->body['und'][0]['value']; ?>
	</div>
	<?php
endif;
?>
</article>