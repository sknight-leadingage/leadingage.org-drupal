<?php
/**
 * @file
 * Frontpage Donations
 */
    if ( !empty( $block->field_amount['und'][0]['value'] ) ) {
      $donationAmount = $block->field_amount['und'][0]['value'];
		}
		if ( !empty( $block->field_goal['und'][0]['value'] ) ) {
      $donationGoal = $block->field_goal['und'][0]['value'];
    }
    if ( !empty( $block->field_description_line_1['und'][0]['value'] ) ) {
      $donationsLine1 = $block->field_description_line_1['und'][0]['value'];
    }
    if ( !empty( $block->field_description_line_2['und'][0]['value'] ) ) {
      $donationsLine2 = $block->field_description_line_2['und'][0]['value'];
    }
    if ($donationAmount > 0 && $donationGoal > 0) {
      ?>
<style type="text/css">     
.progress-bg {
    margin: 0 auto;
    width: 65%;
    height: 78px;
    border-radius: 10px;
    text-align: left;
    border: 2px solid #bbbbbb;
    -moz-box-shadow:    inset 0 0 10px #ccc;
    -webkit-box-shadow: inset 0 0 10px #ccc;
    box-shadow:         inset 0 0 10px #ccc;
}

.progress-bar {
    height: 78px;
    border-radius: 10px;
    float: left;
    width: <?php print round(($donationAmount / $donationGoal) * 100, 1); ?>%;
    /* fallback */ 
    background-color: #1c314a; 

    /* Safari 4-5, Chrome 1-9 */ 
    background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#1c314a), to(#27425f)); 

    /* Safari 5.1, Chrome 10+ */ 
    background: -webkit-linear-gradient(top, #1c314a, #27425f); 

    /* Firefox 3.6+ */ 
    background: -moz-linear-gradient(top, #1c314a, #27425f); 

    /* IE 10 */ 
    background: -ms-linear-gradient(top, #1c314a, #27425f); 

    /* Opera 11.10+ */ 
    background: -o-linear-gradient(top, #1c314a, #27425f); 
}

.progress-bg h3.goal, .progress-bg h3.raised {
    font-size: 2em;
    font-weight: 600;
    line-height: 78px;
    margin: 0;
    padding: 0;
    text-align: center;
    display: inline;
}


.progress-bg h3.raised {
    color: #fff;
    margin: 14px 25px 0 50px;
    padding: 0 25px 0 0;
}

.progress-bg h3.goal {
    color: #000000;
    text-align: center;
}

.progress-bg h3.goal {
    padding: 0 0 0 24px;
    text-align: center;
}

.final-goal {
    font-size: 2em;
}

@media screen and (max-width: 520px) {
    .progress-bg h3.goal, .progress-bg h3.raised, .final-goal {
        font-size: 1.2em;
    }
}
</style>
      <?php
      
      if ( !empty( $donationsLine1 ) ) {
          print "<br /><center><h3>$donationsLine1</h3></center>";
      }
      
      print '<div class="progress-bg">';
      print '   <div class="progress-bar">';
      if (round(($donationAmount / $donationGoal) * 100, 1) >= 50) {
          print '     <h3 class="raised">$' . number_format($donationAmount, 0, '.', ',') . ' raised</h3>';
      }
      print '   </div>';
      if (round(($donationAmount / $donationGoal) * 100, 1) < 50) {
          print '     <h3 class="goal">$' . number_format($donationAmount, 0, '.', ',') . ' raised</h3>';
      }
      print '</div>';
      //print '<div style="width: 80%; text-align: right;"><h3 class="final-goal">Goal: $' . number_format($donationGoal, 0, '.', ',') .'</h3></div>';
      
      if ( !empty( $donationsLine1 ) ) {
          print "<center><h3>$donationsLine2</h3></center>";
      }
    }
?>