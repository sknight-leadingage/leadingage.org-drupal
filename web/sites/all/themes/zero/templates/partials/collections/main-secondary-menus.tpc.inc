<?php
if ( $main_menu || $secondary_menu ) :
	?>
		<div id="navigation">
			<div class="section">
	<?php
	print theme( 'links__system_main_menu', array(
		'links' => $main_menu,
		'attributes' => array(
			'id' => 'main-menu',
			'class' => array(
				'links',
				'inline',
				'clearfix',
			)
		),
		'heading' => t( 'Main menu' ) ) );
	print theme( 'links__system_secondary_menu', array( 'links' => $secondary_menu,
		'attributes' => array( 'id' => 'secondary-menu',
			'class' => array( 'links',
				'inline',
				'clearfix',
				)
			),
			'heading' => t( 'Secondary menu' )
		) );
	?>
			</div>
		</div> <!-- /.section, /#navigation -->
	<?php
endif;