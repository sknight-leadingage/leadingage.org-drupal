<?php
/**
 * @file
 * Pager Sidebar collection.
 */
// dpm( $wrapper );
// dpm( $content );
global $base_url;
switch ( $type ) {
	case 'magazine_article':
		$description = 'Explore This Issue';
		$contentLabel = 'article';
		break;
	default:
		break;
}
$currentIssue = $content->field_parent_issue['und'][0]['target_id'];
$currentArticle = $content->nid;
$result = db_query( 'SELECT * FROM {node} JOIN {field_data_field_parent_issue} ON node.nid = field_data_field_parent_issue.entity_id WHERE node.type = :type AND field_data_field_parent_issue.field_parent_issue_target_id = :currentIssue AND node.status = 1 ORDER BY node.created DESC', array( ':type' => $type, ':currentIssue' => $currentIssue ) );
$resultArray = array();
foreach ( $result as $row ) {
	array_push( $resultArray, $row );
}
$resultLength = count( $resultArray );
if ( $resultLength > 2 ) {
	$i = 0;
	foreach ( $resultArray as $element ) {
		if ( $currentArticle === $element->nid ) {
			break;
		}
		$i++;
	}
	if ( 0 === $i ) {
		$previousArticle = $resultArray[ $resultLength - 1 ]->nid;
		$nextArticle = $resultArray[ $i + 1 ]->nid;
	} elseif ( $resultLength - 1 === $i ) {
		$nextArticle = $resultArray[0]->nid;
		$previousArticle = $resultArray[ $i - 1 ]->nid;
	} else {
		$nextArticle = $resultArray[ $i + 1 ]->nid;
		$previousArticle = $resultArray[ $i - 1 ]->nid;
	}
} elseif ( 2 === $resultLength ) {
	$nextArticle = $resultArray[0]->nid;
	if ( $resultArray[0]->nid === $currentArticle ) {
		$nextArticle = $resultArray[1]->nid;
	}
}
if ( ! empty( $nextArticle ) ) :
	$direction = 'next ';
	$node = node_load( $nextArticle );
?>
<aside class="site__sidebar span3 f">
	<div class="section">
		<h2 class = ''><?php print $description; ?></h2>
	<?php
	if ( ! empty( $nextArticle ) ) :
		require drupal_get_path( 'theme', 'zero' ) . '/templates/partials/items/pager-item.tpl.inc';
	endif;
	if ( ! empty( $previousArticle ) ) :
		$direction = 'prev';
		$node = node_load( $previousArticle );
		require drupal_get_path( 'theme', 'zero' ) . '/templates/partials/items/pager-item.tpl.inc';
	endif;
	?>
	</div>
<?php
endif;

$childPage = false;
if ( isset( $content->field_article_ad_sidebar ) ) {
  $childPage = true;
}

if ( !$childPage && ! empty( $ads_fields ) || ( $childPage && ! empty( $content->field_article_ad_sidebar ) ) ) :
	require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/sidebar-ad.tpl.inc';
endif;
?>
</aside>

