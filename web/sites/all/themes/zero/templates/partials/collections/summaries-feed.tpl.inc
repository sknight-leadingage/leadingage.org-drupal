<?php
/**
 * @file
 * Summary Feed collection.
 */
//Convert all types of information into a formate that summary items accept
$summaries = array();
switch ( $type ) {
	case 'general_session':
		foreach ( $content->field_session_speakers['und'] as $speaker ) {
			$key = $speaker['value'];
			$speakerData = entity_load( 'field_collection_item', array( $key ) );
			$summary = array( 'img' => '', 'title' => '', 'info' => '', 'img-alt' => '', 'img-title' => '' );
			$summary['img'] = file_create_url( $speakerData[ $key ]->field_headshot['und'][0]['uri'] );
			$summary['img-alt'] = $speakerData[ $key ]->field_headshot['und'][0]['alt'];
			$summary['img-title'] = $speakerData[ $key ]->field_headshot['und'][0]['title'];
			$summary['title'] = $speakerData[ $key ]->field_heading['und'][0]['value'];
			$summary['info'] = $speakerData[ $key ]->field_text['und'][0]['value'];
			array_push( $summaries, $summary );
		}
		$summary['url'] = '';
		$additionalClasses = ' -speakers';
		break;
	default:
		# code...
		break;
}
//Print out all summaries:
foreach ( $summaries as $summary ) {
	require drupal_get_path( 'theme', 'zero' ) . '/templates/partials/items/summary-item.tpl.inc';
}
