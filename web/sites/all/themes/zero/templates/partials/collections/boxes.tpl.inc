<?php
/**
 * @file
 * Box collection.
 */
?>
<h2><?php print $wrapper->field_heading_2->value(); ?> </h2>
<?php
// Obviously don't leave like this.
// dpm( $wrapper );
?>
<div class='box-wrap -grid clone -parent'>
	<div class='row-flush'>
		<?php
		$i = 0;
		foreach ( $article_nodes as $node_id => $article_node ) :
			$article_entity = new Nil\Entity( $article_node, 'magazine_article' );
			$article_wrapper = $article_entity->getWrapper();
			$article_fields = $article_entity->getFieldsArray();
			if ( 2 === $i ) :
				?>
	</div>
	<div class='row-flush'>
				<?php
			endif;
			?>
		<a href='<?php print drupal_get_path_alias( 'node/' . $article_wrapper->getIdentifier() ); ?>' class='box -main span6
			<?php
			if ( 0 === $i || 2 === $i ) {
					print ' f';
			}
			$i++;
			?>'>
			<div class='box__inner clone -height'>
				<header class='box__header'>
					<h4 class='box__heading'><?php print $article_wrapper->title->value(); ?></h4>
				</header>
				<div class='box__body'>
					<?php if ( ! empty( $article_fields['field_intro'] ) ) : ?>
					<p class='box__text'><?php print $article_wrapper->field_intro->value()['safe_value']; ?></p>
					<?php elseif ( ! empty( $value->field_field_intro ) ) : ?>
					<p class='box__text'><?php print strip_tags( $value->field_field_intro[0]['rendered']['#markup'] ); ?></p>
					<?php else : ?>
					<p class='box__text'>&nbsp;</p>
					<?php endif; ?>
				</div>
				<footer class='box__footer'>
					<span class='meta'>Read More</span>
				</footer>
			</div>
		</a>
			<?php
		endforeach;
		?>
	</div>
</div>
<footer class='box-wrap-after'>
	<a class='meta' href='/newsroom'>+ View More Activitiy</a>
</footer>