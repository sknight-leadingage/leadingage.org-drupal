<?php
$pagination_size = 5;
$start = $view_current_page * $pagination_size + 1;
$lastPage = $result_count <= ( $view_current_page + 1 ) * $pagination_size;
if ( $result_count > 0 ) :
	?>
<ol class="feed section -marked -short" start="<?php print $start; ?>">
	<?php
	for ( $i = $view_current_page * $pagination_size ; $i < ( ($view_current_page + 1) * $pagination_size ) ; $i++ ) :
		if ( $i < $result_count ) :
			include drupal_get_path( 'theme', 'zero' ) . '/templates/partials/items/feed-item.tpl.inc';
		endif;
	endfor;
	?>
</ol>
	<?php
	include drupal_get_path( 'theme', 'zero' ) . '/templates/partials/items/pagination.tpl.inc';
endif;