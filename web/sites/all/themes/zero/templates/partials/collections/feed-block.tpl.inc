<?php
/**
 * @file
 * Custom Feed collection block.
 */
global $base_url;
$title = '<h2 class="-section">'.$block->field_heading['und'][0]['value'].'</h2>';
$img = '';
if ( ! empty( $block->field_image['und'][0]['uri'] ) ) {
	$img = '<img src="'.file_create_url( $block->field_image['und'][0]['uri'] ).'"/>';
}
$link = $block->field_url['und'][0]['value'];
$linkText = $block->field_label['und'][0]['value'];
$displayDate = false;
switch ( $block->field_feed_content['und'][0]['value'] ) {
	case 'blog_post':
		// $relations = $wrapper->value()->field_blog['und'];
		$target = 'target_id';
		$displayDate = false;
		$result = db_query( 'SELECT nid FROM {node} WHERE node.type = :type AND node.status = 1 ORDER BY node.created DESC', array( ':type' => $type ) );
		break;
	case 'newsroom_post':
		$type = 'newsroom_post';
		$type1 = 'center_post';
		$type2 = 'magazine_article';
		$type3 = 'blog_post';
		$displayDate = false;
		$result = db_query( 'SELECT nid FROM {node} LEFT JOIN {field_data_field_associated_blog} ON node.nid = field_data_field_associated_blog.entity_id WHERE (node.type = :type OR node.type = :type1 OR node.type = :type2 OR (node.type = :type3 AND field_data_field_associated_blog.field_associated_blog_tid = 51)) AND node.status = 1 ORDER BY node.created DESC', array( ':type' => $type, ':type1' => $type1, ':type2' => $type2, ':type3' => $type3 ) );
		break;
	case 'event':
		$type = 'event';
		$all = true;
		$currentDate = getdate()[0];
		$displayDate = true;
		$result = db_query( 'SELECT node.nid, node.type
                          FROM {node}
                          JOIN {field_data_field_date} ON node.nid = field_data_field_date.entity_id
                          LEFT JOIN {field_data_field_pinned} ON node.nid = field_data_field_pinned.entity_id
                          LEFT JOIN taxonomy_index
                          ON node.nid = taxonomy_index.nid
                          WHERE field_data_field_date.field_date_value > :currentDate AND node.type = :type AND node.status = 1 AND taxonomy_index.tid IN (:terms)
                          ORDER BY IFNULL(field_data_field_pinned.field_pinned_value, 0) DESC, field_data_field_date.field_date_value ASC',
              array( ':currentDate' => $currentDate, ':type' => $type, ':terms' => '228' ) );
		break;
	default:
		break;
}
?>
<div class="item">
<?php
print $title;
print $img;
$i = 0;
foreach ( $result as $row ) :
	$node = node_load( $row->nid );
	// dpm( $node );
	if ( $displayDate ) {
		$label = '<p class="label color -grey">'.date( 'F d, Y', $node->field_date['und'][0]['value'] ).'</p>';
	} else {
		$label = '<p class="label color -grey">'.implode( ' ', explode( '_', $node->type ) ).'</p>';
	}
	if ( ! empty( $node->field_intro ) ) {
		$text = '<p>'.strip_tags( $node->field_intro['und'][0]['value'] ).'</p>';
	} else {
		$text = '';
	}
	?>
	<div class="summary">
		<a href="<?php print drupal_get_path_alias( 'node/'.$node->nid ); ?>">
			<h4 class="summary__heading"><span><?php print $node->title; ?></span></h4>
	<?php
	print $label;
	print $text;
	?>
		</a>
	</div>
	<?php
	$i++;
	if ( $i > 3 ) {
		break;
	}
endforeach;
if ( ! empty( $link ) && ! empty( $linkText ) ) :
	?>
	<a href="<?php print $link; ?>" class="box -main">
		<footer class="box__footer">
			<span href="#" class="meta"><?php print $linkText; ?></span>
		</footer>
	</a>
	<?php
endif;
?>
</div>