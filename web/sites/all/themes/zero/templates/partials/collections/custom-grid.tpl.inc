<?php
/**
 * @file
 * Author's Feed collection.
 */
$headingOne = $block->field_heading['und'][0]['safe_value'];
$headingTwo = $block->field_heading_2['und'][0]['safe_value'];
$headingThree = $block->field_heading_3['und'][0]['safe_value'];
$text = $block->field_text['und'][0]['value'];
$img = file_create_url( $block->field_image['und'][0]['uri'] );
$labelOne = $block->field_label['und'][0]['safe_value'];
$labelTwo = $block->field_label_2['und'][0]['safe_value'];
$labelThree = $block->field_label_3['und'][0]['safe_value'];
$urlOne = $block->field_url['und'][0]['safe_value'];
$urlTwo = $block->field_url_2['und'][0]['safe_value'];
$urlThree = $block->field_url_3['und'][0]['safe_value'];
?>
<div class="section">
	<div class="box-wrap -grid -homepage">
		<div class="row-flush">
			<a href="<?php print $urlOne; ?>" class="box -main span4">
				<div class="box__inner faller">
					<header class="box__header">
						<h3 class="box__heading"><?php print $headingOne; ?></h3>
					</header>
					<div class="box__body">
						<p class="box__text"><?php print $text; ?></p>
					</div>
					<footer class="box__footer">
						<span class="meta"><?php print $labelOne; ?></span>
					</footer>
				</div>
			</a>
			<div class="box -main span4 image" style="background-image: url('<?php print $img;?>');">
				<div class="box__inner"></div>
			</div>
			<div class="rows -two clone -parent">
				<a href="<?php print $urlTwo; ?>" class="box -main span4">
					<div class="box__inner clone -height">
						<header class="box__header">
							<h3 class="box__heading"><?php print $headingTwo; ?></h3>
						</header>
						<footer class="box__footer">
							<span class="meta"><?php print $labelTwo; ?></span>
						</footer>
					</div>
				</a>
				<a href="<?php print $urlThree; ?>" class="box -main span4">
					<div class="box__inner clone -height l">
						<header class="box__header">
							<h3 class="box__heading"><?php print $headingThree; ?></h3>
						</header>
						<footer class="box__footer">
							<span class="meta"><?php print $labelThree; ?></span>
						</footer>
					</div>
				</a>
			</div>
		</div>
	</div>
</div>