<?php
/**
 * @file
 * Author's Feed collection.
 */
// dpm( $wrapper );
global $base_url;
$content = $wrapper->value();
// dpm( $content );
$all = false;
$relations = array();
$target = '';
$currentNid = $content->nid;

switch ( $type ) {
	case 'education_spotlight_post':
		$tax = field_get_items( 'node', $content, 'field_education_spotlight' )[0]['tid'];
		$all = true;
		$displayDate = false;
		$description = 'Recent News';
		$result = db_query( 'SELECT * FROM {node} LEFT JOIN taxonomy_index AS tax ON tax.nid = node.nid WHERE node.type = :type AND node.nid <> :currentNid AND tax.tid = :tax AND node.status = 1 ORDER BY node.created DESC', array( ':type' => $type, ':currentNid' => $currentNid, ':tax' => $tax ) );
		break;
	case 'center_post':
		$tax = field_get_items( 'node', $content, 'field_center_type' )[0]['tid'];
		$relations = field_get_items( 'node', $content, 'field_center' );
		$target = 'value';
		$description = 'Recent News';
		$displayDate = false;
		$result = db_query( 'SELECT * FROM {node} LEFT JOIN taxonomy_index AS tax ON tax.nid = node.nid WHERE node.type = :type AND node.nid <> :currentNid AND tax.tid = :tax AND node.status = 1 ORDER BY node.created DESC', array( ':type' => $type, ':currentNid' => $currentNid, ':tax' => $tax ) );
		break;
	case 'blog_post':
		$relations = $content->field_associated_blog['und'];
		$target = 'tid';
		$description = 'Recent Posts';
		$displayDate = true;
		$result = db_query( 'SELECT * FROM {node} WHERE node.type = :type AND node.nid <> :currentNid AND node.status = 1 ORDER BY node.created DESC', array( ':type' => $type, ':currentNid' => $currentNid ) );
		break;
	case 'newsroom_post':
		$relations = field_get_items( 'node', $content, 'field_news_types' );
		$target = 'tid';
		$description = 'Recent News';
		$displayDate = false;
		$result = db_query( 'SELECT * FROM {node} WHERE node.type = :type AND node.nid <> :currentNid AND node.status = 1 ORDER BY node.created DESC', array( ':type' => $type, ':currentNid' => $currentNid ) );
		break;
	case 'event':
		$all = true;
		$currentDate = getdate()[0];
		$description = 'Upcoming Events';
		$displayDate = true;
		$result = db_query( 'SELECT * FROM {node} JOIN {field_data_field_date} ON node.nid = field_data_field_date.entity_id WHERE node.type = :type AND node.nid <> :currentNid AND field_data_field_date.field_date_value > :currentDate AND node.status = 1 ORDER BY field_data_field_date.field_date_value ASC', array( ':type' => $type, ':currentNid' => $currentNid, ':currentDate' => $currentDate ) );
		break;
	default:
		break;
}
$has_relations = false;
foreach ( $relations as $key => $value ) {
	if ( $value[ $target ] ){
		$has_relations = true;
		break;
	}
}
if ( $has_relations || $all ) :
	$currentRelations = array();
	foreach ( $relations as $relation ) {
		array_push( $currentRelations, $relation[ $target ] );
	}
	// Find all posts sharing the current post type
	if ( ! empty( $result ) ) :
		?>
<aside class="site__sidebar span3">
	<div class="section">
		<h2 class = '-section -short'><?php print $description; ?></h2>
		<?php
		$i = 0;
		foreach ( $result as $row ) :
			$node = node_load( $row->nid );
			$currentTarget = '';
			if ( 'blog_post' === $type ) {
				if ( ! empty( $node->field_associated_blog ) ){
					$currentTarget = field_get_items( 'node', $node, 'field_associated_blog' )[0][ $target ];
				}
			} elseif ( 'newsroom_post' === $type ) {
				if ( ! empty( $node->field_news_types ) ){
					$currentTarget = field_get_items( 'node', $node, 'field_news_types' )[0][ $target ];
				}
			}
			if ( 'center_post' === $type ){
				if ( ! empty( field_get_items( 'node', $content, 'field_center' ) ) ){
					$currentTarget = field_get_items( 'node', $node, 'field_center' )[0]['value'];
				}
			}
			if ( in_array( $currentTarget, $currentRelations ) || $all ) : // Limits results to current taxonomies
				if ( ! empty( $node->field_image ) ){
					$imageData = new stdClass();
					$imageData->classes = '-preview';
					$imageData->info = $node->field_image;
				}
				$href = drupal_get_path_alias('node/'.$row->nid);
				print "<div class = 'related-post'>";
				require drupal_get_path( 'theme', 'zero' ) . '/templates/partials/items/images.tpl.inc';
				print "<a href='/$href'><h6>$node->title</h6></a>";
				if ( $displayDate ) {
					if ( 'event' === $type ) {
						$date = date( 'F d, Y', $node->field_date['und'][0]['value'] );
					} else {
						$date = date( 'F d, Y', $node->created );
					}
					print "<h6 class = 'meta'>$date</h6>";
				}
				if ( $i > 1 ) { // Only allows 3 posts
					break;
				}
				$i++;
				print '</div>';
			endif;
		endforeach;
		?>
	</div>
</aside>
		<?php
	endif;
endif;
?>