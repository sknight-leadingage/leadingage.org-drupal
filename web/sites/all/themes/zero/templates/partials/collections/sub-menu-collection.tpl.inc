<?php
/**
 * @file
 * Sub-Menu Collection of Items. Used when the secondary nav items have children.
 */
?>
<div class="secondary-menu-container">
<?php
$link['nav_class'] = ' -aside ';
$parent_link = $link;
print '<nav class="-tertiary nav">';
foreach ( $parent_link['children'] as $key => $child ) {
	$link = menu_link_load( $child['id'] );
	$shref = $link['href'];
	if ( startsWith( strtolower( trim ( $shref ) ), 'http:/' ) || startsWith( strtolower( trim ( $shref ) ), 'https:/' ) ) {
      $link['path'] = $shref;
    } else {
      $link['path'] = '/' . $shref;
	}
	require drupal_get_path( 'theme', 'zero' ) . '/templates/partials/items/menu-item.tpl.inc';
}
print '</nav>';
?>
</div>