<?php
/**
 * @file
 * Hero -feature collection.
 */
$title = $text = $url = $img = '';
if ( ! empty( $fields['field_image'] ) ) {
	$img = '<img class="hero__bg" src="'.file_create_url( $fields['field_image']['uri'] ).'" alt="'.$wrapper->title->value().'" />';
}
if ( ! empty( $fields['field_heading'] ) ) {
	$title = "<h1 class='-hero'>".$fields['field_heading'].'</h1>';
}
if ( ! empty( $fields['field_text'] ) ) {
	$text = '<p><strong>'.$fields['field_text'].'</strong></p>';
}
if ( ! empty( $fields['field_url'] ) ) {
  if ( empty( $fields['field_url_text'] ) ) {
    $tLinkText = 'Click to Learn More';
   } else {
    $tLinkText = $fields['field_url_text'];
  }
  
	$link = "<a href='".$fields['field_url']."' class='btn -main'>$tLinkText</a>";
}
?>
<article class='hero -feature fronthero'>
	<?php
	/**
	 * Banner Image.
	 *
	 * Images aren't entities perse so, we use the fields array instead.
	 */
	print $img;
	?>
	<div class='hero__gradient'></div>
	<div class='hero__content'>
	<?php print $title.$text.$link; ?>
	</div>
</article>