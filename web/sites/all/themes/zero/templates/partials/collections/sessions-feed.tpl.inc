<?php
/**
 * @file
 * Session's Feed collection.
 */
// dpm( $wrapper );
// dpm( $wrapper->value() );
$parentEvent = $content->field_parent_event['und'][0]['target_id'];
$result = db_query( 'SELECT nid FROM {node} JOIN {field_revision_field_parent_event} ON node.nid = field_revision_field_parent_event.entity_id WHERE node.type = :type AND field_revision_field_parent_event.field_parent_event_target_id = :parentEvent AND node.status = 1 ORDER BY node.created DESC', array( ':type' => 'general_session', ':parentEvent' => $parentEvent ) );
$sessionArray = array();
foreach ( $result as $row ) {
	array_push( $sessionArray, node_load( $row->nid ) );
}
?>
<h2 class="-section">General Sessions</h2>
<?php

foreach ( $sessionArray as $content ) :
	$speakers = $info = $date = $timeOne = $timeTwo = $url = $title = $img = '';
	if ( ! empty( $content->field_date ) ){
		$date = date( 'l', $content->field_date['und'][0]['value'] );
		if ( date( 'A', $content->field_date['und'][0]['value'] ) === date( 'A', $content->field_date['und'][0]['value2'] ) ) {
			$timeOne = date( 'g:i', $content->field_date['und'][0]['value'] );
		} else {
			$timeOne = date( 'g:i A', $content->field_date['und'][0]['value'] );
		}
		$timeTwo = date( 'g:i A', $content->field_date['und'][0]['value2'] );
		$info = "<b>$date</b> &bull; $timeOne - $timeTwo";
	}
	$title = $content->title;
	$url = '/'.drupal_get_path_alias( 'node/'.$content->nid );
	$i = 0;
	if ( ! empty( field_get_items( 'node', $content, 'field_session_speakers' ) ) ){
		foreach ( field_get_items( 'node', $content, 'field_session_speakers' ) as $speaker ) {
			$key = $speaker['value'];
			$speakerData = entity_load( 'field_collection_item', array( $key ) );
			if ( 0 === $i ) {
				$img = file_create_url( field_get_items( 'field_collection_item', $speakerData[ $key ], 'field_headshot' )[0]['uri'] );
			}
			$name = $speakerData[ $key ]->field_heading['und'][0]['value'];
			$speakers .= "<h3 class='color -brand'>$name</h3>";
			$i++;
		}
	}
	?>
<div class="sessions-summary box-wrap -grid">
	<div class="row-flush">
		<a href="<?php print $url; ?>" class="box -main span4 f">
			<div class="image -headshot -session f" style="background-image: url( <?php print $img; ?> )"></div>
		</a>
		<a href="<?php print $url; ?>" class="box -main span8 l">
			<div class="box__inner">
				<header class="box__header">
					<h3 class="box__heading"><?php print $info; ?></h3>
				</header>
				<div class="box__body">
	<?php print $speakers; ?>
					<p class="box__text"><i><?php print $title; ?></i></p>
				</div>
				<footer class="box__footer">
					<span href="#" class="meta">View More</span>
				</footer>
			</div>
		</a>
	</div>
</div>
	<?php
endforeach;
$content = $wrapper->value();
?>