<?php
/**
 * @file
 * Card Grid collection.
 */
print '<section class="row -doc-show grid">';
foreach ( $nodes as $nodeID => $node ) :
	if ( ! empty( field_get_items( 'node', $node, 'field_team' )[0]['value'] ) ) :
?>
<article class="card -main span-4 item">
	<a href="<?php print url( 'node/'.$nodeID ); ?>" class="card__inner clone -height mh-med">
		<h3><?php print $node->title; ?></h3>
		<p><?php print field_get_items( 'node', $node, 'field_job_title' )[0]['value']; ?></p>
		<h6 class="label"><?php print field_get_items( 'node', $node, 'field_team' )[0]['value']; ?></h6>
	</a>
</article>
<?php
	endif;
endforeach;
print '</section>';