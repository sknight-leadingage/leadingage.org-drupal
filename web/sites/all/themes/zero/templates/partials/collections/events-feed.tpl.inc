<?php
/**
 * @file
 * Event's Feed collection.
 */
// dpm($wrapper);
// dpm($wrapper->value());
?>
<ol class='feed'>
<?php
foreach ( $feed as $content ) :
	if ( 'event' === $content->type ) {
		$date = date( 'F d, Y', $content->field_date['und'][0]['value'] );
		$time = date( 'g:i A T', $content->field_date['und'][0]['value'] );
		if ( ! empty( $content->field_location['und'][0]['value'] ) ){
			$location = '<div class="line"><div class="image -location"></div>'.$content->field_location['und'][0]['value'].'</div>';
		} else {
			$location = '';
		}
		$info = "<div class='line' ><div class='image -date'></div></div>$date.$location<div class='line' ><div class='image -time'></div>$time</div>";
	}
	$title = $content->title;
	$intro = $content->field_intro['und'][0]['safe_value'];
	$url = '/'.drupal_get_path_alias( 'node/'.$content->nid );
?>
	<li>
		<div class='testme title'>
			<a href='<?php print $url; ?>'><h4><?php print $title; ?></h4></a>
		</div>
		<div class='info'>
			<h6 class='label color -brand'><?php print $info; ?></h6>
		</div>
		<div class='introduction'>
			<?php print $intro; ?>
		</div>
	</li>
<?php
endforeach;
?>
</ol>