<?php
/**
 * @file
 * Terminology Filtered Box collection.
 */
// dpm( $wrapper->value() );
?>
<h2><?php print $wrapper->field_heading_2->value(); ?> </h2>
<div class="box-wrap -grid clone -parent">
	<div class="row-flush">
<?php
$i = 0;
foreach ( $nodes as $node ) :
	$currentNode = node_load( $node );
	if ( 2 === $i ):
		?>
	</div>
	<div class="row-flush">
		<?php
	endif;
	?>
		<a href="<?php print drupal_get_path_alias( 'node/' . $currentNode->nid ); ?>" class="box -main span6
	<?php
	if ( 0 === $i || 2 === $i ) {
		print ' f';
	}
	$i++;
	?>">
			<div class="box__inner clone -height">
				<header class="box__header">
					<h4 class="box__heading"><?php print $currentNode->title; ?></h4>
				</header>
				<div class="box__body">
					<?php if ( ! empty( $currentNode->field_intro ) ) : ?>
					<p class="box__text"><?php print strip_tags( $currentNode->field_intro['und'][0]['safe_value'] ); ?></p>
					<?php else : ?>
					<p class="box__text">&nbsp;</p>
					<?php endif; ?>
				</div>
				<footer class="box__footer">
					<span class="meta">Read More</span>
				</footer>
			</div>
		</a>
	<?php
endforeach;
?>
	</div>
</div>
<footer class="box-wrap-after">
	<a class="meta" href="/newsroom">+ View More Activitiy</a>
</footer>