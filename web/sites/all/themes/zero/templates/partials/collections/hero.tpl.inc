<?php
/**
 * @file
 * Hero collection.
 */
?>
<article class='hero -simple'>
	<?php
	/**
	 * Banner Image.
	 *
	 * Images aren't entities perse so, we use the fields array instead.
	 */
	if ( ! empty( $fields['field_image'] ) ) :
		?>
	<img class='hero__bg' src='<?php print file_create_url( $fields['field_image']['uri'] ); ?>' alt='<?php print $wrapper->title->value(); ?>' />
	<?php
      list($imgwidth, $imgheight, $imgtype, $imgattr) = getimagesize( $fields['field_image']['uri'] );
      if ( $imgheight > 250 ) {
        echo("<div class='hero__content'>");
      } else {
        echo("<div class='hero__content__short__img'>");
      }
	else :
		?>
	<!-- <img class='hero__bg' src='<?php //print '/' . drupal_get_path( 'theme', 'zero' ) . '/docs/assets/img/samples/hero_homepage.jpg'; ?>' alt='Sample image for homepage hero' /> -->
	<div class='hero__content__short'>
		<?php
	endif;
	?>
	<?php
	if ( ! empty( $fields['title'] ) && ( ( ! empty( $fields['field_hide_title'] ) && $fields['field_hide_title'] != 1 ) ) || ( empty( $fields['field_hide_title'] ) ) ) :
	?>
		<h1 class='-hero'><?php print $fields['title']; ?></h1>
	<?php
	else :
	?>
		<h1 class='-hero'>&nbsp;</h1>
	<?php
	endif;
	?>
	</div>
</article>