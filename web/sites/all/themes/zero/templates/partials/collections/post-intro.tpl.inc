<?php
/**
 * @file
 * Post Intro collection.
 */
global $user, $base_url, $base_path;
if ( isset( $wrapper ) && ! empty( $wrapper ) ) {
  $content = $wrapper->value();
  // dpm( $content );
  $title = $content->title;
  if ( ! empty( $content->field_intro ) ) {
    $intro = field_get_items( 'node', $content, 'field_intro' )[0]['value'];
  } else {
    $intro = '';
  }
  $date = date( 'F d, Y', $content->created );
  $separator = '   |   ';
  if ( ! empty( $content->field_author ) || ! empty( $content->field_author_non_staff ) ) {
    $author = $separator.'by ';
    $i = 0;
    $authors = field_get_items( 'node', $content, 'field_author' );
    if ( ! empty( $authors ) ){
      foreach ( $authors as $key => $name ) {
        if ( $i > 0 ){
          $author .= ', ';
        }
        $author .= $name['entity']->title;
        $i++;
      }
    }
    $additional_authors = field_get_items( 'node', $content, 'field_author_non_staff' );
    if ( ! empty( $additional_authors ) ){
      foreach ( $additional_authors as $key => $additional_author ){
        if ( $i > 0 ){
          $author .= ', ';
        }
        $author .= $additional_author['value'];
        $i++;
      }
    }
  } else {
    $author = '';
  }

  switch ( $type ) {
    case 'education_spotlight_post':
      $info = 'Education Spotlight';
      $info .= $separator.field_get_items( 'node', $content, 'field_education_spotlight' )[0]['taxonomy_term']->name;
      break;
    case 'center_post':
      $info = 'Center Post';
      break;
    case 'blog_post':
      $relatedPosts = field_get_items( 'node', $content, 'field_associated_blog' )[0]['tid'];
      $relatedPostsName = field_get_items( 'node', $content, 'field_associated_blog' )[0]['taxonomy_term']->name;
      $info = $relatedPostsName.$separator.$date.$author;
      break;
    case 'newsroom_post':
      $relatedPosts = field_get_items( 'node', $content, 'field_news_type' )[0]['tid'];
      $relatedPostsName = field_get_items( 'node', $content, 'field_news_type' )[0]['taxonomy_term']->name;
      $info = $relatedPostsName.$separator.$date.$author;
      break;
    case 'event':
      $relatedPosts = $content->type;
      // $relatedPostsName = $content->field_news_type['und'][0]['taxonomy_term']->name;
      date_default_timezone_set( field_get_items( 'node', $content, 'field_date' )[0]['timezone'] );
      $date = date( 'F d, Y', field_get_items( 'node', $content, 'field_date' )[0]['value'] );
      $time = date( 'g:i A T', field_get_items( 'node', $content, 'field_date' )[0]['value'] );
      if ( ! empty( $content->field_location['und'][0]['value'] ) ){
        $location = '<div class="line"><div class="image -location"></div>'.field_get_items( 'node', $content, 'field_locaion' )[0]['value'].'</div>';
      } else {
        $location = '';
      }
      $info = "<div class='line'><div class='image -date'></div>$date</div>$location<div class='line'><div class='image -time'></div>$time</div>";
      break;
    case 'magazine_article':
      $info = $date.$author;
      break;
    case 'general_session':
      date_default_timezone_set( field_get_items( 'node', $content, 'field_date' )[0]['timezone'] );
      $date = date( 'F d, Y', field_get_items( 'node', $content, 'field_date' )[0]['value'] );
      if ( date( 'A', field_get_items( 'node', $content, 'field_date' )[0]['value'] ) === date( 'A', field_get_items( 'node', $content, 'field_date' )[0]['value'] ) ) {
        $timeOne = date( 'g:i T', field_get_items( 'node', $content, 'field_date' )[0]['value'] );
      } else {
        $timeOne = date( 'g:i A T', field_get_items( 'node', $content, 'field_date' )[0]['value'] );
      }
      $timeTwo = date( 'g:i A T', field_get_items( 'node', $content, 'field_date' )[0]['value'] );
      $time = "$timeOne - $timeTwo";
      $info = "<div class='image -date'></div>$date<div class='image -time'></div>$time";
      break;
    default:
      break;
  }

  $path = current_path();
  $path_alias = drupal_lookup_path( 'alias', $path );

  $imageData = new stdClass();
  $imageData->classes = '-intro';
  if ( ! empty( $content->field_image ) ){
    $imageData->info = $content->field_image;
  }

  $data = new stdClass();

  $data->url = "$base_url/$path_alias";
  $data->title = str_replace( '"', '', str_replace( "'", '', strip_tags( $title ) ) );

  if ( ! empty( $imageData->info['und'][0]['uri'] ) ) {
    $data->image = 'http:'.file_create_url( $imageData->info['und'][0]['uri'] );
  } else {
    $data->image = $base_url.'/sites/all/themes/zero/logo.png';
  }
  $data->text = strip_tags( $intro );
  $shareData = "data-title='$data->title' data-url='$data->url' data-image='$data->image' data-text='$data->text'";

  ?>
<article>
  <div class='-intro'>
	<div class="title">
		<h1><?php print $title; ?></h1>
	</div>
	<?php
	require drupal_get_path( 'theme', 'zero' ) . '/templates/partials/items/images.tpl.inc';
	?>
	<div class="info">
		<h6 class="label color -brand"><?php print $info; ?></h6>
	</div>
	<div class="printhead">
      <a href="javascript:print();" class="printme">Print this Article</a>
  </div>
	<?php
	if ( ! empty( $intro ) ) :
		?>
	<div class="introduction">
		<p><i><?php print $intro; ?></i></p>
	</div>
		<?php
	endif;
	?>
	<div class="social section -marked -wide">
		<a class="btn -main socicon-twitter" <?php print $shareData; ?> >Voice Your Opinion</a>
		<a class="btn -main socicon-facebook" <?php print $shareData; ?> >Tell Your Friends</a>
	</div>
  </div>
</article>
<?php
  }
?>