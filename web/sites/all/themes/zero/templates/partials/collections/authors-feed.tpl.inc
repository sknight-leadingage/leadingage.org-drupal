<?php
/**
 * @file
 * Author's Feed collection.
 */
// dpm( $wrapper );
// dpm( $wrapper->value() );
if ( ! empty( $wrapper->value()->field_user['und'][0]['target_id'] ) ) :
	$content = $wrapper->value()->field_user['und'][0]['target_id'];
	//$result = db_query( 'SELECT * FROM {node} WHERE uid = :uid AND type = :type AND status = 1 ORDER BY created DESC', array( ':uid' => $content, ':type' => 'blog_post' ) );
	$result = db_query( 'SELECT * FROM {node} WHERE uid = :uid AND status = 1 ORDER BY created DESC LIMIT 10', array( ':uid' => $content ) );
	if ( ! empty( $result ) ) :
		?>
	<div class='section'>
		<h2 class='-alt'>Recent Articles from this Author</h2>
		<?php
		foreach ( $result as $row ) :
			?>
		<a href="/node/<?php print $row->nid; ?>"><p><?php print $row->title; ?></p></a>
			<?php
		endforeach;
		?>
	</div>
		<?php
	endif;
endif;
?>