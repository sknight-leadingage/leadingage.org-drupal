<?php
/**
 * @file
 * Meta-info Collection.
 */
$issueNid = $content->field_parent_issue['und'][0]['target_id'];
$node = node_load( $issueNid );
$volume = $issue = $number = '';
$issue = $node->field_issue['und'][0]['safe_value'];
if ( ! empty( $node->field_volume['und'][0]['value'] ) ) {
	$volume = ' &bull; Volume '.sprintf( '%02d', $node->field_volume['und'][0]['value'] );
}
if ( ! empty( $node->field_number['und'][0]['value'] ) ) {
	$number = ' &bull; Number '.sprintf( '%02d', $node->field_number['und'][0]['value'] );
}
$metaInfo = $issue.$volume.$number;
if ( ! empty( $metaInfo ) ) {
  $metaInfo = "LeadingAge Magazine &middot; " . $metaInfo;
}
?>
<div class='meta__content background -grey'>
	<div class='row--main container'>
		<h6 class='meta color -black'><?php print $metaInfo; ?></h6>
	</div>
</div>
