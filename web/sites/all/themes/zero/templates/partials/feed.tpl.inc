<?php
/**
 * @file
 * Filterable feed partial.
 */
global $base_url;
$feed_header = $filterableFeedIntro = '';

if ( ! empty( field_get_items( 'node', $content, 'field_heading_2' )[0]['value'] ) ) {
	$header = field_get_items( 'node', $content, 'field_heading_2' )[0]['value'];
	$feed_header = "<h2>$header</h2>";
}
$results_array = views_get_view_result( explode( '|', field_get_items( 'node', $content, 'field_feed' )[0]['vname'] )[0] );
$result_count = count( $results_array );
include drupal_get_path( 'theme', 'zero' ) . '/templates/partials/collections/base-feed.tpl.inc';