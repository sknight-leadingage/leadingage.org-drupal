<?php
/**
 * @file
 * Filterable feed partial.
 */
global $base_url;
// dpm( $content );
if ( ! field_get_items( 'node', $content, 'field_is_blog' )[0]['value'] || empty( field_get_items( 'node', $content, 'field_is_blog' ) ) ) :
	$terms = $skip_filter = $results_array = $removed = $types = $terms = $removed_options = $additional = $term_groups = array();
	// If There is a Query param, add it to the required terms for the feed
	if ( ! empty( $query_params ) ){
		foreach ( $query_params as $key => $value ) {
			if ( 'p' !== $key && 'mkt_tok' !== $key ){
				array_push( $terms, $value );
			}
		}
		if ( ! empty( $terms ) ){
			array_push( $term_groups, $terms );
		}
	}
	// Get Content Types to include in the feed
	if ( ! empty( field_get_items( 'node', $content, 'field_feed_content_types' ) ) ){
		foreach ( field_get_items( 'node', $content, 'field_feed_content_types' ) as $key => $value ) {
			array_push( $types, str_replace( '-', '_', $value['value'] ) );
		}
	}
	// Get Taxonomies to include in the feed
	if ( ! empty( field_get_items( 'node', $content, 'field_remove_filter_options' ) ) ){
		$terms = array();
		foreach ( field_get_items( 'node', $content, 'field_remove_filter_options' ) as $key => $value ) {
			array_push( $removed, $value['target_id'] );
		}
		array_push( $term_groups, $removed );
	}
	// Handle Feeds differently on provider pages as opposed to landing pages.
	if ( 'provider' === $content->type ){
		$types = array( 'newsroom_post' );
		if ( empty( $terms ) ){
			$terms = array( field_get_items( 'node', $content, 'field_provider_type' )[0]['target_id'] );
			foreach ( taxonomy_get_children( $terms[0] ) as $key => $value ) {
				array_push( $terms, $value );
				array_push( $terms, $value->tid );
			}
			if ( count( $terms ) < 2 ) {
				$skip_filter = true;
			}
		}else {
			foreach ( taxonomy_get_children( field_get_items( 'node', $content, 'field_provider_type' )[0]['target_id'] ) as $key => $value ) {
				array_push( $terms, $value );
			}
		}
		if ( ! empty( $terms ) ){
        array_push( $term_groups, $terms );
		}
	}
	if ( empty( $term_groups ) ){
		// Return a feed of content all content of the designated type if no taxonomies are included.
		$result = db_query("
			SELECT DISTINCT node.nid AS nid,
			node.title AS node_title,
			node.created AS node_created
			FROM node
			LEFT JOIN taxonomy_index
			ON node.nid = taxonomy_index.nid
			WHERE node.status = '1'
			AND node.type IN (:types)
			ORDER BY node_created DESC;",
			array(':types' => $types )
		);
	} elseif ( 1 === count( $term_groups ) ) {
		// Return a feed of content that includes at least one of the included taxonomies.
		$result = db_query("
			SELECT DISTINCT node.nid AS nid,
			node.title AS node_title,
			node.created AS node_created
			FROM node
			LEFT JOIN taxonomy_index
			ON node.nid = taxonomy_index.nid
			WHERE node.status = '1'
			AND taxonomy_index.tid IN (:terms)
			AND node.type IN (:types)
			ORDER BY node_created DESC;",
			array(':terms' => $term_groups[0], ':types' => $types)
		);
	} else {
		// Return a feed of content that includes at least one of the included taxonomies from each source of taxonomies (The admin panel taxonomy selector, and the query params created by filter dropdown links).
		$result = db_query("
			SELECT DISTINCT node.nid AS nid,
			node.title AS node_title,
			node.created AS node_created
			FROM node
			LEFT JOIN taxonomy_index as tax_index_1
			ON node.nid = tax_index_1.nid
			LEFT JOIN taxonomy_index as tax_index_2
			ON node.nid = tax_index_2.nid
			WHERE node.status = '1'
			AND tax_index_1.tid IN (:terms)
			AND tax_index_2.tid IN (:additional)
			AND node.type IN (:types)
			ORDER BY node_created DESC;",
			array(':terms' => $term_groups[0], ':types' => $types, ':additional' => $term_groups[1] )
		);
	}

	$results_array = $result->fetchAll();

	$result_count = count( $results_array );
	if ( ! empty( $content->field_feed_filters ) && count( $content->field_feed_filters['und'] ) > 1 ) {
		$classes = '  -multi ';
	}
	if ( ( empty( field_get_items( 'node', $content, 'field_hide_filter' ) ) && ! empty( $filter ) ) || ! field_get_items( 'node', $content, 'field_hide_filter' )[0]['value'] ) :
		if ( ! empty( $content->field_remove_filter_options ) ){
			foreach ( $content->field_remove_filter_options['und'] as $key => $value ) {
				array_push( $removed_options, $value['target_id'] );
			}
		}
		if ( ! empty( field_get_items( 'node', $content, 'field_heading' ) ) && 'provider' === $content->type ){
			$heading = '<h2>'.field_get_items( 'node', $content, 'field_heading' )[0]['value'].'</h2>';
		} elseif ( ! empty( field_get_items( 'node', $content, 'field_heading_3' ) ) ){
			$heading = '<h2>'.field_get_items( 'node', $content, 'field_heading_3' )[0]['value'].'</h2>';
		}else {
			$heading = '<h2>Recent Activity</h2>';
		}
		?>
<section class="section" id="filter">
		<?php
		if ( ! $skip_filter ) :
			// Gather all terms for all results in order to remove filter options with 0 results
			$nids = array();
			foreach ($results_array as $key => $value) {
				array_push($nids, $value->nid);
			}
			if ( ! empty( $nids ) ) {
          $included_terms = db_query("
            SELECT DISTINCT tax_index_1.tid
            FROM node
            LEFT JOIN taxonomy_index as tax_index_1
            ON node.nid = tax_index_1.nid
            AND node.nid IN (:nids)",
            array(':nids' => $nids )
          );
          $included_terms = $included_terms->fetchAll();
          $tids = array();
          foreach ( $included_terms as $key => $value ) {
            $included_terms[$key] = $value->tid;
          }
        } else {
          $skip_filter = true;
			}
			?>
	<h2><?php print $heading; ?></h2>
	<div class="box-wrap -filter <?php print $classes; ?>">
			<?php
			if ( ! empty( field_get_items( 'node', $content, 'field_instructions' ) ) ) {
				$instructions = field_get_items( 'node', $content, 'field_instructions' )[0]['value'];
			} else {
				$instructions = 'select a member type from the dropdown on the right to filter the items below';
			}
			if ( 1 === count( field_get_items( 'node', $content, 'field_feed_filters' ) ) ){
				print '<p class="label">'.$instructions.'</p>';
			}
			if ( 'provider' === $content->type ){
				$filter['entity'] = taxonomy_vocabulary_machine_name_load( 'provider_types' );
				$filter_name = $filter['entity']->name;
				require drupal_get_path( 'theme', 'zero' ) . '/templates/partials/items/dropdown-filter.tpl.inc';
			} else {
				// Add up to 3 filter drop downs.
				foreach ( field_get_items( 'node', $content, 'field_feed_filters' ) as $key => $filter ) :
					$terms = taxonomy_get_tree( $filter['target_id'] );
					// Remove filter options with empty results
					foreach ( $terms as $key => $value ) {
						if ( ! in_array( $value->tid, $included_terms ) ){
							unset( $terms[$key] );
						}
					}
					$filter_name = $filter['entity']->name;
					if ( ! empty( $query_params ) ){
						foreach ( $query_params as $key => $value ) {
							if ( $key === $filter['entity']->machine_name ){
								$term = taxonomy_term_load( $value );
								$filter_name = $term->name;
							}
						}
					}
					include drupal_get_path( 'theme', 'zero' ) . '/templates/partials/items/dropdown-filter.tpl.inc';
				endforeach;
			}
			?>
	</div>
			<?php
		endif;
		if ( ! empty( field_get_items( 'node', $content, 'field_intro' ) ) || ! empty( field_get_items( 'node', $content, 'field_section_heading' ) ) ) :
			?>
	<div>
			<?php
			if ( ! empty( field_get_items( 'node', $content, 'field_section_heading' ) ) ){
				print '<h2>'.field_get_items( 'node', $content, 'field_section_heading' )[0]['value'].'</h2>';
			}
			if ( ! empty( field_get_items( 'node', $content, 'field_intro' ) ) ){
				print '<i>'.field_get_items( 'node', $content, 'field_intro' )[0]['value'].'</i>';
			}
			?>
	</div>
			<?php
		endif;
		?>
</section>
	<?php
	endif;
	include drupal_get_path( 'theme', 'zero' ) . '/templates/partials/collections/base-feed.tpl.inc';
endif;