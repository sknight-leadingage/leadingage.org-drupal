<?php
/**
 * @file
 * Magazine nav partial.
 */
?>
<nav id='magazine-nav' class='section -flat'>
	<ul class='nav group container -alt'>
		<li class='nav__item f
<?php
if ( $is_latest_magazine ) {
	print ' -active';
}
?>
	'>
			<a title='This Issue' href='/magazine'>This Issue</a>
		</li>
<?php
$mag_menu_count = count( $mag_menu );
foreach ( $mag_menu as $mag_menu_key => $mag_menu_item ) :
	?>
		<li class='nav__item
	<?php
	if ( $mag_menu_key === $mag_menu_count - 1 ) {
		print ' l';
	}
	if ( '/' . $path_alias === $mag_menu_item['path'] ) {
		print ' -active';
	}
	?>
	'>
			<a title='<?php print $mag_menu_item['title']; ?>' href='<?php print $mag_menu_item['path']; ?>'>
	<?php print $mag_menu_item['title']; ?>
			</a>
		</li>
	<?php
endforeach;
?>
	</ul>
</nav>
