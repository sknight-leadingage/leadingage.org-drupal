<?php
/**
 * @file
 * Standard block.
 */
if ( ! empty( $fields['field_block'][0]->title ) ) {
	$block_title = $fields['field_block'][0]->title;
} else {
	// $block_title = fields['field_block'][0]->field_feature['und'][0]title['und'][0];
}
$node = node_load( $fields['field_block'][0]->field_feature['und'][0]['target_id'] );
// dpm( $node );
?>
<section class='block-wrap section'>
	<div class='container'>
		<article class='block'>
			<div class='inner'>
		<?php if ( ! empty( $fields['field_block'][0]->field_image ) ) : ?>
				<img class='block__image' src='<?php print image_style_url( 'block', $fields['field_block'][0]->field_image['und'][0]['uri'] ); ?>' alt='<?php print $block_title; ?>' />
				<div class='block__text'>
		<?php endif; ?>
					<h1><?php print $block_title; ?></h1>
		<?php
		if ( ! empty( $fields['field_block'][0]->field_intro ) ) :
			?>
					<p><?php print $fields['field_block'][0]->field_intro['und'][0]['value']; ?></p>
			<?php
		else :
			print $fields['field_block'][0]->field_feature->body['und'][0]['summary'];
		endif;
		?>
					<a href='/<?php ( ! empty( $fields['field_block'][0]->field_url ) )? print $fields['field_block'][0]->field_url['und'][0] : print drupal_get_path_alias( 'node/' . $fields['field_block'][0]->field_feature['und'][0]['target_id'] ); ?>' class='btn -main'>
		<?php
		if ( ! empty( $fields['field_block'][0]->field_button ) ) :
			print $fields['field_block'][0]->field_button['und'][0]['value'];
		else :
			print $fields['field_block'][0]->field_feature->nid;
		endif;
		?>
					</a>
		<?php
		if ( ! empty( $fields['field_block'][0]->field_image ) ) :
			?>
				</div>
			<?php
		endif;
		?>
			</div>
		</article>
	</div>
</section>
