<?php
/**
 * @file
 * Buckets partial.
 */
if ( ! empty( $ads_fields['title'] ) ) :
	?>
<h2 class='-section -short'><?php print $ads_fields['title']; ?></h2>
	<?php
endif;
if ( ! empty( $ads_fields['field_ads'] ) ) :
	$ads = $ads_fields['field_ads'];
	//shuffle( $ads );
	//$ads = array_slice( $ads, 0, 3 );
	foreach ( $ads as $ad ) :
		if ( ! empty( $ad->field_image ) ) :
			?>
<div class='ad-container' style='background-color: #<?php print field_get_items( 'node', $ad, 'field_background_color' )[0]['jquery_colorpicker']; ?>; '>
	<a class='ad -sidebar' href='<?php print field_get_items( 'node', $ad, 'field_url' )[0]['value']; ?>'>
		<div class='ad-img-sidebar'>
			<img src='<?php print image_style_url( 'sidebar_ads', field_get_items( 'node', $ad, 'field_image' )[0]['uri'] ); ?>' alt='<?php print $ad->title; ?>' />
		</div>
	<!-- <p class='ad-text--side'><?php //print $ad->field_ad_text['und'][0]['value']; ?></p> -->
	</a>
</div>
			<?php
		endif;
	endforeach;
endif;
?>
