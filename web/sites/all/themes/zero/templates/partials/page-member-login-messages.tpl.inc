<?php
/**
 * @file
 * Members Only logic partial.
 */

  function outTextNotMember() {
    print "<p>This content is provided exclusively for LeadingAge members. Our records indicate that you are not a member.</p>\n<p>If you believe that you are a member, please <a href=\"/contact-us\">reach out via our Contact Us page</a> for further assistance.</p>\n";
  }
  
  function outTextLogIn() {
    $strHost = 'my';
    if (stripos(gethostname(), 'leading-age-web-02') !== FALSE) { $strHost = 'devmy'; }
    $strReturnUrl = rawurlencode(drupal_get_path_alias());
    if (empty($strReturnUrl) || $strReturnUrl == 'node%2F114') { $strReturnUrl = '%2F'; }
    print "<p>You must be logged in with member access to view content on this page. <a href=\"https://$strHost.leadingage.org/core/user/dsso.aspx?r=$strReturnUrl\">Please click here to log in with your My.LeadingAge account</a>.</p>\n";
  }

?>