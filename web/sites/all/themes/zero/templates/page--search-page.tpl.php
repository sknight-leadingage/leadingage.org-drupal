<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 * - $type: describes the content type currently being processed.
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
$basicPost = $relatedPostsSidebar = $pagerSidebar = $subMenu = $metaInfo = $sessionList = $inheritAds = $speakerSummary = false;
// dpm( $content);
// $view = views_get_view('clone_of_search_page');
// dpm($view);
if ( ! empty( $wrapper)) {
    $content = $wrapper->value();
}
$query_params = drupal_get_query_parameters();
$filters = array();

foreach ( $query_params as $key => $value ) {
  if ( 'f' === $key ){
    foreach ($value as $filter_key => $filter_value) {
      $pieces = explode( ':', $filter_value );
      if ( 'type' === $pieces[0] ){
        $types = node_type_get_types();
        $pieces[1] = $types[$pieces[1]]->name;
      } else {
        $pieces[1] = taxonomy_term_load( $pieces[1] )->name;
      }
      $filters[ $pieces[0] ] = $pieces[1];
    }
  }
}
?>
<div id="page-wrapper">
  <div id="page">
    <div id="header">
    <?php
    if ( false && $logo ) :?>
        <a href="<?php print $front_page; ?>" title="<?php print t( 'Home' ); ?>" rel="home" id="logo">
          <img src="<?php print $logo; ?>" alt="<?php print t( 'Home' ); ?>" />
        </a>
    <?php
    endif;
    print render( $page['header']);?>
      <section class="row--main container">
        <article class="span7">
          <header class="section"<?php if ( ! empty( $query_params['title'] ) ) { echo ' style="margin-bottom: 0px;"'; } ?>>
            <h1 class='search-results'>Search results</h1>
          </header>
        </article>
      </section>
    </div> <!-- /.section, /#header -->
    <?php
    if ( $metaInfo) {
        require_once drupal_get_path( 'theme', 'zero' ) . "/templates/partials/collections/meta-info.tpl.inc";
    }
    if ( $subMenu) {
        require_once drupal_get_path( 'theme', 'zero' ) . "/templates/partials/collections/menu.tpl.inc";
    }
    print $messages;
    ?>
    <div id="main-wrapper">
      <div id="main" class="clearfix container row--main">
        <?php
        if ( ! empty( $relatedPostsSidebar) && $relatedPostsSidebar) {
            require_once drupal_get_path( 'theme', 'zero' ) . "/templates/partials/collections/related-posts.tpl.inc";
        } elseif ( ! empty( $pagerSidebar) && $pagerSidebar) {
            require_once drupal_get_path( 'theme', 'zero' ) . "/templates/partials/collections/pager-sidebar.tpl.inc";
        }
        ?>
        <div id="content" class="column span7">
          <div class="section">
          
          <?php
          if ( ! empty( $query_params['title'] ) ) {
          ?>
          <p>We've searched our content for <i>article titles</i> that contain <strong><?php echo $query_params['title']; ?></strong> and
          <?php
          if ( $GLOBALS['pager_total_items'][0] <= 0 ) {
              echo "we couldn't find any results.  However, you can expand your search with one of these options:";
            } else {
              echo "the results are below.  If you'd like to expand your search, try one of these:";
          }
          ?>
          </p>
          <div style="margin: 0 0 36px 24px;">
            <ul>
              <li><a href="<?php echo request_path() . '?fulltext=' . $query_params['title']; ?>" title="keyword content search">Search all article text for the <i>keywords</i>: <?php echo $query_params['title']; ?></a></li>
              <li><a href="<?php echo request_path() . '?fulltext=%22' . $query_params['title'] . '%22'; ?>" title="sentence content search">Search all article text for <i>the sentence</i> "<?php echo $query_params['title']; ?>"</a></li>
              <li><a href="<?php echo '/search-files?filename=%22' . $query_params['title'] . '%22&caption=%22' . $query_params['title'] . '%22'; ?>" title="documents search">Search Documents (PDF, Excel, Word, etc.) where their filename contains: <?php echo $query_params['title']; ?></a></li>
            </ul>
          </div>
          <?php
          }
          ?>
          
        <?php
        $regex= '/pager-current(?:"| last")>(\d+)/';
        preg_match($regex, $page['content']['system_main']['main']['#markup'], $num);
        if( empty( $num[1] ) ){
          $num[1]='1';
        }
        $startingPlace= '<ol start="'.$num[1].'" class="feed ';
        $page['content']['system_main']['main']['#markup'] = preg_replace('/(<ol class=")/', $startingPlace, $page['content']['system_main']['main']['#markup']);
        
        print render( $page['content'] );

        ?>
        </div> <!-- /.section, /#content -->
      </div>
      <aside class="side__sidebar span3 l">
        <?php
        require_once drupal_get_path( 'theme', 'zero' ) . "/templates/partials/sidebar-ad.tpl.inc";
        ?>
      </aside>
    </div> <!-- /#main, /#main-wrapper -->
    <div id="footer">
      <div class="section">
        <?php print render( $page['footer']); ?>
      </div>
    </div> <!-- /.section, /#footer -->
  </div>
</div> <!-- /#page, /#page-wrapper -->
