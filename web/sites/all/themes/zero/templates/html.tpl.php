<?php
/**
 * @file
 * Html wrapper template.
 */
 
 $strServerHost = gethostname();
 $strSandboxHost = 'LA-CL-WEBDEV';
 
 $strtmpCSP = drupal_get_http_header('Content-Security-Policy');
 $strtmpCSPPos1 = stripos($strtmpCSP, 'nonce-');
 if ($strtmpCSPPos1 >= 0) {
    $strtmpCSPPos1 = $strtmpCSPPos1 + 6;
    $strtmpCSPPos2 = stripos($strtmpCSP, "'", $strtmpCSPPos1);
 }
 $strInlineNonce = '';
 if ($strtmpCSPPos1 >= 0 && $strtmpCSPPos2 > $strtmpCSPPos1) {
    $strInlineNonce = 'nonce="' . substr($strtmpCSP, $strtmpCSPPos1, $strtmpCSPPos2 - $strtmpCSPPos1) . '"';
 }
 
?>
<!DOCTYPE html>
<html xmlns:og="http://opengraphprotocol.org/schema/">
<head>
	<?php print $head; ?>
	<title><?php print $head_title; ?></title>
	<?php print $styles; ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="https://use.typekit.net/sfm0ebu.js"></script>

	<link type="text/css" rel="stylesheet" href="/sites/all/themes/zero/assets/css/patch.css?v=4.41" media="all" />
  


	<script <?php echo($strInlineNonce); ?>>try{Typekit.load( { async: true } );}catch( e ){}</script>


<!-- BEGIN: MDG 20180723 Google Tag Manager -->
<script <?php echo($strInlineNonce); ?>>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;var n=d.querySelector('[nonce]');
n&&j.setAttribute('nonce',n.nonce||n.getAttribute('nonce'));f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TV9GS4X');</script>
<!-- END: MDG 20180723 Google Tag Manager -->





<?php
  if ( empty( $strServerHost ) || ( !empty( $strServerHost ) && $strServerHost !== $strSandboxHost ) ) {
?>
  <!-- 
  Start of global snippet: Please do not remove
  Place this snippet between the <head> and </head> tags on every page of your site.
  -->
  <!-- Global site tag (gtag.js) - Google Marketing Platform -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=DC-9036391" <?php echo($strInlineNonce); ?>></script>
  <script <?php echo($strInlineNonce); ?>>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'DC-9036391');
  </script>
  <!-- End of global snippet: Please do not remove -->

  <!-- Global site tag (gtag.js) - Google Ads: 860960538 -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=AW-860960538" <?php echo($strInlineNonce); ?>></script>
  <script <?php echo($strInlineNonce); ?>>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'AW-860960538');
  </script>


<?php
  }
  $nid1 = 0;  
  if ( !empty($node) ) {
    $nid1 = $node->nid;
  } else if ($node1 = menu_get_object()) {
    $nid1 = $node1->nid;
  }

  if ($nid1 == 58968 || $nid1 == 58974 || $nid1 == 62805) {
  ?>
  <link type="text/css" rel="stylesheet" href="/sites/all/themes/zero/assets/css/data-table.css?v=1.01" media="all" />
<?php
}
?>
</head>
<body>
	<div id="skip-link">
		<a href="#main" class="element-invisible element-focusable"><?php print t( 'Skip to main content' ); ?></a>
	</div>
	<header class="site__header">
		<div id="hamburger">
			<div class='line'></div>
		</div>
		<div class="header__top container">
<?php
include drupal_get_path( 'theme', 'zero' ) . '/templates/partials/items/site-info.tpl.inc';
?>
			<div class="segment -right">
<?php
$search_form = drupal_get_form( 'search_block_form' );
print drupal_render( $search_form );
?>


        <ul class="nav-simple-top">
<?php
  $strSelfServiceHost = 'my';
  if ( !empty( $strServerHost ) && $strServerHost === $strSandboxHost ) { $strSelfServiceHost = 'devmy'; }

  if (user_is_logged_in() == TRUE) {
          $soUname = 'My.LeadingAge Profile';
          if (isset($user->name) && !empty($user->name)) {
              $soUname = 'Hello ' . $user->name;
              $aName = explode(" ", $user->name);
              if (count($aName) > 1 && strlen($aName[0]) > 1) {
                  $sanamet = substr(trim(strtolower($aName[0])), 0, 2);
                  if ($sanamet == 'mr' || $sanamet == 'ms' || $sanamet == 'dr') {
                      $soUname = 'Hello ' . $aName[1];
                  }
              }
              
          }
          echo '<li class="rbar"><a href="https://' . $strSelfServiceHost . '.leadingage.org/">' . $soUname . '</a></li>';
  }
?>
          <li class="rbar">
<?php
  if (user_is_logged_in() == TRUE) {
      echo '					  <a href="/user/logout">Logout</a>';
  }
  else {
      $strReturnUrl = rawurlencode(drupal_get_path_alias());
      if (empty($strReturnUrl) || $strReturnUrl == 'node%2F114') { $strReturnUrl = '%2F'; }
      echo '					  <a href="https://' . $strSelfServiceHost . '.leadingage.org/core/user/dsso.aspx?r=' . $strReturnUrl . '">My.LeadingAge Login</a>';
  }
?>
          </li>
          <li><a href="#" title="Careers" id="cnav">Careers</a>
            <ul>
              <li><a title="Aging Services Career Center" href="http://careers.leadingage.org/">Aging Services Career Center</a></li>
              <li><a title="LeadingAge Current Job Openings" href="/working-leadingage">LeadingAge Current Job Openings</a></li>
            </ul>
          </li>
          <div class="clear"></div>
        </ul>
			</div>
		</div>
		<div class="header__main container">
			<h1 id="brand"><a href="/"><?php print $head_title_array['name']; ?></a></h1>
			<?php
        if ( !empty( $strServerHost ) && $strServerHost === $strSandboxHost ) {
			?>
      <div id="topnavspacing">
        <span class="top-block-highlight">This is our Sandbox Website. Please visit <a href="http://www.leadingage.org/">www.leadingage.org</a> for current information.</span>
      </div>
      <?php
         } else {
      ?>
      <div id="topnavspacing"></div>
      <?php
        }
      ?>
			<ul id="nav" class="nav group">
<?php
$menu_index = 0;
$menus_arr_length = count( $main_menu );
$main_menu_data_array = array();
foreach ( $main_menu as $menu_key => $menu_data ) :
	$contains_active = false;
	$menu_item_links = $menu_data['links'];
	// Add to array to turn to JSON.
	$main_menu_data_array[ $menu_key ]['above'] = $menu_item_links;
	// Check first level for active link.

  //if ( isset( $menu_item_data['path'] ) ) {
      foreach ( $menu_item_links as $menu_item_key => $menu_item_data ) :
        if (isset($menu_item_data['path']) && ($menu_item_data['title'])) {
            if ( isset( $path_alias_parts[0] ) && $menu_item_data['path'] == '/'.$path_alias ) :
              $contains_active = true;
            endif;
            $main_menu_data_array[ $menu_key ]['above'][ $menu_item_key ]['is_active'] = ( $menu_item_data['path'] === '/' . $path_alias );
            // Check second level for active link.
            if ( isset( $menu_item_data['below'] ) ) {
              $menu_item_links_below = $menu_item_data['below'];
              // Add to array to turn into JSON.
              $main_menu_data_array[ $menu_key ]['below'][ $menu_item_data['title'] ] = $menu_item_links_below;
              foreach ( $menu_item_links_below as $menu_item_below_key => $menu_item_below_data ) {
                if ( '/'.$path_alias == $menu_item_below_data['path'] ) {
                  $contains_active = true;
                }
                $main_menu_data_array[ $menu_key ]['below'][ $menu_item_data['title'] ][ $menu_item_below_key ]['is_active'] = ( $menu_item_below_data['path'] === '/' . $path_alias );
              }
            }
        }
      endforeach;
  //}
	
	if ( 0 === $menu_index ) :
		?>
				<li class='nav__item f<?php if ( $contains_active ) { print ' -active'; } ?>'><!-- menu3 -->
		<?php
	elseif ( $menu_index === ( $menus_arr_length - 1 ) ) :
		?>
				<li class='nav__item l<?php if ( $contains_active ) { print ' -active'; } ?>'><!-- menu2 -->
		<?php
	else :
		?>
				<li class='nav__item<?php if ( $contains_active ) { print ' -active'; } ?>'><!-- menu1 -->
		<?php
	endif;
	?>
	<?php
    $strMenuTitle = $menu_data['title'];
    if (strtolower($strMenuTitle) == 'education') {
        $strMenuTitle = 'resources';
      } elseif (strtolower($strMenuTitle) == 'advocacy') {
        $strMenuTitle = 'policy';
    }
	?>
					<a data-ref="<?php print $menu_key; ?>" title="<?php print $strMenuTitle ?>" href="#"><?php print $strMenuTitle ?></a>
				</li>
	<?php
	$menu_index++;
endforeach;
?>
			</ul>
		</div>
		<nav id="secondary-nav" class="site__nav">
			<div class="nav__below row-flush">
				<div class="nav__secondary span4">
					<ul id="secondary-nav-list"></ul>
				</div>
				<aside class="nav__aside span8 l">
					<div class="inner">
						<div id="tertiary-nav-lists" class="row--nav"></div>
					</div>
				</aside>
			</div>
		</nav>
	</header>
<?php
print $page_top;
print $page;
print $page_bottom;
include drupal_get_path( 'theme', 'zero' ) . '/templates/partials/footer-ad.tpl.inc';
?>
	<footer class='site__footer'>
		<div class="footer__top container">
			<h1 id="footer-brand"><a href='/'><?php print $head_title_array['name']; ?></a></h1>
				<?php if ( isset( $site_info_fields['field_location'] ) && isset( $site_info_fields['field_phone'] ) && isset( $site_info_fields['field_email'] ) ) : ?>
			<p class="footer__contact">
            <?php print '<span>' .
                  $site_info_fields['field_location'] . '<span class="non-mobile">•</span>' .
                  $site_info_fields['field_phone'] . '<span>•</span><a href="mailto:' .
                  $site_info_fields['field_email'].'" target="_top">'.
                  $site_info_fields['field_email'].'</a><span>•</span><a href="'.
                  $site_info_fields['field_privacy_policy'].'">Privacy Policy</a>';
            ?></p>
				<?php endif; ?>
			<ul class="nav -social group">
				<?php foreach ( $site_info_fields['field_social_links'] as $social_link_key => $social_link ) : ?>
					<li class='nav__item'><a href='<?php print $social_link['field_url']; ?>' target="_blank"><i class='icon -<?php print strtolower( $social_link['field_heading'] ); ?>'><?php print ucwords( $social_link['field_heading'] ); ?></i></a></li>
				<?php endforeach; ?>
			</ul>
		</div>
		<p class='footer__end'>&copy;<?php print $head_title_array['name']; ?> <?php echo date( 'Y' ); ?>. All rights reserved.</p>
	</footer>
	<script type="text/javascript" <?php echo($strInlineNonce); ?>>
		var mainMenuData = <?php print json_encode( $main_menu_data_array ); ?>
	</script>
	<script id="secondary-nav-template" type="text/template" <?php echo($strInlineNonce); ?>>
		<% _.each( data, function ( link, index ) { %>
			<% if ( index === 0 ) { %>
				<li class="nav__item -below f<% if ( link.is_active ) {%> -active<% } %>">
			<% } else if ( index === data.length - 1 ) { %>
				<li class="nav__item -below l<% if ( link.is_active ) {%> -active<% } %>">
			<% } else { %>
				<li class="nav__item -below<% if ( link.is_active ) {%> -active<% } %>">
			<% } %>
			<a data-ref="<%= link.title %>" href="<%= link.path %>"><%= link.title %></a></li>
		<% } ); %>
	</script>
	<script id="tertiary-nav-template" type="text/template" <?php echo($strInlineNonce); ?>>
		<% _.each( data, function ( list, index ) { %>
			<% if ( index === 0 ) { %>
				<ul class="span4 f">
			<% } else if ( index === data.length - 1 ) { %>
				<ul class="span4 l">
			<% } else { %>
				<ul class="span4">
			<% } %>
				<% _.each( list, function ( item, itemIndex ) { %>
					<% if ( itemIndex === 0 ) { %>
						<li class="nav__item -aside f<% if ( item.is_active ) {%> -active<% } %>">
					<% } else if ( itemIndex === list.length - 1 ) { %>
						<li class="nav__item -aside l<% if ( item.is_active ) {%> -active<% } %>">
					<% } else { %>
						<li class="nav__item -aside<% if ( item.is_active ) {%> -active<% } %>">
					<% } %>
					<a href="<%= item.path %>"><%= item.title %></a></li>
				<% } ); %>
			</ul>
		<% } ); %>
	</script>
  <script type="text/javascript" <?php echo($strInlineNonce); ?>>
      var strLoc = new String(window.top.location);
      strLoc = strLoc.toLowerCase();

      if (strLoc.indexOf("/drdev.leadingage.org") <= 0) {
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-1184646-15', 'auto');
          ga('require', 'displayfeatures');
          ga('send', 'pageview');
      }
  </script>
  
<?php
  if ( empty( $strServerHost ) || ( !empty( $strServerHost ) && $strServerHost !== $strSandboxHost ) ) {
?>

  <script type="text/javascript" <?php echo($strInlineNonce); ?>>
  (function() {
    var didInit = false;
    function initMunchkin() {
      if(didInit === false) {
        didInit = true;
        Munchkin.init('234-PNF-634');
      }
    }
    var s = document.createElement('script');
    s.type = 'text/javascript';
    s.async = true;
    s.src = '//munchkin.marketo.net/munchkin.js';
    s.onreadystatechange = function() {
      if (this.readyState == 'complete' || this.readyState == 'loaded') {
        initMunchkin();
      }
    };
    s.onload = initMunchkin;
    document.getElementsByTagName('head')[0].appendChild(s);
  })();
  </script>
  
<!-- Facebook Pixel Code -->
<script <?php echo($strInlineNonce); ?>>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
// Insert Your Facebook Pixel ID below. 
fbq('init', '248570442301510');
fbq('track', 'PageView');
</script>
<!-- Insert Your Facebook Pixel ID below. --> 
<noscript><img height="1" width="1" class="display-none-hiding"
src="https://www.facebook.com/tr?id=248570442301510&amp;ev=PageView&amp;noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- BEGIN: Assoc. tracking for Josh -->
<script <?php echo($strInlineNonce); ?>>
  gtag('event', 'conversion', {
    'allow_custom_scripts': true,
    'send_to': 'DC-9036391/homep0/https0+standard'
  });
</script>
<noscript>
<img src="https://ad.doubleclick.net/ddm/activity/src=9036391;type=homep0;cat=https0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;tfua=;npa=;ord=1?" width="1" height="1" alt=""/>
</noscript>
<!-- END: Assoc. tracking for Josh -->

<!-- Piwik --><script type="text/javascript" <?php echo($strInlineNonce); ?>>  var _paq = _paq || [];  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */  _paq.push(["setDomains", ["*.leadingage.org"]]);  _paq.push(["setCampaignNameKey", "CID"]);  _paq.push(['trackPageView']);  _paq.push(['enableLinkTracking']);  (function() {    var u="//www.rumiview.com/";    _paq.push(['setTrackerUrl', u+'piwik.php']);    _paq.push(['setSiteId', '8267']);    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);  })();</script><!-- End Piwik Code -->
<!-- Lotame -->
<script src="https://tags.crwdcntrl.net/c/12671/cc.js?ns=_cc12671" id="LOTCC_12671" <?php echo($strInlineNonce); ?>></script>
<script <?php echo($strInlineNonce); ?>>
_cc12671.add("seg","17.12.0");
_cc12671.add("seg","17.13.8");
_cc12671.add("seg","17.13.9");
_cc12671.add("seg","17.13.12");
_cc12671.add("seg","17.16.11");
_cc12671.add("seg","17.16.12");
_cc12671.add("seg","17.16.19");
_cc12671.add("seg","17.16.20");
_cc12671.add("seg","17.16.32");
_cc12671.add("seg","17.16.33");
_cc12671.add("seg","17.16.34");
_cc12671.add("seg","17.16.35");
_cc12671.add("seg","17.16.36");
_cc12671.add("seg","17.32.3");
_cc12671.add("seg","17.32.16");
_cc12671.add("seg","17.41.17");
_cc12671.add("seg","17.41.43");
_cc12671.add("seg","17.41.44");
_cc12671.add("seg","LA");
_cc12671.bcp();
</script>
<!-- Static Tag -->
<img src="https://bcp.crwdcntrl.net/5/c=12671/seg=17.12.0/seg=17.13.8/seg=17.13.9/seg=17.13.12/seg=17.16.11/seg=17.16.12/seg=17.16.19/seg=17.16.20/seg=17.16.32/seg=17.16.33/seg=17.16.34/seg=17.16.35/seg=17.16.36/seg=17.32.3/seg=17.32.16/seg=17.41.17/seg=17.41.43/seg=17.41.44/seg=LA" width="1" height="1"/>
<!-- End Lotame -->

<?php
      if (trim(request_path()) != '') {
        //This is any page other than the homepage
      }
  }
?>
  
<?php print $scripts; ?>

<?php
  $nid1 = 0;  
  if ( !empty($node) ) {
    $nid1 = $node->nid;
  } else if ($node1 = menu_get_object()) {
    $nid1 = $node1->nid;
  }

  if ($nid1 == 58968 || $nid1 == 58974 || $nid1 == 62805) {
  ?>
  <script type="text/javascript" language="javascript" src="/sites/all/themes/zero/assets/js/ts-jquery.js"></script>
  <script type="text/javascript" language="javascript" src="/sites/all/themes/zero/assets/js/ts-jquery.dataTables.min.js"></script>
  <script type="text/javascript" charset="utf-8" <?php echo($strInlineNonce); ?>>
    $(function() {
        $('#tablesort_system').dataTable({
          "iDisplayLength": 25
        }).fnSort( [ [0,'desc'] ] );
    });

    function browsebycat(findTerm) {
        $('input:text').focus().val(findTerm).trigger("keyup");
    }
  </script>
  <?php
  }
?>
</body>
</html>
