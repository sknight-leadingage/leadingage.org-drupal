<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array ): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array ): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php ):
 * - $title_prefix (array ): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array ): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array ): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node ).
 * - $action_links (array ): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345 ).
 * - $type: describes the content type currently being processed.
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
 
function startsWith($haystack, $needle)
{
     $length = strlen($needle);
     return (substr($haystack, 0, $length) === $needle);
}

function cachet($needle) {
    $cachedEventText = $specialeventText = "";
    cache_clear_all($needle, 'cache', FALSE);
    
    if ($cachedEventText = cache_get($needle)) {
      if ($cachedEventText->expire > REQUEST_TIME) {
          $specialeventText = $cachedEventText->data;
      }
    }

    if ( empty( $specialeventText ) ) {
      $expires = REQUEST_TIME + 28800;
      if ($needle == 'seAMSessions1') {
          $specialeventText = file_get_contents("https://hweb.leadingage.org/annualmeeting/education-sessions/");
        } else if ($needle == 'seAMSessionsTracks') {
          $specialeventText = file_get_contents("https://hweb.leadingage.org/annualmeeting/education-sessions/default.aspx?sid=1");
        } else if ($needle == 'seAMSpeakers') {
          $specialeventText = file_get_contents("https://hweb.leadingage.org/annualmeeting/speaker-listing/");
      }
      
      if ($needle == 'sePEAKSessions1') {
          $specialeventText = file_get_contents("https://hweb.leadingage.org/peak/education-sessions/");
        } else if ($needle == 'sePEAKSessionsTracks') {
          $specialeventText = file_get_contents("https://hweb.leadingage.org/peak/education-sessions/default.aspx?sid=1");
        } else if ($needle == 'sePEAKSpeakers') {
          $specialeventText = file_get_contents("https://hweb.leadingage.org/peak/speaker-listing/");
      }
      
      cache_set($needle, $specialeventText, 'cache', $expires);
    }
    
    return $specialeventText;
}
 
$basicPost = $relatedPostsSidebar = $pagerSidebar = $subMenu = $metaInfo = $sessionList = $inheritAds = $speakerSummary = false;

if ( ! empty( $wrapper ) ){
	$content = $wrapper->value();
}
// dpm( $content );

if ( ! empty( $type ) && 'provider' === $type ) :
	require_once drupal_get_path( 'theme', 'zero' ) . '/templates/page/page--landing-page.tpl.php';
elseif ( ! empty( $type ) && 'student_portal' === $type ) :
	require_once drupal_get_path( 'theme', 'zero' ) . '/templates/page/page--student-portal.tpl.php';
else :
	if ( ! empty( $type ) ){
		if ( 'newsroom_post' === $type || 'blog_post' === $type || 'event' === $type || 'magazine_article' === $type || 'center_post' === $type || 'education_spotlight_post' === $type ) {
			$basicPost = true;
		}
		if ( 'newsroom_post' === $type || 'blog_post' === $type || 'event' === $type || 'center_post' === $type || 'education_spotlight_post' === $type ) {
			$relatedPostsSidebar = true;
		} elseif ( 'magazine_article' === $type ) {
			$pagerSidebar = true;
		}
		if ( 'magazine_article' === $type || 'magazine_issue' === $type || 'special_event' === $type || 'special_event_page' === $type || 'special_event_grid_page' === $type ) {
			$subMenu = true;
			
			if ( 'special_event_page' === $type && ! empty( $content->field_hidesubmenu['und'] ) ) {
          $subMenu = !($content->field_hidesubmenu['und'][0]['value']);
			}
		}
		if ( 'magazine_article' === $type ) {
			$metaInfo = true;
		}
		if ( 'special_event_page' === $type || 'special_event_grid_page' === $type ) {
			$sessionList = field_get_items( 'node', $content, 'field_list_sessions_' )[0]['value'];
			if ( ! empty( $content->field_sidebar['und'] ) ) {
        $inheritAds = $content->field_sidebar['und'][0]['value'];
      } else {
        $inheritAds = true;
      }
		}
		if ( 'general_session' === $type ) {
			$inheritAds = $basicPost = $speakerSummary = true;
		}
    } else {
      $basicPost = true;
      $type = '';
	}
	?>
<div id='page-wrapper'>
  <div id='page'>
	<div id='header'>
	
	<?php
	if ( false && $logo ) :?>
		<a href='<?php print $front_page; ?>' title='<?php print t( 'Home' ); ?>' rel='home' id='logo'>
		  <img src='<?php print $logo; ?>' alt='<?php print t( 'Home' ); ?>' />
		</a>
	<?php
	endif;
	print render( $page['header'] );?>
	</div> <!-- /.section, /#header -->
	<?php
	if ( ! $basicPost ) {
		require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/collections/hero.tpl.inc';
	}
	if ( $main_menu || $secondary_menu ) :?>
	<div id='navigation'>
	  <div class='section'>
		<?php
		print theme( 'links__system_main_menu', array(
			'links' => $main_menu,
			'attributes' => array(
			'id' => 'main-menu',
			'class' => array(
				'links',
				'inline',
				'clearfix',
				)
			),
			'heading' => t( 'Main menu' ) ) );
		print theme( 'links__system_secondary_menu', array(
			'links' => $secondary_menu,
			'attributes' => array(
				'id' => 'secondary-menu',
				'class' => array(
					'links',
					'inline',
					'clearfix',
					)
				),
				'heading' => t( 'Secondary menu' )
			) );
		?>
	  </div>
	</div> <!-- /.section, /#navigation -->
	<?php
	endif;
	if ( $metaInfo ) {
		require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/collections/meta-info.tpl.inc';
	}
	if ( $subMenu ) {
		require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/collections/menu.tpl.inc';
	}
	print $messages;
	?>
	<div id='main-wrapper'>
	  <div id='main' class='clearfix container row--main'>
		<?php
		
		$query_params = drupal_get_query_parameters();
		if ( current_path() == 'search-files' && (! empty ( $query_params['filename'] ) || ! empty ( $query_params['caption'] ) ) ) {
        $searchText = '';
        if ( ! empty ( $query_params['filename'] ) ) {
          $searchText = $query_params['filename'];
         } else {
          $searchText = $query_params['caption'];
        }
        echo '<h1 class="search-results">Search documents</h1>';
        echo "<p>We've searched our content for documents whose names match <strong>$searchText</strong> and the results are below. If you'd like to expand your search, try one of these:</p>";
        echo '<ul>';
        if ( startsWith( trim ( $searchText ), "\"" ) ) {
            $searchText = str_replace( "\"", "", $searchText);
            echo '<li><a href="/search-files?filename=' . $searchText . '&caption=' . $searchText . '" title="document search">Perform another document search where the filenames contain <i>any of the words</i> <strong>' . $searchText . '</strong></a></li>';
          } else {
            echo '<li><a href="/search-files?filename=%22' . $searchText . '%22&caption=%22' . $searchText . '%22" title="document search">Perform another document search where the filenames must contain <i>all the words</i> <strong>"' . $searchText . '"</strong></a></li>';
        }
        echo '<li><a href="/search-page?title=' . $searchText . '&fulltext=' . $searchText . '" title="web content search">Perform a web content search for <strong>' . $searchText . '</strong></a></li>';
        echo '</ul>';
		}
		
		$span = '';
		if ( ! empty( $relatedPostsSidebar ) && $relatedPostsSidebar ) {
			$span = 'span7';
			require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/collections/related-posts.tpl.inc';
		} elseif ( ! empty( $pagerSidebar ) && $pagerSidebar ) {
			$span = 'span7';
			require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/collections/pager-sidebar.tpl.inc';
		} elseif ( ( ! empty( $content->field_sidebar['und'] ) && $content->field_sidebar['und'][0]['value'] ) || $inheritAds ){
			$span = 'span7';
		}
		?>
		<div id='content' class='column <?php print $span; ?>'<?php if ($type === 'special_event_page' || $type === 'special_event' || $type === 'special_event_grid_page') { echo( ' style="font-family: freight-sans-pro,Helvetica,Arial,sans-serif;"' ); } ?>>
		  <div class='section'<?php if ($type === 'staff_page') { echo( ' style="display: none;"' ); } ?>>
		<?php if ( $page['highlighted'] ) :?>
			<div id='highlighted'><?php print render( $page['highlighted'] ); ?></div>
		<?php endif; ?>
			<a id='main-content'></a>
		<?php
		if ( $tabs ) :?>
			<div class='tabs'><?php print render( $tabs ); ?></div>
		<?php
		endif;
		print render( $page['help'] );
		if ( $action_links ) :?>
			<ul class='action-links'><?php print render( $action_links ); ?></ul>
		<?php
		endif;
		
		require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/page-member-login-messages.tpl.inc';
		
    $requireMember = $userIsMember = false;
    if (isset($user) && in_array('Member', $user->roles)) {
      $userIsMember = true;
    }

    if ( isset($content) && !empty( $content->field_members_only['und'][0]['value'] ) && $content->field_members_only['und'][0]['value'] == '1' ) {
      $requireMember = true;
    }
      
    if ( ! empty( $page['content']['system_main']['nodes'] ) && is_array( $page['content']['system_main']['nodes'] ) ) {
      $pageContent = reset( $page['content']['system_main']['nodes'] );
    }
    
    if ( empty( $type ) ) {
      if (($requireMember === FALSE) || ($requireMember === TRUE && $userIsMember === TRUE)) {
        print render( $page['content'] );
      } else if ($requireMember === TRUE && $userIsMember === FALSE) {
        if ($logged_in === TRUE) {
          outTextNotMember();
        } else {
          outTextLogIn();
        }
      }
    }

		if ( $sessionList ) {
			require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/collections/sessions-feed.tpl.inc';
		}
		if ( ! empty( $basicPost ) && $basicPost ) {
			require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/collections/post-intro.tpl.inc';
		}
		if ( ! empty( $speakerSummary ) && $speakerSummary ) {
			require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/collections/summaries-feed.tpl.inc';
		}
		if ( ! empty( $content->body['und'][0]['safe_value'] ) && ( $type !== 'staff' && $type !== 'staff_page' ) ) {

      if (($requireMember === FALSE) || ($requireMember === TRUE && $userIsMember === TRUE)) {
      
        $pageContentOutput = $content->body['und'][0]['value'];
        $specialeventText = "";

        //Test for Annual Meeting
        $strTestAMSessions = '[SpecialEvent:AM-Sessions]';
        $strTestAMSessionsTracks = '[SpecialEvent:AM-Sessions-Tracks]';
        $strTestAMSpeakers = '[SpecialEvent:AM-Speakers]';
        
        if (strpos($pageContentOutput, $strTestAMSessions) !== false) {
          $specialeventText = cachet('seAMSessions1');
          $pageContentOutput = str_replace($strTestAMSessions, $specialeventText, $pageContentOutput);
        }
        
        if (strpos($pageContentOutput, $strTestAMSessionsTracks) !== false) {
          $specialeventText = cachet('seAMSessionsTracks');
          $pageContentOutput = str_replace($strTestAMSessionsTracks, $specialeventText, $pageContentOutput);
        }
        
        if (strpos($pageContentOutput, $strTestAMSpeakers) !== false) {
          $specialeventText = cachet('seAMSpeakers');
          $pageContentOutput = str_replace($strTestAMSpeakers, $specialeventText, $pageContentOutput);
        }
        
        //Test for PEAK
        $strTestAMSessions = '[SpecialEvent:PEAK-Sessions]';
        $strTestAMSessionsTracks = '[SpecialEvent:PEAK-Sessions-Tracks]';
        $strTestAMSpeakers = '[SpecialEvent:PEAK-Speakers]';
        
        if (strpos($pageContentOutput, $strTestAMSessions) !== false) {
          $specialeventText = cachet('sePEAKSessions1');
          $pageContentOutput = str_replace($strTestAMSessions, $specialeventText, $pageContentOutput);
        }
        
        if (strpos($pageContentOutput, $strTestAMSessionsTracks) !== false) {
          $specialeventText = cachet('sePEAKSessionsTracks');
          $pageContentOutput = str_replace($strTestAMSessionsTracks, $specialeventText, $pageContentOutput);
        }
        
        if (strpos($pageContentOutput, $strTestAMSpeakers) !== false) {
          $specialeventText = cachet('sePEAKSpeakers');
          $pageContentOutput = str_replace($strTestAMSpeakers, $specialeventText, $pageContentOutput);
        }
        
        print render( $pageContentOutput );
        
      } else if ($requireMember === TRUE && $userIsMember === FALSE) {
        if ($logged_in === TRUE) {
          outTextNotMember();
        } else {
          outTextLogIn();
        }
      }
		}
		
    if ( $type === 'special_event_grid_page' && !empty( $fields['field_buckets'] ) ) {
        if ( ! empty( $fields['field_buckets'] ) ) {
          $bucket_count = $wrapper->field_buckets->count( );
          if ( $bucket_count > 1 ) {
            require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/buckets.tpl.inc';
          }
        }
    }
    
    if ( ! empty( $content->body['und'][0]['safe_value'] ) && ( $type !== 'staff' && $type !== 'staff_page' ) ) {
			print render( $pageContent['disqus'] );
			// Turning off flags on the front end for now:
			// $flags = flag_get_flags( 'node', $content->type );
			// foreach ( $flags as $key => $flag ) {
			//     print flag_create_link( $flag->name, $node->nid );
			// }
    }
				
		print $feed_icons;
		if ( ! empty( $type ) && 'staff' === $type ) {
			require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/collections/member.tpl.inc';
			print '</div>';
			require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/collections/authors-feed.tpl.inc';
		} else {
			print '</div>';
		}
		if ( ! empty( $type ) && 'staff_page' === $type ) :
			// Query all of the nids of a particular content type.
			$query = db_select( 'node', 'n' );
			$query->join( 'field_data_field_heading', 'h', 'n.nid = h.entity_id' );
			$query->fields( 'n', array( 'nid' ) );
			$query->condition( 'n.type', 'staff', '=' );
			$query->orderBy( 'h.field_heading_value', 'ASC' );
			
			$sUrlStaffComp = strtolower($_SERVER['REQUEST_URI']);
			$sUrlEndsWith1 = '/staff/teams';
			$sUrlEndsWith2 = '/staff/teams/';
			
			if (
            (substr_compare($sUrlStaffComp, $sUrlEndsWith1, strlen($sUrlStaffComp)-strlen($sUrlEndsWith1), strlen($sUrlEndsWith1)) === 0)
            ||
            (substr_compare($sUrlStaffComp, $sUrlEndsWith2, strlen($sUrlStaffComp)-strlen($sUrlEndsWith2), strlen($sUrlEndsWith2)) === 0)
          ) {
          $query = db_select( 'node', 'n' );
          $query->join( 'field_data_field_heading', 'h', 'n.nid = h.entity_id' );
          $query->join( 'field_data_field_team', 't', 'n.nid = t.entity_id' );
          $query->fields( 'n', array( 'nid' ) );
          $query->condition( 'n.type', 'staff', '=' );
          $query->orderBy( 't.field_team_value, h.field_heading_value', 'ASC' );
			}
						
			$results = $query->execute();
			while ( $result = $results->fetchCol() ){
				$nids = $result;
			}
			// Get all of the article nodes.
			$nodes = node_load_multiple( $nids );
			require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/collections/card-grid.tpl.inc';
		endif;
		?>
		</div> <!-- /.section, /#content -->
		<?php
		if ( $page['sidebar_first'] ) :
		?>
		<div id='sidebar-first' class='column sidebar'>
		  <div class='section'>
			<?php print render( $page['sidebar_first'] ); ?>
		  </div>
		</div> <!-- /.section, /#sidebar-first -->
		<?php
		endif;
		if ( $page['sidebar_second'] ) :
		?>
		<div id='sidebar-second' class='column sidebar'>
		  <div class='section'>
			<?php print render( $page['sidebar_second'] ); ?>
		  </div>
		</div> <!-- /.section, /#sidebar-second -->
		<?php
		elseif ( ( ! empty( $content->field_sidebar['und'] ) && $content->field_sidebar['und'][0]['value'] ) || $inheritAds ) :
			if ( $inheritAds ) {
				$parentId = $content->field_parent_event['und'][0]['target_id'];
				$adContent = node_load( $parentId );
			} else {
				$adContent = $content;
			}
		?>
		<aside class='site__sidebar span3 l'>
		  <div class='section'>
		  <?php
			  if (! empty( $adContent->field_sidebar_title['und'][0]['value'] ) ) {
            echo "<h2 class='-section'>" . $adContent->field_sidebar_title['und'][0]['value'] . '</h2>';
        }
			?>
			<a href='<?php echo $adContent->field_sidebar_link['und'][0]['value'] ?>'><img src='<?php echo file_create_url( $adContent->field_sidebar_image['und'][0]['uri'] ) ?>'></a>
		  </div>
		  <h3><?php if ( !empty($adContent->field_sidebar_text['und'][0]['value']) ) { echo $adContent->field_sidebar_text['und'][0]['value']; } ?></h3>
		</aside> <!-- /.section, /#sidebar-second -->
		<?php
		elseif ( ! empty( $ads_fields ) ) :
			// print '<aside class='side__sidebar span3 l'>';
			// require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/sidebar-ad.tpl.inc';
			// print '</aside>';
		endif;

		?>
	  </div>
	</div> <!-- /#main, /#main-wrapper -->
	<div id='footer'>
	  <div class='section'>
		<?php print render( $page['footer'] ); ?>
	  </div>
	</div> <!-- /.section, /#footer -->
  </div>
</div> <!-- /#page, /#page-wrapper -->
<?php
endif;