<?php
/**
 * @file
 * Landing Page template.
 */
// dpm( '$path' );
$content = $wrapper->value();
// dpm( $fields );
$introClasses = '';
$span = 'span10';
$calendarEvents = array();
if ( ! empty( $fields['field_events_calendar'] ) && $fields['field_events_calendar'] ) {
	$introClasses .= ' -section';
	$now = getdate( )[0];
	$result = db_query( "SELECT nid FROM {node} JOIN {field_data_field_date} ON node.nid = field_data_field_date.entity_id LEFT JOIN {field_data_field_pinned} ON node.nid = field_data_field_pinned.entity_id WHERE node.type = 'event' AND field_data_field_date.field_date_value > :now AND node.status = 1 ORDER BY IFNULL(field_data_field_pinned.field_pinned_value, 0) DESC, field_data_field_date.field_date_value ASC", array( ':now' => $now ) );
	foreach ( $result as $element ) {
		array_push( $calendarEvents, node_load( $element->nid ) );
	}
}
print $messages;
if ( $tabs && ! empty( $tabs['#primary'] ) ) :
?>
<div class='tabs'><?php print render( $tabs ); ?></div>
<?php
endif;
require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/collections/hero.tpl.inc';
?>

<?php
/**
 * Body.
 */
/**
 * Featured Section.
 *
 * Including buckets.
 * @todo
 *   move this logic to a field processor inc file?
 */
// dpm( $fields );


global $user;

require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/page-member-login-messages.tpl.inc';

$requireMember = $userIsMember = false;
if (isset($user) && in_array('Member', $user->roles)) {
  $userIsMember = true;
}

if ( isset( $wrapper ) && isset( $wrapper->field_members_only ) && !empty( $wrapper->field_members_only->value() ) && $wrapper->field_members_only->value() == '1' ) {
  $requireMember = true;
}

if (($requireMember === FALSE) || ($requireMember === TRUE && $userIsMember === TRUE)) {


if ( ! empty( $fields['body'] ) ) {
	print '<section class=\'container -intro\'>';
	print preg_replace( '/<p>/', '<p class="intro'.$introClasses.'">', $wrapper->body->value( )['safe_value'] );
	print '</section>';
}
if ( ! empty( $fields['field_buckets'] ) ) {
	$bucket_count = $wrapper->field_buckets->count( );
	if ( $bucket_count > 1 ) {
		require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/buckets-student-portal.tpl.inc';
	}
}

/************ topline */

if ( ! empty( $fields['field_wf_topline_blocks'] ) ) {
	$sr_wf_topline_count = $wrapper->field_wf_topline_blocks->count( );
	if ( $sr_wf_topline_count > 1 ) {
		require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/buckets-wf-topline-blocks.tpl.inc';
	}
}

/************ midline */

if ( ! empty( $fields['field_sidebar_image'] ) ) {
  print '<div class="row--fwb container clone -parent"><div style="padding-top: 75px; padding-bottom: 25px;"><div style="float: none; clear: both; margin: auto; width: 100%; height: 300px; border: 2px solid #101010;"><center>';
  print '  <div style="text-align: center;">';
  print '   <div style="float: left; width: 49%; height: 300px;"><img src="' . file_create_url( $fields['field_sidebar_image']['uri'] ) . '" alt="image" style="height: 300px;" /></div>';
  
  if ( !empty( $fields['field_caption'] ) && !empty( $fields['field_button'] ) && !empty( $fields['field_url_2'] ) ) {
    print '  <div style="float: left; width: 49%; height: 300px;"><div style="padding-top: 10%;"><center>';
    print '     <h2>' . $fields['field_caption'] . '</h2>';
    print '     <div style="padding-top: 26px;"><a href="' . $fields['field_url_2'] . '" class="btn -main">' . $fields['field_button'] . '</a></div>';
    print '  </center></div></div>';
  }
  else {
    if ( !empty( $fields['field_caption'] ) ) {
      print '  <div style="float: left; width: 49%;">';
      print '     <h2>' . $fields['field_caption'] . '</h2>';
      print '  </div>';
    }
    if ( !empty( $fields['field_button'] ) && !empty( $fields['field_url_2'] ) ) {
      print '  <div style="float: left; width: 49%;">';
      print '     <h2><a href="' . $fields['field_url_2'] . '" class="btn -main">' . $fields['field_button'] . '</a></h2>';
      print '  </div>';
    }
    if ( empty( $fields['field_caption'] ) && empty( $fields['field_button'] ) && empty( $fields['field_url_2'] ) ) {
      print '  <div style="float: left; width: 49%;">&nbsp;</div>';
    }
  }

  print '  </div>';
  print '</center></div></div></div><div style="float: none; clear: both;"></div>';
}


/************ student resources */

if ( ! empty( $fields['field_heading_2'] ) ) {
  print '<div style="text-align: center; padding-top: 52px; padding-bottom: 42px;"><h2> ' . $fields['field_heading_2'] . ' </h2></div>';
}

if ( ! empty( $fields['field_student_resources'] ) ) {
	$sr_bucket_count = $wrapper->field_student_resources->count( );
	if ( $sr_bucket_count > 1 ) {
		require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/buckets-student-portal-resources.tpl.inc';
	}
}


/************ Callout Header/Banner */

if ( ! empty( $fields['field_heading_3'] ) ) {
  print '<div class="row--fwb container clone -parent">';
  print '<div style="text-align: center; padding-top: 30px; padding-bottom: 30px;"><div style="text-align: center; padding-top: 30px; padding-bottom: 30px; background-color: #BABABA;"><h2> ' . $fields['field_heading_3'] . ' </h2></div></div>';
  print '</div>';
}



/************ careers videos */

if ( ! empty( $fields['field_careers_videos_blocks'] ) ) {
	$cvideos_bucket_count = $wrapper->field_careers_videos_blocks->count( );
	if ( $cvideos_bucket_count > 1 ) {
		require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/buckets-careers-videos.tpl.inc';
	}
}

if ( ! empty( $fields['field_intro'] ) ) {
	print '<section class=\'container\'>';
	print $wrapper->field_intro->value( )['safe_value'];
	print '</section>';
}

?>
<main class='site__main'>
  <section class='row--main container'>
<?php
if ( ! $is_magazine_page && empty( $wrapper->value()->field_feed ) &&  empty( $fields['field_feed_content_types'] ) && empty( $fields['field_filterable_feed'] ) && ! empty( $fields['field_is_blog'] ) && ! $fields['field_is_blog'] && 'provider' !== $content->type  ) :
	else :
		?>
	<article class='span7'>
		<?php
		/**
		 * Feed.
		 */
		 
      if ( ! empty( $fields['field_buckets'] ) ) {
        $bucket_count = $wrapper->field_buckets->count( );
        if ( $bucket_count <= 1 ) {
          $span = 'span3';
          print '<section class="box -main">';
          require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/items/section-header.tpl.inc';
          require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/collections/standalone-summary.tpl.inc';
          require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/items/content-footer.tpl.inc';
          print '</section>';
        }
      }
      // Optional boxes Section:
      $printBoxes = false;
      $entity_data = $wrapper->value( );
      $final_data = get_object_vars( $entity_data );
      if ( ! empty( $wrapper->value( )->field_feed ) ) {
        $optionalContent = $wrapper->value( )->field_feed['und'][0];
        $modifyArgs = false;
        // dpm( explode( '|', $optionalContent['vname'] )[0] );
        switch ( explode( '|', $optionalContent['vname'] )[0] ) {
          case 'blog_posts':
            $printBoxes = true;
            break;
          case 'newsroom':
            $printBoxes = true;
            $categoryNid = array( '25' );
            break;
          case 'news_areas':
            $printBoxes = true;
            $tid = field_get_items( 'node', $wrapper->value(), 'field_feed_term' )[0]['tid'];
            $result = db_query("SELECT DISTINCT node.nid FROM node
              LEFT JOIN taxonomy_index
              ON taxonomy_index.nid = node.nid
              WHERE taxonomy_index.tid = :tid
              AND node.type = 'newsroom_post'
              AND node.status = '1'
              ORDER BY node.created DESC
              LIMIT 4" ,
              array(':tid' => $tid )
            );
            $results = $result->fetchAll();
            $nodes = array();
            foreach ( $results as $key => $value ) {
              array_push( $nodes, $value->nid );
            }
            break;
          case 'senior_action_network_news_area':
            $printBoxes = true;
            break;
          default:
            // print 'none found';
        }
      }
      if ( $printBoxes && empty( $nodes ) ) {
        $span = 'span3';
        $articles_view_setup = new Nil\View( 'senior-action-network', $categoryNid );
        if ( $modifyArgs ) {
          $articles_view_setup->modArgs( $categoryNid );
        }
        $articles_view = $articles_view_setup->getView( 4 );

        foreach ( $articles_view as $view_key => $row ) {
          $article_nids[ $view_key ] = $row->tid;
        }
        $article_nodes = node_load_multiple( $article_nids );
        include drupal_get_path( 'theme', 'zero' ) . '/templates/partials/collections/boxes.tpl.inc';
      } elseif ( $printBoxes ) {
        $span = 'span3';
        include drupal_get_path( 'theme', 'zero' ) . '/templates/partials/collections/term-boxes.tpl.inc';
      }
      /**
       * Filterable Feed.
       */
      if ( ! empty( $fields['field_is_blog'] ) && $fields['field_is_blog'] ) {
        $span = 'span3';
        include drupal_get_path( 'theme', 'zero' ) . '/templates/partials/blog-feed.tpl.inc';
      } elseif( ! empty( $calendarEvents ) ){
        $span = 'span3';
        $results_array = $calendarEvents;
        $result_count = count( $results_array );
        include drupal_get_path( 'theme', 'zero' ) . '/templates/partials/collections/base-feed.tpl.inc';
      }elseif ( ! empty( $fields['field_feed_content_types'] ) || 'provider' === $content->type ){
        $span = 'span3';
        include drupal_get_path( 'theme', 'zero' ) . '/templates/partials/filterable-feed.tpl.inc';
      } elseif ( ! empty( $fields['field_feed'] ) ) {
        $span = 'span3';
        include drupal_get_path( 'theme', 'zero' ) . '/templates/partials/feed.tpl.inc';
      }


  ?>
	</article>
	<aside class='side__sidebar l <?php print $span; ?>'>
		<?php
		if ( ! empty( $ads_fields ) ) :
			include drupal_get_path( 'theme', 'zero' ) . '/templates/partials/sidebar-ad.tpl.inc';
		endif;
		?>
	</aside>
		<?php
	endif;
	
      } else if ($requireMember === TRUE && $userIsMember === FALSE) {
        print "<article>\n<div style=\"padding: 50px 10px 120px 20px;\">\n";
        if ($logged_in === TRUE) {
          outTextNotMember();
        } else {
          outTextLogIn();
        }
        print "\n</article>\n";
      }
	
	// Turning off flags on the front end for now:
	// if ( ! empty( $node->nid ) ){
	// 	$flags = flag_get_flags( 'node', $content->type );
	// 	foreach ( $flags as $key => $flag ) {
	// 		print flag_create_link( $flag->name, $node->nid );
	// 	}
	// }
	?>
  </section>
	<?php
	/**
	 * Block.
	 */
	if ( ! empty( $fields['field_block'] ) ) {
		require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/standard-block.tpl.inc';
	}
	?>
</main>
