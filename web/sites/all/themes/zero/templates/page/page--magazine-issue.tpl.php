<?php
/**
 * @file
 * Page content type template.
 */
// dpm( $fields );
// dpm( $wrapper->value() );
print $messages;
if ( $tabs && ! empty( $tabs['#primary'] ) ) :
?>
<div class='tabs'><?php print render( $tabs ); ?></div>
<?php
endif;
print render( $page['help'] );
?>
<article class='hero -complex'>
<?php
/**
 * Banner Image.
 */
if ( ! empty( $fields['field_image'] ) ) :
?>
	<img class='hero__bg' src='<?php print file_create_url( $fields['field_image']['uri'] ); ?>' alt='<?php print $wrapper->title->value(); ?>' />
<?php
else :
	?>
	<img class='hero__bg' src='<?php print '/' . drupal_get_path( 'theme', 'zero' ) . '/docs/assets/img/samples/hero_homepage.jpg'; ?>' alt='Sample image for homepage hero' />
	<?php
endif;
?>
	<div class='hero__content'>
		<h2 class='-section'>LeadingAge Magazine</h2>
		<div class='inner'>
			<h6 class='meta'>
	<?php
	if ( ! empty( $fields['field_issue'] ) ) {
		print $wrapper->field_issue->value();
	}
	if ( ! empty( $fields['field_volume'] ) ) {
		print '&nbsp;&nbsp;•&nbsp;&nbsp;Volume ';
		print sprintf( '%02d', $wrapper->field_volume->value() );
	}
	if ( ! empty( $fields['field_number'] ) ) {
		print '&nbsp;&nbsp;•&nbsp;&nbsp;Number ';
		print sprintf( '%02d', $wrapper->field_number->value() );
	}
	?>
			</h6>
			<h1 class='other -alt'><?php print $wrapper->title->value(); ?></h1>
	<?php
	if ( ! empty( $fields['field_intro'] ) && isset( $fields['field_intro']['safe_value'] ) ) :
		print preg_replace( '/<p>/', '<p class="intro">', $wrapper->field_intro->value()['safe_value'] );
	endif;
	?>
		</div>
	</div>
</article>
<?php require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/collections/menu.tpl.inc'; ?>
<main class='site__main'>
	<section class='row--main container'>
		<article class='span7'>
<?php
if ( ! empty( $fields['field_heading'] ) ) :
	?>
			<h2 class='-section'><?php print $wrapper->field_heading->value(); ?></h2>
	<?php
else :
	?>
			<h2 class='-section'>Magazine Features</h2>
	<?php
endif;
$articles_view_setup = new Nil\View( 'magazine_articles|default', $wrapper->getIdentifier() );
$articles_view = $articles_view_setup->getView( 0 );
foreach ( $articles_view as $view_key => $row ) {
		$article_nids[ $view_key ] = $row->nid;
}
$article_nodes = node_load_multiple( $article_nids );
if ( ! empty( $article_nodes ) ) {
		require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/magazine-summary-list.tpl.inc';
}
?>
		</article>
		<aside class='side__sidebar span3 l'>
<?php
if ( ! empty( $ads_fields ) ) :
		require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/sidebar-ad.tpl.inc';
		require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/collections/sidebar-advertizers.tpl.inc';
endif;
?>
		</aside>
	</section>
</main>
