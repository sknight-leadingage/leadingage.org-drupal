<?php

  $content = $wrapper->value();
  $pTitle = $content->title;
  $pBody = $content->body['und']['0']['value'];
  $pUrl = $content->field_content_library_link['und']['0']['value'];
        
?>
<div id='page-wrapper'>
  <div id='page'>
	<div id='header'>
	</div> <!-- /.section, /#header -->
	<article class="hero -simple">
    <div class="hero__content__short">
					<h1 class="-hero"><?php echo( $pTitle ); ?></h1>
		</div>
  </article>
	<section class="container -intro">
      <?php
        
        echo("<p>$pBody</p>\n");
        echo("<p><a href=\"$pUrl\" title=\"$pTitle\">View in the Learning Hub</a></p>\n");
      
      ?>
  </section>