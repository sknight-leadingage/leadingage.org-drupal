<?php
/**
 * @file
 * Page content type template.
 */
if ( $is_magazine_page ) :
	require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/collections/menu.tpl.inc';
endif;
print $messages;
if ( $tabs && ! empty( $tabs['#primary'] ) ) :
	?>
<div class='tabs'><?php print render( $tabs ); ?></div>
	<?php
endif;
print render( $page['help'] );
?>
<main class='site__main'>
	<section class='row--main container test'>
<?php
if ( isset($wrapper) && !empty( $wrapper->field_exclude_sidebar->value() ) && $wrapper->field_exclude_sidebar->value() == '1' ) {
    echo("		<article>");
  } else {
    echo("		<article class='span7'>");
}
?>
			<header class='section -marked'>
<?php
if ( ! empty( $fields['title'] ) ) : ?>
				<h1><?php print $wrapper->title->value(); ?></h1>
<?php
$breadcrumb = drupal_get_breadcrumb();
$breadcount = count($breadcrumb);
if ($breadcount >= 2) {
  $bcunder = '';
  if ($breadcount >= 3) {
    $bcunder = ', under: ' . $breadcrumb[2];
  }
  print ('<div style="padding: 0 0 24px 0;">Part of: ' . $breadcrumb[1] . $bcunder . '</div>'."\n");
}
else {
  print ("\n<!-- breadcrumb: " . print_r($breadcrumb, true) . " -->\n");
}
endif;
if ( ! empty( $fields['field_intro'] ) ) :
		print preg_replace( '/<p>/', '<p class="intro">', $wrapper->field_intro->value()['safe_value'] );
endif;
?>
			</header>
<?php
    global $user;
    
    require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/page-member-login-messages.tpl.inc';

if ( ! empty( $fields['body'] ) ) :

    $requireMember = $userIsMember = false;
    if (isset($user) && in_array('Member', $user->roles)) {
      $userIsMember = true;
    }
    
    if ( isset($wrapper) && !empty( $wrapper->field_members_only->value() ) && $wrapper->field_members_only->value() == '1' ) {
      $requireMember = true;
    }
    
      if (($requireMember === FALSE) || ($requireMember === TRUE && $userIsMember === TRUE)) {
        print preg_replace( '/( <p>&nbsp;<\/p> )|( <p>\s?\n )|( \n<\/p> )|( width|height )="\d*px"/', '', $wrapper->body->value()['safe_value'] );
      } else if ($requireMember === TRUE && $userIsMember === FALSE) {
        if ($logged_in === TRUE) {
          outTextNotMember();
        } else {
          outTextLogIn();
        }
      }


		//print preg_replace( '/( <p>&nbsp;<\/p> )|( <p>\s?\n )|( \n<\/p> )|( width|height )="\d*px"/', '', $wrapper->body->value()['safe_value'] );
		
		
		// Turning off flags on the front end for now:
		// $flags = flag_get_flags( 'node', $wrapper->value()->type );
		// foreach ( $flags as $key => $flag ) {
		//     print flag_create_link( $flag->name, $node->nid );
		// }
endif;
if ( ! empty( $content_blocks ) ): ?>
			<div class='section'>
	<?php
	foreach ( $content_blocks as $block_rendered ) :
		print $block_rendered;
	endforeach;
	?>
			</div>
	<?php
endif;
?>
		</article>
<?php
if (!( isset($wrapper) && !empty( $wrapper->field_exclude_sidebar->value() ) && $wrapper->field_exclude_sidebar->value() == '1' )) {

    echo "		<aside class='side__sidebar span3 l'>";
    if ( ! empty( $ads_fields ) ) :
      require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/sidebar-ad.tpl.inc';
    endif;
    echo "		</aside>";
}
?>
	</section>
</main>
