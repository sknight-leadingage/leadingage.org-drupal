<?php
/**
 * @file
 * Landing Page template ( version B ).
 */
// dpm( $fields );
// dpm( $wrapper->value() );
print $messages;
if ( $tabs && ! empty( $tabs['#primary'] ) ) :
	?>
	<div class='tabs'><?php print render( $tabs ); ?></div>
	<?php
endif;
?>

<article class='hero'>

<?php
/**
 * Banner Image.
 *
 * Images aren't entities perse so, we use the fields array instead.
 */
if ( ! empty( $fields['field_image'] ) ) :
	?>
	<img src='<?php print file_create_url( $fields['field_image']['uri'] ); ?>' alt='<?php print $wrapper->title->value(); ?>' />
	<?php else : ?>
	<img src='<?php print '/' . drupal_get_path( 'theme', 'zero' ) . '/docs/assets/img/samples/hero_homepage.jpg'; ?>' alt='Sample image for homepage hero' />
	<?php
endif;
?>
</article>
<main class='site__main'>
	<section class='row--main container'>
		<article class='span7'>
			<header class='section'>
				<h1><?php print $wrapper->title->value(); ?></h1>
				<?php
				/**
				 * Body.
				 */
				if ( ! empty( $fields['body'] ) ) {
					print preg_replace( '/<p>/', '<p class="intro">', $wrapper->body->value()['safe_value'] );
				}
				?>
			</header>
				<?php
				/**
				 * Feed.
				 */
				if ( ! empty( $fields['field_feed'] ) ) :
					?>
				<div class='row--mcb'>
					<?php
					$feed = $fields['field_feed']->getView( 0 );
					$feed_count = count( $feed );
					foreach ( $feed as $key => $value ) :
						$staff_entity = new Nil\Entity( $value->nid, 'staff' );
						$staff_fields = $staff_entity->getFieldsArray();
						$staff_wrapper = $staff_entity->getWrapper();
						if ( 0 !== $key && 0 === ( $key + 1 ) % 3 ) :
							?>
					<article class='card -main span4 l'>
							<?php
						else :
							?>
					<article class='card -main span4'>
							<?php
						endif;
						?>
						<a href='<?php print drupal_get_path_alias( 'node/' . $value->nid ); ?>' class='card__inner'>
							<h3><?php print $staff_wrapper->title->value() . ' ' . $staff_wrapper->field_mname->value() . ' ' . $staff_wrapper->field_lname->value(); ?></h3>
						<?php
						if ( ! empty( $staff_fields['field_job_title'] ) ) :
							?>
							<p><?php print $staff_wrapper->field_job_title->value(); ?></p>
							<?php
						endif;
						if ( $staff_fields['field_team'] ) :
							?>
							<h6 class='label'><?php print $staff_wrapper->field_team->value(); ?></h6>
							<?php
						endif;
						?>
						</a>
					</article>
						<?php
						if ( 0 !== $key && 0 === ( $key + 1 ) % 3 ) :
							if ( $feed_count > $key + 1 ) :
								?>
						</div><div class='row--mcb'>
								<?php
							endif;
						endif;
					endforeach;
					?>
				</div>
					<?php
				endif;
				?>
		</article>
		<aside class='side__sidebar span3 l'>
		<?php
		if ( ! empty( $ads_fields ) ) :
			require_once drupal_get_path( 'theme', 'zero' ) . '/templates/partials/sidebar-ad.tpl.inc';
		endif;
		?>
		</aside>
	</section>
</main>
