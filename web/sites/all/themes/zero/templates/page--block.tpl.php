<div id="page-wrapper">
	<div id="page">
		<div id="main-wrapper">
			<section class="row--main container">
<?php
if ( $tabs ) :
	?>
				<div class='tabs'><?php print render( $tabs ); ?></div>
	<?php
endif;

switch ( $bean_fields['type'] ){
	case 'basic':
		$fields['field_block'][0] = $bean_block;
		include drupal_get_path( 'theme', 'zero' ) . '/templates/partials/standard-block.tpl.inc';
		break;
	case 'custom_feed':
		$block = $bean_block;
		print '<section class="grid -two-columns feed">';
		include drupal_get_path( 'theme', 'zero' ) . '/templates/partials/collections/feed-block.tpl.inc';
		print '</section>';
		break;
	case 'front_page_grid':
		$block = $bean_block;
		include drupal_get_path( 'theme', 'zero' ) . '/templates/partials/collections/custom-grid.tpl.inc';
		break;
	case 'footer_ads':
		$footer_ads_fields = $bean_fields;
		include drupal_get_path( 'theme', 'zero' ) . '/templates/partials/footer-ad.tpl.inc';
		break;
	case 'site_info':
		$site_info_fields = $bean_fields;
		include drupal_get_path( 'theme', 'zero' ) . '/templates/partials/items/site-info.tpl.inc';
		break;
	case 'sidebar_ads':
		$ads_fields = $bean_fields;
		print '<aside class="side__sidebar span3 l">';
		include drupal_get_path( 'theme', 'zero' ) . '/templates/partials/sidebar-ad.tpl.inc';
		print '</aside>';
		break;
}
?>
			</section>
		</div>
	</div>
</div>