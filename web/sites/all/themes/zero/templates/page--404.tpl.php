<div class='four0four -background'>
	<large class='align -centered'><i>Oops! The page you are looking for is not here.</i></large>
	<h1 class='align -centered'>4<span class='color -transparent'>0</span>4</h1>
	<a href='/' class='btn -main -centered'>Go Back</a>
</div>