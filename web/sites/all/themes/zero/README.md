# Zero for Leading Age

Complete documentation can be found on the `/docs` folder.

## Standard Browncoat Project Start Up

1. Make sure you have Node `v4.x` installed (npm ships with node)
2. Do this:
  ```bash
  # Install dependencies
  npm install

  # Opens browser to localhost:300x to view docs
  npm run docs

  # Opens browser to localhost:300x to view app (empty by default)
  npm run gulp

  # Run Tests
  npm test
  ```
3. `npm run docs` lets you view the docs from `/docs`
4. Questions/comments/concerns `qcc's` direct towards Leo Bauza (original developer) or Dave Tate (director of technology)
