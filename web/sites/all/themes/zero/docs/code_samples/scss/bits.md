### colours
```css
/* greyscale */
$black: #000;
$grey: #c8c8c8;
$dark_grey: #b3b3b3;
$off_white: #f3f3f3;
$white: #fff;

/* general */
$brand: #7bb337;
$brand_alt: #404450;
$brand_aux: #6d7079;
$body_bg: #fff;
$nav_bg: #464a55;

/* links */
$link_colour: #7bb337;
$link_hover_colour: #45651f;

/* text */
$text_colour: #646a7b;
$text_colour_light: #d6d7d8;

/* alerts */
$default: #c8c8c8;
$info: #66d9ef;
$success: #5cb85c;
$warning: #e6db74;
$error: #d9534f;
/* end */
```
---
### typography
```css

/* typography */
$base_size: 62.5%;
$display: "freight-sans-pro", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
$sans_serif: "source-sans-pro", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
$serif: "calluna", "Georgia", "Times New Roman", Times, serif;
$code: Menlo,Monaco,Consolas,"Courier New",monospace;

/* not everything inherits from the body, use $global_family */
$global_family: $serif;

/* paragraphs and lists */
$meta_size: 1.5em;
$text_size: 1.8em;
$text_line_height: 1.8em;
$lists_font_size: $text_size;
$lists_line_height: $text_line_height;
$intro_size: 2.4em;

/* headings */
$h0: 7.2em;
$h1: 5.0em;
$h2: 2.6em;
$h3: 2.2em;
$h4: 1.8em;
$h5: 1.6em;
$h6: 1.4em;

/* weights */
$regularFont: 400;
$boldFont: 700;
$blackFont: 900;
/* end */
```
---
### grid
```css
/* global grid */
$global_columns: 12;
$global_gutter: 7.79661;
$global_margin: 2;

/* full width boxes */
$fwb_columns: $global_columns;
$fwb_gutter: 3.728814;

/* full width lists */
$fwl_columns: $global_columns;
$fwl_gutter: 7.118644;

/* main container boxes */
$mcb_columns: $global_columns;
$mcb_gutter: 6.617647;
/* end */
```
---
### utils
```css
/* mixin */
@mixin myMixin ($value) {
  property: $value;
}
/* function */
@function myFn($value) {
  return $value;
}
/* placeholder */
%placeholder {
  property: value;
}
/* end */
```
---
### sprites
```css
/*
 * Usage
 * For file "icon_twitter.svg"
 */
.icon.-twitter {
  display: block;
  @include sprite($icon_twitter);
}
```
---
### backgrounds
```html
<div class="background -brand">
	<h2>Background Example</h2>
</div>
<div class="background -brand_aux">
	<h2>Background Example Two</h2>
</div>
```
---
### text-colors
```html
<div>
	<h2 class="color -brand">Color Example</h2>
</div>
<div class="color -brand_aux">
	<h2>Color Example Two</h2>
</div>
```