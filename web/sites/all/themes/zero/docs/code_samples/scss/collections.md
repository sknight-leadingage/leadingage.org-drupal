### boxwrap
```html
<div class="box-wrap -filter -multi">
  <p>Filter Content from the dropdowns below.</p>
  <nav class="dropdown -hover">
    <a href="#" class="btn -main"><span class="dropdown__selected">News</span><i class="arrow -down"></i></a>
  </nav>
  <nav class="dropdown -hover">
    <a href="#" class="btn -main"><span class="dropdown__selected">Cfar</span><i class="arrow -down"></i></a>
  </nav>
  <nav class="dropdown -hover l">
    <a href="#" class="btn -main"><span class="dropdown__selected">Select Topic</span><i class="arrow -down"></i></a>
  </nav>
</div>
<div class="box-wrap -grid">
  <div class="row-flush">
    <a href="#" class="box -main span6 f">
      <div class="box__inner">
        <header class="box__header">
          <h4 class="box__heading">Sample Box with Inner</h4>
        </header>
        <div class="box__body">
          <p class="box__text">Sample box body. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
        </div>
        <footer class="box__footer">
          <span href="#" class="meta">Read More</span>
        </footer>
      </div>
    </a>
    <a href="#" class="box -main span6">
      <div class="box__inner">
        <header class="box__header">
          <h4 class="box__heading">Sample Box with Inner</h4>
        </header>
        <div class="box__body">
          <p class="box__text">Sample box body. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
        </div>
        <footer class="box__footer">
          <span href="#" class="meta">Read More</span>
        </footer>
      </div>
    </a>
  </div>
</div>
<footer class="box-wrap-after">
  <a href="#" class="meta">+ Box Wrap After</a>
</footer>
```
---
### nav
```html
<header class="site__header">
  <ul class="nav group">
    <li class="nav__item f"><a title="About" href="#">About</a></li>
    <li class="nav__item"><a title="Newsroom" href="#">Newsroom</a></li>
    <li class="nav__item -exploring"><a title="Centers" href="#">Centers</a></li>
    <li class="nav__item"><a title="Education" href="#">Education</a></li>
    <li class="nav__item -active"><a href="#">Advocacy</a></li>
    <li class="nav__item"><a title="Events" href="#">Events</a></li>
    <li class="nav__item"><a title="Members" href="#">Members</a></li>
    <li class="nav__item l"><a title="Consumers" href="#">Consumers</a></li>
  </ul>
</header>
<nav class="site__nav">
  <div class="nav__below row-flush">
    <div class="nav__secondary span4">
      <ul>
        <li class="nav__item -below f"><a href="#">Center for Applied Research</a></li>
        <li class="nav__item -below"><a href="#">Center Aging Services Technologies</a></li>
        <li class="nav__item -below"><a href="#">Center for Housing Plus Services</a></li>
        <li class="nav__item -below f"><a href="#">IAHSA</a></li>
      </ul>
    </div>
    <aside class="nav__aside span8 l">
      <div class="inner">
        <div class="row--nav">
          <ul class="span4 f">
            <li class="nav__item -aside f"><a href="#">About CAST</a></li>
            <li class="nav__item -aside"><a href="#">Joining CAST</a></li>
            <li class="nav__item -aside"><a href="#">CAST Sponsors</a></li>
            <li class="nav__item -aside"><a href="#">CAST Associates</a></li>
            <li class="nav__item -aside l"><a href="#">Aging Services</a></li>
          </ul>
          <ul class="span4">
            <li class="nav__item -aside f"><a href="#">Strategic IT Planning Tools</a></li>
            <li class="nav__item -aside"><a href="#">Technology Selection Tools</a></li>
            <li class="nav__item -aside"><a href="#">State of Technology Reports</a></li>
            <li class="nav__item -aside"><a href="#">CAST Case Studies</a></li>
            <li class="nav__item -aside l"><a href="#">Idea House</a></li>
          </ul>
          <ul class="span4 l">
            <li class="nav__item -aside f"><a href="#">Technology Policy</a></li>
            <li class="nav__item -aside"><a href="#">CAST Newsroom</a></li>
            <li class="nav__item -aside"><a href="#">Resources</a></li>
            <li class="nav__item -aside"><a href="#">Majd's Message</a></li>
          </ul>
        </div>
      </div>
    </aside>
  </div>
</nav>
```
---
### pagination
```html
<ul class="pagination">
  <li class="pagination__item f"><a href="#">Prev</a></li>
  <li class="pagination__item active"><a href="#">01-05</a></li>
  <li class="pagination__item"><a href="#">06-10</a></li>
  <li class="pagination__item"><a href="#">11-15</a></li>
  <li class="pagination__item"><a href="#">16-20</a></li>
  <li class="pagination__item"><a href="#">21-25</a></li>
  <li class="pagination__item l"><a href="#">Next</a></li>
</ul>
```
---
### summaries
```html
<ol class="summary-list">
  <li class="summary">
    <div class="summary__inner">
      <h4 class="summary__heading">Summary without link</h4>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
    </div>
  </li>
  <li class="summary">
    <a class="summary__inner" href="#">
      <h4 class="summary__heading"><span>Summary with link</span></h4>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
    </a>
  </li>
  <li class="summary">
    <a class="summary__inner group" href="#">
      <img class="summary__image" src="/assets/img/samples/summary_thumb.jpg" alt="Sample Summary" />
      <div class="summary__text">
        <h4 class="summary__heading"><span>Summary with link</span></h4>
        <h6 class="meta">By Byline Byliner</h6>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
      </div>
    </a>
  </li>
</ol>
<div class="summary">
  <a href="#">
    <h4 class="summary__heading"><span>Summary not in a list</span></h4>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
  </a>
</div>
```
---
### heroes
```html
<article class="hero">
  <img class="hero__bg" src="/assets/img/samples/hero_homepage.jpg" alt="Sample image for homepage hero" />
  <div class="hero__content">
    <h6 class="meta">january / february 2016  •  volume 06  •  number 01</h6>
    <h1 class="-hero">Hero Heading Could Be Two Lines</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
  </div>
</article>
<article class="hero -feature">
  <img class="hero__bg" src="/assets/img/samples/hero_homepage.jpg" alt="Sample image for homepage hero" />
  <div class="hero__content">
    <h1 class="-hero">Hero Heading Could Be Two Lines</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
    <a href="#" class="btn -main">Click to Learn More</a>
  </div>
</article>
<article class="hero -simple">
  <img class="hero__bg" src="/assets/img/samples/hero_homepage.jpg" alt="Sample image for homepage hero" />
  <div class="hero__content">
    <h1 class="-hero">Hero Heading Could Be Two Lines</h1>
  </div>
</article>
<article class="hero -complex">
  <img class="hero__bg" src="/assets/img/samples/hero_homepage.jpg" alt="Sample image for homepage hero" />
  <div class="hero__content">
    <h2 class="-section">Section Heading</h2>
    <div class="inner">
      <h6 class="meta">january / february 2016  •  volume 06  •  number 01</h6>
      <h1 class="-alt">Hero Heading Could Be Two Lines</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
    </div>
  </div>
</article>
```
---
### boxes
```html
<article class="box -main">
  <header class="box__header">
    <img src="/assets/img/samples/box_image_thirds.png" alt="sample box image for 1/3" />
    <h2 class="box__heading">Sample Box</h2>
  </header>
  <div class="box__body">
    <p class="box__text">Sample box body. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
  </div>
  <footer class="box__footer">
    <a href="#" class="meta">Read More</a>
  </footer>
</article>
<a href="#" class="box -main">
  <header class="box__header">
    <img src="/assets/img/samples/box_image_thirds.png" alt="sample box image for 1/3" />
    <h2 class="box__heading"><span>Sample Link Box</span></h2>
  </header>
  <div class="box__body">
    <p class="box__text">Sample box body. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
  </div>
  <footer class="box__footer">
    <span href="#" class="meta">Read More</span>
  </footer>
</a>
<a href="#" class="box -main">
  <div class="box__inner">
    <header class="box__header">
      <h4 class="box__heading">Sample Box with Inner</h4>
    </header>
    <div class="box__body">
      <p class="box__text">Sample box body. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
    </div>
    <footer class="box__footer">
      <span href="#" class="meta">Read More</span>
    </footer>
  </div>
</a>
```
---
### cards
```html
<article class="card -main">
  <a href="#" class="card__inner">
    <h3>Sample Card</h3>
    <p>Lorem ipsum dolor sit amet.</p>
    <h6 class="label">Meta Information</h6>
  </a>
</article>
```
---
### dropdowns
```html
<nav class="dropdown -hover">
  <a href="#" class="btn -main">
    <span class="dropdown__selected">Dropdown</span>
    <i class="arrow -down"></i>
    <ul class="list-group -main">
      <li>Option One</li>
      <li>Option Two</li>
      <li>Option Three</li>
    </ul>
  </a>
</nav>
<nav class="dropdown -js">
  <a href="#" class="btn -main">
    <span class="dropdown__selected">Dropdown</span>
    <i class="arrow -down"></i>
    <ul class="list-group">
      <li>Option One</li>
      <li>Option Two</li>
      <li>Option Three</li>
    </ul>
  </a>
</nav>
```
---
### formgroups
```html
<form>
  <div class="form__group">
    <label for="name">Name</label>
    <input type="text">
  </div>
  <div class="radio">
    <label><input type="radio" name="truthiness" value="true">True</label>
    <label><input type="radio" name="truthiness" value="false">False</label>
  </div>
  <div class="checkbox">
    <label for="check"><input type="checkbox" name="check">Check</label>
  </div>
  <input type="submit" class="btn" name="submit">
</form>
```
---
### sessionsummary
```html
<div class="sessions-summary box-wrap -grid">
  <div class="row-flush">
    <a href="#" class="box -main span4 f">
      <div class="image -headshot -session f" style="background-image: url(/assets/img/samples/summary_thumb.jpg)"></div>
    </a>
    <a href="<?php echo $url ?>" class="box -main span8 l">
      <div class="box__inner">
        <header class="box__header">
          <h3 class="box__heading"><b>Tuesday</b> • 11:30 AM - 2:30 PM</h3>
        </header>
        <div class="box__body">
          <h3 class="color -brand">Robyn Who Reads</h3>
          <p class="box__text"><i>Session Three (Expo)</i></p>
        </div>
        <footer class="box__footer">
          <span href="#" class="meta">View More</span>
        </footer>
      </div>
    </a>
  </div>
</div>
```