### container
```html
<div class="container">
  <p>Horizontal rules.</p>
</div>
<div class="segment">
  <p>Various rules depending on modifier.</p>
</div>
<div class="section">
  <p>Margin bottom and border.</p>
</div>
```
---
### rows
```html
<h5>Basic Row</h5>
<section class="row -doc-show">
  <article class="span4">
    <p>Span</p>
  </article>
  <article class="span4">
    <p>Span</p>
  </article>
  <article class="span4">
    <p>Span</p>
  </article>
</section>
<h5>Row without right/left padding and using overflow:hidden</h5>
<section class="row-alt -doc-show">
  <article class="span4">
    <p>Span</p>
  </article>
  <article class="span4">
    <p>Span</p>
  </article>
  <article class="span4">
    <p>Span</p>
  </article>
</section>
<h5>Row without gutter</h5>
<section class="row-flush -doc-show">
  <article class="span4">
    <p>Span</p>
  </article>
  <article class="span4">
    <p>Span</p>
  </article>
  <article class="span4">
    <p>Span</p>
  </article>
</section>
```
---
### grids
```html
<h5>Basic Grid</h5>
<section class="grid -four-columns">
  <article class="item background -brand">
    <p>Grid Item</p>
  </article>
  <article class="item background -brand">
    <p>Grid Item</p>
  </article>
  <article class="item background -brand">
    <p>Grid Item</p>
  </article>
  <article class="item background -brand">
    <p>Grid Item</p>
  </article>
  <article class="item background -brand">
    <p>Grid Item</p>
  </article>
  <article class="item background -brand">
    <p>Grid Item</p>
  </article>
  <article class="item background -brand">
    <p>Grid Item</p>
  </article>
  <article class="item background -brand">
    <p>Grid Item</p>
  </article>
  <article class="item background -brand">
    <p>Grid Item</p>
  </article>
</section>
```
---
### makerows
```css
@mixin rows($gutter: 3.183, $columns: 12) {
  /* mixin code */
}

/*
 * Create a grid with 10 columsn with a gutter of 1% between them.
 */
.row-descriptor {
  @include rows(1, 10);
}
```
