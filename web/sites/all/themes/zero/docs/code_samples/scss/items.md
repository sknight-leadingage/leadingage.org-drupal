### btns
```html
<a href="#" class="btn">Button</a>
<a href="#" class="btn -main">Main Button</a>
```
---
### callouts
```html
<div class="callout">A default callout</div>
<div class="callout -main">A main callout</div>
<div class="callout -info">A info callout</div>
<div class="callout -success">A success callout</div>
<div class="callout -warning">A warning callout</div>
<div class="callout -error">An error callout</div>
```
---
### gen
```html
<p class="intro">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
<p class="small">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
<p class="large">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
```
---
### headings
```html
<h1 class="-hero">Hero Heading</h1>
<h1 class="-alt">Alt Heading</h1>
<h2 class="-section">Section Heading</h2>
<h6 class="meta">Meta Heading</h1>
<h6 class="label">Label Heading</h1>
<h6 class="number">01</h1>
<h6 class="label -regular">Label Regular Heading</h1>
```
---
### icons
```html
<i class="icon -twitter"></i>
<i class="icon -facebook"></i>
<i class="icon -linkedin"></i>
<i class="icon -instagram"></i>
<i class="icon -rss"></i>
<i class="icon -search"></i>
<i class="arrow -up"></i>
<i class="arrow -right"></i>
<i class="arrow -down"></i>
<i class="arrow -left"></i>
```
