### start
```bash
# Zero Structure

# Compiled CSS and javasript (/css & /js).
# Under no circumstances should these be edited directly.
# Sprites generated with gulp sprite will appear in an /img
# folder inside the /assets folder. Image assets utilized by
# css should be placed inside the same /img folder.
/assets

# The documentation you are reading right now.
/docs

# Files included by template.php. These is where
# preprocess and theme functions should go.
/includes

# /scss and /js contain uncompiled SCSS and modular js.
# /icons contain SVG images to be combined into a svg/png sprite.
/src

# Drupal templates
/templates

# (optional) Visual regression testing.
/wraith

logo.png # nothing important
README.md # READ IT
screenshot.png # screenshot for drupal admin
template.php # theme functions (requires /incudes)
zero.info # required theme file (theme configuration)
```
---
### modules
```bash
# contrib (todo: short description)
admin_menu
admin_views
adminimal_admin_menu
better_formats
ckeditor
context
ctools
devel
entity
entityreference
field_group
file_entity
media
module_filter
pathauto
token
views
views_bulk_operations

# custom
nil # the zero companion module
```
---
### gulp
```bash
# Default gulp task
npm run gulp
## Watches:
# /src/scss
# /src/js
# *.html in /web
## Compiles to:
# /web/assets/css
# /web/assets/js

# Default gulp task
npm run docs
## Watches:
# /src/scss
# /src/js
# *.html in /docs
# *.md in /docs
# *.json in /docs
## Compiles to:
# /docs/assets/css
# /docs/assets/js

```
