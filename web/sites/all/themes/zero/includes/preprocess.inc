<?php
/**
 * @file
 * Preprocessors.
 *
 * The zero theme requires the Nil namespace provided by the nil module.
 *
 * @see
 *   /modules/custom/nil
 */


/**
 * Alters Search Facet 'Turn off' link
 */
function zero_facetapi_link_active( $variables ) {

	// Sanitizes the link text if necessary.
	$sanitize = empty( $variables['options']['html'] );
	$link_text = ( $sanitize ) ? check_plain( $variables['text'] ) : $variables['text'];

	// Theme function variables fro accessible markup.
	// @see http://drupal.org/node/1316580
	$accessible_vars = array(
		'text' => $variables['text'],
		'active' => true,
	);

	// Builds link, passes through t() which gives us the ability to change the
	// position of the widget on a per-language basis.
	$replacements = array(
		'!facetapi_deactivate_widget' => 'View All',
		'!facetapi_accessible_markup' => theme( 'facetapi_accessible_markup', $accessible_vars ),
	);
	$variables['text'] = t( '!facetapi_deactivate_widget !facetapi_accessible_markup', $replacements );
	$variables['options']['html'] = true;
	$variables['options']['attributes'] = array(
			'class' => array( 'btn -main form-select ctools-auto-submit form-select' ),
		);
	return theme_link( $variables );
}

function zero_facetapi_link_inactive( $variables ) {
	// Builds accessible markup.
	// @see http://drupal.org/node/1316580
	$accessible_vars = array(
		'text' => $variables['text'],
		'active' => false,
	);
	$accessible_markup = theme( 'facetapi_accessible_markup', $accessible_vars );
	// Sanitizes the link text if necessary.
	$sanitize = empty( $variables['options']['html'] );
	$variables['text'] = ( $sanitize ) ? check_plain( $variables['text'] ) : $variables['text'];

	// Adds count to link if one was passed.
	if ( isset( $variables['count'] ) ) {
		// $variables['text'] .= ' ' . theme( 'facetapi_count', $variables );
	}

	// Resets link text, sets to options to HTML since we already sanitized the
	// link text and are providing additional markup for accessibility.
	$variables['text'] .= $accessible_markup;
	$variables['options']['html'] = true;
	$variables['options']['attributes'] = array(
		'class' => array( 'btn -main form-select ctools-auto-submit form-select' ),
	);
	return theme_link( $variables );
}

/**
 * Implements hook_preprocess_pagerer_standard().
 */
function zero_preprocess_pagerer_standard( &$variables ){
	//Change Text For Pagerer custom pagers.
	$variables['tags']['first'] = t( 'FIRST' );
	$variables['tags']['next'] = t( 'NEXT' );
	$variables['tags']['last'] = t( 'LAST' );
	$variables['tags']['previous'] = t( 'PREV' );
}

/**
 * Implements hook_preprocess_html().
 */
function zero_preprocess_html( &$vars )
{
	$menus = array(
		'about',
		'newsroom',
		'centers',
		'education',
		'advocacy',
		'events',
		'members',
		'partners',
	);
	// Retrieve Menus.
	$main_menu = $vars['main_menu'] = Nil\Theme::getMenus( $menus );
	$path_alias = $vars['path_alias'] = drupal_get_path_alias( current_path() );
	$vars['path_alias_parts'] = explode( '/', $path_alias );
	$site_info_bean = bean_load( 'site-info' );
	$site_info_entity = $vars['site_info_entity'] = new Nil\Entity( $site_info_bean, 'site_info', 'bean' );
	$site_info_fields = $vars['site_info_fields'] = $site_info_entity->getFieldsArray();
	$site_info_wrapper = $vars['site_info_wrapper'] = $site_info_entity->getWrapper();
	$footer_ads_bean = bean_load( 'global-footer-ads' );
	$footer_ads_entity = $vars['footer_ads_entity'] = new Nil\Entity( $footer_ads_bean, 'footer_ads', 'bean' );
	// $footer_ads_fields = $vars['footer_ads_fields'] = $footer_ads_entity->getFieldsArray();
	$footer_ads_wrapper = $vars['footer_ads_wrapper'] = $footer_ads_entity->getWrapper();

	/*
	 * 404 Page
	 */
	$header = drupal_get_http_header( 'status' );
	if ( '404 Not Found' === $header ) {
		unset( $footer_ads_fields );
	}
}

/**
* Implements hook_preocesss_view
* This function is used to get the add_field value for the Taxonomy views to render internal-ads block.
*/
function zero_preprocess_views_view(&$vars) {
	//echo"<pre>";print_r(bean_load( 'internal-ads' ));die;
	if(isset($vars['view']->name) == 'Taxonomy' || isset($vars['view']->name) == 'LeadingAge_Service_Partner' ){
		$internal_ads_bean = empty( $fields['field_ad_sidebar'] ) ? bean_load( 'internal-ads' ) : $fields['field_ad_sidebar'];
		if ( ! empty( $internal_ads_bean ) ) {
			//echo"<pre>";print_r("if");exit;
			$ads_entity = $vars['ads_entity'] = new Nil\Entity( $internal_ads_bean, 'sidebar_ads', 'bean' );
			$ads_fields = $vars['ads_fields'] = $ads_entity->getFieldsArray();
			$ads_wrapper = $vars['ads_wrapper'] = $ads_entity->getWrapper();
		}
		//echo"<pre>";print_r($ads_fields);exit;
	}
}

/**
 * Implements hook_preprocess_page().
 *
 * @todo: Fix lack of flexibility and dynamics.
 * The page template never properly Drupal renders content or the node.
 * Rather, it manually retrieves a static set of fields and prints them,
 * perhaps to make the front-end styles 'clean' or easier. This essentially
 * cripples Drupal CMS: fields, contexts, blocks, can't be added/don't work.
 */
function zero_preprocess_page( &$vars )
{
/* this  is the for the page content type to display og tags */
	$arg_value = arg(0);
	if($vars['page']['#type'] == 'page'){
		//echo"<pre>";print_r();die;
		if(isset($vars['node']->title)){
		$og_title = array(
	  		'#tag' => 'meta',
	  		'#attributes' => array(
	    		'property' => 'og:title',
	    		'content' => $vars['node']->title,
	  		),
		);

		drupal_add_html_head($og_title, 'og_title');
		
		$body_text = null;
		if ( isset( $vars['node']->field_intro ) ) {
        $body_field = field_view_field('node', $vars['node'], 'field_intro', array('type' => 'text_plain'));
        if ( isset( $body_field[0]['#markup'] ) ) {
            $body_text = text_summary($body_field[0]['#markup']);
          } else {
            $body_text = $vars['node']->title;
        }
      } else {
        $body_text = $vars['node']->title;
		}

			$og_description = array(
			 '#tag' => 'meta',
			 '#attributes' => array(
			   'property' => 'og:description',
			   'content' => $body_text,
			 ),
			);

			drupal_add_html_head($og_description, 'og_description');
		}
	}


	if($arg_value == 'business_members'){
		$internal_ads_bean = empty( $fields['field_ad_sidebar'] ) ? bean_load( 'internal-ads' ) : $fields['field_ad_sidebar'];
		if ( ! empty( $internal_ads_bean ) ) {
			$ads_entity = $vars['ads_entity'] = new Nil\Entity( $internal_ads_bean, 'sidebar_ads', 'bean' );
			$ads_fields = $vars['ads_fields'] = $ads_entity->getFieldsArray();
			$ads_wrapper = $vars['ads_wrapper'] = $ads_entity->getWrapper();
		}
	}
/* ends the code */

	$type = '';
	$request_path = request_path();
	$query_params = drupal_get_query_parameters();
	$path_alias = $vars['path_alias'] = drupal_get_path_alias( current_path() );
	$path_alias_parts = $vars['path_alias_parts'] = explode( '/', $path_alias );
	$is_magazine_page = $vars['is_magazine_page'] = $path_alias_parts[0] === 'magazine';
	$is_senior_action_network_page = $vars['is_senior_action_network_page'] = $path_alias_parts[0] === 'senior-action-network';
	$vars['is_latest_magazine'] = $request_path === 'magazine';

	/*
	 * 404 Page
	 */
	$header = drupal_get_http_header( 'status' );
	if ( '404 Not Found' === $header ) {
		$vars['theme_hook_suggestions'][] = 'page__404';
	}
	
	/*
	 * Security headers
	 */
	 
	drupal_add_http_header('X-Content-Type-Options',    'nosniff');
	drupal_add_http_header('X-XSS-Protection',          '1; mode=block');
	drupal_add_http_header('Referrer-Policy',           'strict-origin-when-cross-origin');
	drupal_add_http_header('Strict-Transport-Security', 'max-age=31536000; includeSubDomains');
	drupal_add_http_header('X-Frame-Options',           'ALLOW-FROM https://community.leadingage.org');
	drupal_add_http_header('Permissions-Policy',        'microphone=()');
	drupal_add_http_header('Content-Security-Policy',   "default-src 'self'; script-src 'self' www.googletagmanager.com 'nonce-" . substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(10/strlen($x)) )),1,10) . "' use.typekit.net www.leadingage.org;");

	/*
	 * Set theme suggestion based on content type.
	 * Create fields array/entity_metadata_wrapper/Nil\Entity for nodes.
	 */
	if ( isset( $vars['node'] ) ) {
		$node = $vars['node'];
		$node_type = $node->type;
		if ( ! $vars['is_front'] ) {
			$vars['theme_hook_suggestions'][] = 'page__' . $node_type;
		}
		$entity = $vars['entity'] = new Nil\Entity( $node, $node_type );
		$fields = $vars['fields'] = $entity->getFieldsArray();
		$wrapper = $vars['wrapper'] = $entity->getWrapper();
		$type = $vars['type'] = $wrapper->value()->type;
		if ( isset( $fields['field_type_b'] ) && $fields['field_type_b'] ) {
			$vars['theme_hook_suggestions'][] = 'page__' . $node_type . '_b';
		}

		// Here, manually checking context for blocks. Ultimately, the template
		// should be reworked to be more Drupal-aware. Rendering the
		// $page['content'] should display content correctly, it should not be
		// necessary to do this.
		$contexts = context_active_contexts();
		if ( ! empty( $contexts ) ) {
			foreach ( $contexts as $context ) {
				// Right now, only concerned with block reactions.
				if ( ! empty( $context->reactions['block']['blocks'] ) ) {
					foreach ( $context->reactions['block']['blocks'] as $block ) {
						$block_view = module_invoke( $block['module'], 'block_view', $block['delta'] );
						$vars['content_blocks'][] = render( $block_view['content'] );
					}
				}
			}
		}
	}

	/*
	 * Blocks
	 */
	if ( ! empty( $vars['page']['content']['system_main']['bean'] ) ){
		$bean_name = key( $vars['page']['content']['system_main']['bean'] );
		$bean_type = $vars['page']['content']['system_main']['bean'][ $bean_name ]['#bundle'];
		$bean = $vars['bean'] = $vars['page']['content']['system_main']['bean'][ $bean_name ];
		$bean_block = $vars['bean_block'] = bean_load( $bean_name );
		$bean_entity = $vars['bean_entity'] = new Nil\Entity( $bean_block, $bean_type, 'bean' );
		$bean_fields = $vars['bean_fields'] = $bean_entity->getFieldsArray();
		$bean_wrapper = $vars['bean_wrapper'] = $bean_entity->getWrapper();
	}

	/*
	 * Landing Pages & Pages & Magazine Articles
	 */
	if ( ! empty( $vars['page']['highlighted']['search_form'] ) ){
		$internal_ads_bean = empty( $fields['field_ad_sidebar'] ) ? bean_load( 'internal-ads' ) : $fields['field_ad_sidebar'];
		if ( ! empty( $internal_ads_bean ) ) {
			$ads_entity = $vars['ads_entity'] = new Nil\Entity( $internal_ads_bean, 'sidebar_ads', 'bean' );
			$ads_fields = $vars['ads_fields'] = $ads_entity->getFieldsArray();
			$ads_wrapper = $vars['ads_wrapper'] = $ads_entity->getWrapper();
		}
	}

	if ( isset( $node ) ) {
		if ( 'page' === $node_type || 'landing_page' === $node_type || 'magazine_issue' === $node_type || 'staff_page' === $node_type || 'magazine_article' === $node_type ) {
			$internal_ads_bean = empty( $fields['field_ad_sidebar'] ) ? bean_load( 'internal-ads' ) : $fields['field_ad_sidebar'];
			if ( 'magazine_article' === $node_type && isset($node->field_parent_issue['und'][0]['target_id']) ) {
				$parentNode = node_load( $node->field_parent_issue['und'][0]['target_id'] );
				$parentNode_type = $parentNode->type;
				$parentEntity = new Nil\Entity( $parentNode, $parentNode_type );
				$parentFields = $parentEntity->getFieldsArray();
				if ( ! empty( $fields['field_article_ad_sidebar'] ) ){
					$internal_ads_bean = bean_load( $fields['field_article_ad_sidebar']->bid );
				} else {
					$internal_ads_bean = empty( $parentFields['field_article_ad_sidebar'] ) ? bean_load( 'internal-ads' ) : $parentFields['field_article_ad_sidebar'];
				}
			}
			if ( ! empty( $internal_ads_bean ) ) {
				$ads_entity = $vars['ads_entity'] = new Nil\Entity( $internal_ads_bean, 'sidebar_ads', 'bean' );
				$ads_fields = $vars['ads_fields'] = $ads_entity->getFieldsArray();
				$ads_wrapper = $vars['ads_wrapper'] = $ads_entity->getWrapper();
			}
			if ( ! empty( $fields['field_article_ad_sidebar'] ) ) {
				$article_internal_ads_bean = $fields['field_article_ad_sidebar'];
				$article_ads_entity = $vars['article_ads_entity'] = new Nil\Entity( $article_internal_ads_bean, 'sidebar_ads', 'bean' );
				$article_ads_fields = $vars['article_ads_fields'] = $article_ads_entity->getFieldsArray();
				$article_ads_wrapper = $vars['article_ads_wrapper'] = $article_ads_entity->getWrapper();
			}
		}
	}

	/*
	 * Landing Pages
	 */
	if ( isset( $node ) ) {
		if ( 'landing_page' === $node_type || 'special_event' === $node_type || 'provider' === $node_type ) {
			require_once drupal_get_path( 'theme', 'zero' ) . '/includes/node_types/landing-page.inc';
		}
	}

	/*
	 * Magazine Issue
	 */
	if ( $is_magazine_page ) {
		// Get magazine menu.
		$mag_menu = Nil\Theme::getMenus( array( 'magazine' ) );
		$vars['mag_menu'] = $mag_menu['magazine']['links'];
	}
        $node = menu_get_object();
	  if (isset($node->nid) && $node->type == 'account') {
	  	drupal_set_title($node->field_member_name['und'][0]['value']);
	  }
}

/**
 * Emulates JS console.log
 */
function console_log( $data ) {
	if ( empty( $data ) ){
		print '<script>console.log( "Empty Data: '.$data.'" );</script>';
	} else {
		if ( is_array( $data ) || is_object( $data ) ){
			print '<script>console.table( ' . json_encode( $data ) . ' );</script>';
			print '<script>console.log( ' . json_encode( $data ) . ' );</script>';
		} else {
			print '<script>console.log( ' . json_encode( $data ) . ' );</script>';
		}
	}
}
/* this is function used to display og tags for all the content types on the site. */
function zero_preprocess_node(&$vars) {
	//echo"<pre>";print_r($vars);die;
	$og_title = array(
  		'#tag' => 'meta',
  		'#attributes' => array(
    		'property' => 'og:title',
    		'content' => $vars['title'],
  		),
	);

	drupal_add_html_head($og_title, 'og_title');

  $body_text = null;
  if ( isset( $vars['node']->body ) ) {
      $body_field = field_view_field('node', $vars['node'], 'body', array('type' => 'text_plain'));
      $body_text = text_summary($body_field[0]['#markup']);
    } else {
      $body_text = $vars['title'];
  }

		$og_description = array(
		 '#tag' => 'meta',
		 '#attributes' => array(
		   'property' => 'og:description',
		   'content' => $body_text,
		 ),
		);
		 
		drupal_add_html_head($og_description, 'og_description'); 
	
	if ($vars['type'] == 'products') {
		$img = field_get_items('node', $vars['node'], 'field_product_image');
		$img_url = file_create_url($img[0]['uri']);
		$og_image = array(
	  	'#tag' => 'meta',
	  	'#attributes' => array(
	    	'property' => 'og:image',
	    	'content' => $img_url,
	  	),
		);	
	}
	else if($vars['type'] == 'Special Event'){
		$img = field_get_items('node', $vars['node'], 'field_sidebar_image');
		$img_url = file_create_url($img[0]['uri']);
		$og_image = array(
	  	'#tag' => 'meta',
	  	'#attributes' => array(
	    	'property' => 'og:image',
	    	'content' => $img_url,
	  	),
		);
	}
	else{
		$img = field_get_items('node', $vars['node'], 'field_image');
		$img_url = file_create_url($img[0]['uri']);
		$og_image = array(
	  	'#tag' => 'meta',
	  	'#attributes' => array(
	    	'property' => 'og:image',
	    	'content' => $img_url,
	  	),
		);
	}
	drupal_add_html_head($og_image, 'og_image');
}

