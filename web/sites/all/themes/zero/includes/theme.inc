<?php
/**
 * @file
 * theme.inc
 */

/**
 * Implements hook_css_alter().
 */
function zero_css_alter(&$css)
{
		$exclude = array(
			'modules/system/system.base.css' => false,
			'modules/system/system.messages.css' => false,
			'modules/system/system.theme.css' => false,
			'modules/system/system.menus.css' => false,
			'modules/comment/comment.css' => false,
			'modules/field/theme/field.css' => false,
			'modules/node/node.css' => false,
			'modules/search/search.css' => false,
			'modules/user/user.css' => false,
			'sites/all/modules/contrib/ckeditor/css/ckeditor.css' => false,
			'sites/all/modules/contrib/views/css/views.css' => false,
			'sites/all/modules/contrib/ctools/css/ctools.css' => false,
			'sites/all/modules/contrib/date/date_api/date.css' => false,
			'sites/all/modules/contrib/date/date_popup/themes/datepicker.1.7.css' => false,
			'sites/all/modules/contrib/improved_multi_select/improved_multi_select.css' => false,
		);
		$css = array_diff_key($css, $exclude);
}

/**
 * Implements hook_js_alter().
 */
function zero_js_alter(&$js)
{
		// $jquery_path = '/' . drupal_get_path('theme', 'my') . '/assets/js/jquery.min.js';
		// $jquery_path = 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js';
		$jquery_path = 'https://www.leadingage.org/sites/all/themes/zero/assets/js/jquery-3.5.1.min.js?z3xinbw';
		$js['misc/jquery.js']['data'] = $jquery_path;
		$js['misc/jquery.js']['version'] = '3.5.1';
		$js['misc/jquery.js']['type'] = 'external';
		// $exclude = array(
		//   'settings' => false,
		//   'misc/jquery.js' => false,
		//   'misc/drupal.js' => false,
		//   'misc/jquery.once.js' => false,
		// );
		// $js = array_diff_key($js, $exclude);
}

/**
 * Implements hook_menu_link().
 */
function zero_menu_link($vars)
{
		$element = $vars["element"];
		$link = l($element["#title"], $element["#href"], $element["#localized_options"]);
		$submenu = _zero_nested_menus($element);
		return "<li" . drupal_attributes($element["#attributes"]) . ">" . $link . implode("\n", $submenu) . "</li>\n";
}

/**
 * Implements hook_menu_tree().
 */
function zero_menu_tree(&$vars)
{
		return "<ul>" . $vars["tree"] . "</ul>";
}

/**
 * Find all nested menus for a menu.
 *
 * @param array $element
 *   Element array.
 *
 * @return string
 *   Submenu with nested submenus.
 */
function _zero_nested_menus($element)
{
		$submenu = array();

		// If below add below and recurse.
		if (!empty($element["#below"])) {
				unset($element["#below"]["#sorted"]);
				unset($element["#below"]["#theme_wrappers"]);
				$submenu[] = "<ul>";
				foreach ($element["#below"] as $key => $value) {
						$subsubmenu = _zero_nested_menus($value);
						if (!empty($subsubmenu)) {
								$submenu[] = "<li>" . l($value["#title"], $value["#href"], $value["#localized_options"]) . implode("\n", $subsubmenu) . "</li>";
						} else {
								$submenu[] = "<li>" . l($value["#title"], $value["#href"], $value["#localized_options"]) . "</li>";
						}
				}
				$submenu[] = "</ul>";
		}
		return $submenu;
}

/**
 * Implements theme_menu_local_tasks().
 *
 * Tabs.
 */
function zero_menu_local_tasks(&$variables)
{
		$output = '';
		if (!empty($variables['primary'])) {
				$variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
				$variables['primary']['#prefix'] = '<ul>';
				$variables['primary']['#suffix'] = '</ul>';
				$output .= drupal_render($variables['primary']);
		}
		if (!empty($variables['secondary'])) {
				$variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
				$variables['secondary']['#prefix'] = '<ul>';
				$variables['secondary']['#suffix'] = '</ul>';
				$output .= drupal_render($variables['secondary']);
		}
		return $output;
}

/**
 * Implements hook_form_alter().
 */
function zero_form_alter(&$form, &$form_state, $form_id) {
		// dpm($form);
		// dpm($form_id);
		if ('search_form' === $form_id) {
				$result = db_query('SELECT taxonomy_term_data.name AS termName, taxonomy_vocabulary.name AS vocabName, taxonomy_term_data.vid, taxonomy_term_data.tid FROM {taxonomy_term_data} JOIN {taxonomy_vocabulary} ON taxonomy_term_data.vid = taxonomy_vocabulary.vid ORDER BY taxonomy_term_data.vid ASC');
				$termOptions=array('TOPIC');
				$vocabOptions=array('AREA');
				$typeOptions=array('TYPE');
				foreach ($result as $row) {
						if (empty($vocabOptions[$row->vid])) {
								$vocabOptions[$row->vid] = $row->vocabName;
						}
						$termOptions[$row->tid]=$row->termName;
				}
				$result = db_query('SELECT name, type FROM {node_type}');
				foreach ($result as $row) {
						$typeOptions[$row->type]=$row->name;
				}
				$form['type_options'] = array(
					'#type' => 'value',
					'#value' => $typeOptions
				);
				$form['type2_options'] = array(
					'#type' => 'value',
					'#value' => $vocabOptions
				);
				$form['type3_options'] = array(
					'#type' => 'value',
					'#value' => $termOptions
				);
				$form['basic']['keys']["#prefix"] = "<h1 class=''>Search Results</h1>";
				unset($form['basic']['keys']['#title']);
				$form['basic']['submit']['#value'] = 'SEARCH AGAIN';
				$form['advanced']['#collapsible'] = 0;
				$form['advanced']['keywords'] = null;
				// $form['advanced']['#attributes']['class'][1] = ' box-wrap -filter -multi';
				$form['advanced']["#title"]='Filter Search From the dropdowns below';
				$form['advanced']["#prefix"]='<div class="box-wrap -filter -multi">';
				$form['advanced']["#suffix"]='</div>';
				unset($form['advanced']["type"]['#title']);
				$form['advanced']['type']["#type"]='select';
				$form['advanced']['type']["#prefix"]='<nav class="dropdown -hover">';
				$form['advanced']['type']['#attributes']['class']=array('btn -main');
				$form['advanced']['type']['#options'] = $form['type_options']['#value'];
				$form['advanced']['type']["#suffix"]='</nav>';
				$form['advanced']['type2']["#type"]='select';
				$form['advanced']['type2']["#prefix"]='<nav class="dropdown -hover">';
				$form['advanced']['type2']['#attributes']['class']=array('btn -main');
				$form['advanced']['type2']['#options'] = $form['type2_options']['#value'];
				$form['advanced']['type2']["#suffix"]='</nav>';
				$form['advanced']['type3']["#type"]='select';
				$form['advanced']['type3']["#prefix"]='<nav class="dropdown -hover l">';
				$form['advanced']['type3']['#attributes']['class']=array('btn -main');
				$form['advanced']['type3']['#options'] = $form['type3_options']['#value'];
				$form['advanced']['type3']["#suffix"]='</nav>';
				$form['advanced']['submit']['#prefix'] = '<div class="action hidden">';
				$form['search_block_form']['#attributes']['placeholder'] = t( 'Enter search terms...' );
		}
		if ('search_block_form' === $form_id) {
				$form['search_block_form']['#attributes']['placeholder'] = t( 'Search Resources' );
				$form['#submit'][] = 'my_search_form_submit_function';
				$form['actions']['submit']['#value'] = t('SEARCH AGAIN');
		}
}

function my_search_form_submit_function(&$form, &$form_state) {
	$search_str = $form_state['values']['search_block_form'];
	$form_state['rebuild'] = TRUE;
	drupal_goto('search-page', array('query' => array('title' => $search_str, 'fulltext' => $search_str)));
}

function trim_it( $string ) {
	$excerpt_length = 330;
	$excerpt = '';
	if ( strlen( $string ) > $excerpt_length ){
		$exploded = explode( ' ', $string );
		while ( strlen( $excerpt ) < $excerpt_length && ! empty( $exploded[0] ) ){
			$excerpt .= array_shift( $exploded ).' ';
		}
		if ( ! empty( $exploded ) ){
			$excerpt .= '...';
		}
		return $excerpt;
	} else {
		return $string;
	}
}

/**
 * Get pagination items value => label array.
 */
function get_pagination_items($current_page, $total_rows, $items_per_page)
{
	// Get items depending on what the current page is...
		$out = array();
		$pitem_qty = ceil( $total_rows / $items_per_page );
		if ( $total_rows > 0 && $total_rows % $pitem_qty > 0 ){
				$remainder = ($total_rows - $items_per_page * $pitem_qty);
		} else {
				$remainder = 0;
		}
		for ($i = 0; $i < $pitem_qty; $i++) {
			// On last pass check final number in range.
				if( $i + 1 == $pitem_qty && $remainder !== 0 ){
						$endNumber = (($i * $items_per_page) + $items_per_page) + $remainder;
				}else{
						$endNumber = (($i * $items_per_page) + $items_per_page);
				}
				$out[$i] = sprintf('%02d', (($i * $items_per_page) + 1)) . " - " . sprintf('%02d', $endNumber);
		}
		return $out;
}
