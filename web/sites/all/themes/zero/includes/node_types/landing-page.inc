<?php
/**
 * @file
 * Landing Page specific page preprocess.
 */

$topics_view = null;
$areas_view = null;
$types_view = null;
$center_types_view = null;
$vars['multi_filter'] = false;

/*
 * Filterable Feed Filters.
 */
if ( 'newsroom' === $request_path ) {
	$types_view = $vars['content_types_filter'] = array(
		'event' => 'Events',
		'newsroom_post' => 'News',
		'education_spotlight_post' => 'Education Spotlight',
	);
	$vars['multi_filter'] = true;
	$areas_view = new Nil\View( 'news_areas|default' );
	$topics_view = new Nil\View( 'news_topics|default' );
} elseif ( 'cfar' === $request_path || 'cast' === $request_path || 'chps' === $request_path ) {
	// Some have a topics filter others have a types filter
	// Figure out which and enable accordingly.
	if ( true ) {
		$center_types_view = new Nil\View( 'center_post_types|default' );
	} else {
		$topics_view = new Nil\View( 'center_topics|default' );
	}
} elseif ( 'member-types' === $request_path ) {
	$center_types_view = new Nil\View( 'provider_types|default' );
} elseif ( count( explode( '/', $request_path ) ) > 1 ){
} else {
	$topics_view = new Nil\View( 'news_topics|default', $request_path );
}

$vars['topics_filter'] = ( ! is_null( $topics_view ) ) ? $topics_view->getView() : null;
$vars['areas_filter'] = ( ! is_null( $areas_view ) ) ? $areas_view->getView() : null;
$vars['types_filter'] = ( ! is_null( $types_view ) ) ? $types_view : null;
$vars['center_types_filter'] = ( ! is_null( $center_types_view ) ) ? $center_types_view->getView() : null;
/*
 * Query params to modify views.
 *
 * - p [int] a ( semantic ) page number ( ie. index 1 not 0 ).
 * - area [string] News area to filter on ( newsroom only )
 * - topic [string] News topic or center topic to filter on
 * - type [string] Content type or center post type ( newsroom/centers only )
 */
$query_params = drupal_get_query_parameters();
$vars['is_filtered'] = false;
$vars['view_current_page'] = 0;
if ( ! empty( $query_params ) ) {
	$vars['view_current_page'] = 0;
	$vars['query_params'] = $query_params;
	if ( ! empty( $query_params['p'] ) ){
		$vars['view_current_page'] = $query_params['p'] - 1;
	}
	if ( count( $query_params ) > 1 || empty( $query_params['p'] ) ){
		$vars['is_filtered'] = true;
	}
}
