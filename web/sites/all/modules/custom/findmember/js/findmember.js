(function ($) {
  Drupal.behaviors.findmember = {

    attach: function (context) {
    $( ".view-id-directorysample.view-display-id-page_1 .views-field-field-directory-image" ).each(function() {
      var attr = $(this).find( "span" ).attr('data-img');
      $(this).css('background', '#ddddcf url('+attr+') no-repeat center center');
      // $(this).css('background-position', 'center center');
      $(this).css('background-size', 'cover');
      $(this).find( "span" ).hide();
    });
            // console.log(getUrlParameter('clicked'));

      $("#edit-submit-directorysample").val("Search");
      // console.log(document.location.href);
     if(document.location.href.indexOf('&clicked=') > -1){
     indexx  = document.location.href.indexOf('widesearch-');
    // console.log("string index"+indexx);

    id_cnt = document.location.href.substr(indexx).split("&");
    id_current = '#'+id_cnt[0];
    // console.log(id_current);
    // console.log("element previously clicked");
     }else{
       id_current = '#widesearch-city';

     }
     var id_new;
if(!(document.location.href.indexOf('?') > -1)){
$("#views-exposed-form-directorysample-page-1 #edit-name").val("");
$("#views-exposed-form-directorysample-page-1 #edit-p-code").val("");
$("#views-exposed-form-directorysample-page-1 #edit-city").val("");
$('#edit-field-shipping-state-value option:first').attr("selected",true);
$('#edit-field-distance-value option:first').attr("selected",true);
$("#edit-field-services-checkbox input").removeAttr('checked');
}
else{
     $(".view-header").hide();
   $("#edit-submit-directorysample").hide();
   $("#views-exposed-form-directorysample-page-1").hide();
   $(".directory-resultinfo").show();
   $(".directory-detailinfo").hide();
   $(".view-filters").css('background', 'none');   
}
if((document.location.href.indexOf('&dir=b') > -1)){
     $(".view-header").show();
   $("#edit-submit-directorysample").show();
   $("#views-exposed-form-directorysample-page-1").show();
   $(".directory-resultinfo").hide();
   $(".directory-detailinfo").show();
   $(".view-directorysample.view-id-directorysample.view-display-id-page_1 .view-content").hide();
   $(".view-directorysample.view-id-directorysample.view-display-id-page_1 .view-empty").hide();
   $(".pager").hide();
   $(".view-filters").css('background', '#ddddcf');
}
    // console.log(id_current);
$('.directory-entry-lists').removeClass('active');
$(id_current).addClass('active');
$('#views-exposed-form-directorysample-page-1 .views-exposed-widget').hide();
$($(id_current).find('a').attr('data')).show(); 


$('.directory-entry-lists').click(function(e){
  e.preventDefault();
  $("#views-exposed-form-directorysample-page-1 #edit-name").val("");
  $("#views-exposed-form-directorysample-page-1 #edit-p-code").val("");
  $("#views-exposed-form-directorysample-page-1 #edit-city").val("");
  $('#edit-field-shipping-state-value option:first').attr("selected",true);
  $('#edit-field-distance-value option:first').attr("selected",true);
  $("#edit-field-services-checkbox input").removeAttr('checked');
  $('.directory-entry-lists').removeClass('active');
  $(this).addClass('active');
  $('#views-exposed-form-directorysample-page-1 .views-exposed-widget').hide();
  $($(this).find('a').attr('data')).show(); 
  id_new = $(this).attr('id');
  // console.log(id_new+'inside click');
  // $("#edit-submit-directorysample").trigger("click");
});

$('#edit-submit-directorysample').click(function(e){e.preventDefault();
  // console.log(id_new);
  var complete_url = $("form").serialize();
  index  = complete_url.indexOf('&name=');
  url = '?'+complete_url.substr(index);
 clicked_id = $("li.directory-entry-lists.active").attr('id'); 
   document.location = url+'&clicked='+clicked_id;
});
      $('#findmember-Back-to-results').click(function(e){e.preventDefault();
        window.history.back();
        // alert("working back to results");
      });
      $('.dir-searchpage-title').click(function(){
        // window.history.back();
      var savedlater = window.location.href;
      window.sessionStorage.setItem("savedlater", savedlater);
        // alert(savedlater);
      });

      $('#dirsearch-findmember-print').click(function(e){e.preventDefault();
        window.print();
      });
      $('#findmember-back-to-searchpage').click(function(e){e.preventDefault();
        document.location = window.sessionStorage.getItem("savedlater")+'&dir=b';
        });
//       $('#findmember-Back-to-results').click(function(e){e.preventDefault();
//         window.history.back();
//       });
//       $('#findmember-print').click(function(e){e.preventDefault();
//         window.print();
//       });
//       $('.findmember-pagination #findmember-Back').click(function(e){e.preventDefault();
//       // window.history.back();
//       // win = window.history.previous.href;
// // alert(document.referrer);
// //       if(document.referrer == NULL){
//         alert(window.history.previous.href);
//         // win = window.history.previous.href+'&dir=b';
//         //  document.location = win;
//       // }
//       // else{
//       //   document.location = document.referrer+'&dir=b'; 
//       // }      
//       // document.location = document.referrer+'&dir=b';
//       // alert("win");
//       // history.go(-2);
//       // $( "#findmember-listings-Back" ).trigger( "click" );      
//       });

      $('#findmember-listings-Back').click(function(e){e.preventDefault();
        
     $(".view-header").show();
   $("#edit-submit-directorysample").show();
   $("#views-exposed-form-directorysample-page-1").show();
   $(".directory-resultinfo").hide();
   $(".directory-detailinfo").show();
   $(".view-directorysample.view-id-directorysample.view-display-id-page_1 .view-content").hide();
   $(".view-directorysample.view-id-directorysample.view-display-id-page_1 .view-empty").hide();
   $(".view-filters").css('background', '#ddddcf');
      $(".pager").hide();
      });
      
        $(function() {
          $(".rslides").responsiveSlides();
          $("#edit-submit-directorysample").insertAfter(".view-directorysample .view-filters");
          /* visit website start link add HTTP */
          // var link_val = $("a#dir-check-link.directory-website").attr("href");
          // if(!((link_val.search("http")) == 0)){
          // link_val =  "http://"+link_val;
          // $("a#dir-check-link.directory-website").attr("href",link_val); 
          // }
          $("a#dir-check-link.directory-website").each(function(e){
            var link_val = $(this).attr("href");
            if(!((link_val.search("http")) == 0)){
            link_val =  "http://"+link_val;
            $(this).attr("href",link_val); 
            }
          });
          /*  visit website end  */
        });
      $(".rslides").responsiveSlides({
        auto: true,             // Boolean: Animate automatically, true or false
        speed: 1000,            // Integer: Speed of the transition, in milliseconds
        timeout: 4000,          // Integer: Time between slide transitions, in milliseconds
        // pager: true,           // Boolean: Show pager, true or false
        nav: true,             // Boolean: Show navigation, true or false
        // random: false,          // Boolean: Randomize the order of the slides, true or false
        pause: true,           // Boolean: Pause on hover, true or false
        // pauseControls: true,    // Boolean: Pause when hovering controls, true or false
        // prevText: "Previous",   // String: Text for the "previous" button
        // nextText: "Next",       // String: Text for the "next" button
        // maxwidth: "",           // Integer: Max-width of the slideshow, in pixels
        // navContainer: "",       // Selector: Where controls should be appended to, default is after the 'ul'
        // manualControls: "",     // Selector: Declare custom pager navigation
        // namespace: "rslides",   // String: Change the default namespace used
        // before: function(){},   // Function: Before callback
        // after: function(){}     // Function: After callback
      });
    }
  }
})(jQuery);

// (function ($) {
//   Drupal.behaviors.findmember = {

//     attach: function (context) {
//     $( ".view-id-directorydirectorysample.view-display-id-page_1 .views-field-field-directory-image" ).each(function() {
//       var attr = $(this).find( "img" ).attr('data-img');
//       $(this).css('background', '#ddddcf url('+attr+') no-repeat center center');
//       // $(this).css('background-position', 'center center');
//       $(this).css('background-size', 'cover');
//       $(this).find( "img" ).hide();
//     });
//             // console.log(getUrlParameter('clicked'));

//       $("#edit-submit-directorydirectorysample").val("Search");
//       // console.log(document.location.href);
//      if(document.location.href.indexOf('&clicked=') > -1){
//      indexx  = document.location.href.indexOf('widesearch-');
//     // console.log("string index"+indexx);

//     id_cnt = document.location.href.substr(indexx).split("&");
//     id_current = '#'+id_cnt[0];
//     // console.log(id_current);
//     // console.log("element previously clicked");
//      }else{
//        id_current = '#widesearch-city';

//      }
//      var id_new;
// if(!(document.location.href.indexOf('?') > -1)){
// $("#views-exposed-form-directorydirectorysample-page-1 #edit-name").val("");
// $("#views-exposed-form-directorydirectorysample-page-1 #edit-p-code").val("");
// $("#views-exposed-form-directorydirectorysample-page-1 #edit-city").val("");
// $('#edit-field-shipping-state-value option:first').attr("selected",true);
// $('#edit-field-distance-value option:first').attr("selected",true);
// $("#edit-field-services-checkbox input").removeAttr('checked');
// }
//     // console.log(id_current);
// $('.directory-entry-lists').removeClass('active');
// $(id_current).addClass('active');
// $('.view-id-directorydirectorysample .views-exposed-widget').hide();
// $($(id_current).find('a').attr('data')).show(); 


// $('.directory-entry-lists').click(function(e){
//   e.preventDefault();
//   $("#views-exposed-form-directorydirectorysample-page-1 #edit-name").val("");
//   $("#views-exposed-form-directorydirectorysample-page-1 #edit-p-code").val("");
//   $("#views-exposed-form-directorydirectorysample-page-1 #edit-city").val("");
//   $('#edit-field-shipping-state-value option:first').attr("selected",true);
//   $('#edit-field-distance-value option:first').attr("selected",true);
//   $("#edit-field-services-checkbox input").removeAttr('checked');
//   $('.directory-entry-lists').removeClass('active');
//   $(this).addClass('active');
//   $('.views-exposed-widget').hide();
//   $($(this).find('a').attr('data')).show(); 
//   id_new = $(this).attr('id');
//   // console.log(id_new+'inside click');
//   // $("#edit-submit-directorydirectorysample").trigger("click");
// });

// $('#edit-submit-directorydirectorysample').click(function(e){e.preventDefault();
//   // console.log(id_new);
//   var complete_url = $("form").serialize();
//   index  = complete_url.indexOf('&name=');
//   url = '?'+complete_url.substr(index);
//  clicked_id = $("li.directory-entry-lists.active").attr('id'); 
//    document.location = url+'&clicked='+clicked_id;
// });

// // $('#edit-field-shipping-state-value').on('change', function() {
// // codeAddress();
// // });


//       $('#findmember-print').click(function(e){e.preventDefault();
//       	window.print();
//       });
//       $('#findmember-Back').click(function(e){e.preventDefault();
//       window.history.back();
//       // history.go(-2);
//       });
//         $(function() {
//           $(".rslides").responsiveSlides();
//           $("#edit-submit-directorydirectorysample").insertAfter(".view-directorydirectorysample .view-filters");
//           /* visit website start link add HTTP */
//           // var link_val = $("a#dir-check-link.directory-website").attr("href");
//           // if(!((link_val.search("http")) == 0)){
//           // link_val =  "http://"+link_val;
//           // $("a#dir-check-link.directory-website").attr("href",link_val); 
//           // }
//           $("a#dir-check-link.directory-website").each(function(e){
//             var link_val = $(this).attr("href");
//             if(!((link_val.search("http")) == 0)){
//             link_val =  "http://"+link_val;
//             $(this).attr("href",link_val); 
//             }
//           });
//           /*  visit website end  */
//         });
//       $(".rslides").responsiveSlides({
//         auto: true,             // Boolean: Animate automatically, true or false
//         speed: 1000,            // Integer: Speed of the transition, in milliseconds
//         timeout: 4000,          // Integer: Time between slide transitions, in milliseconds
//         // pager: true,           // Boolean: Show pager, true or false
//         nav: true,             // Boolean: Show navigation, true or false
//         // random: false,          // Boolean: Randomize the order of the slides, true or false
//         pause: true,           // Boolean: Pause on hover, true or false
//         // pauseControls: true,    // Boolean: Pause when hovering controls, true or false
//         // prevText: "Previous",   // String: Text for the "previous" button
//         // nextText: "Next",       // String: Text for the "next" button
//         // maxwidth: "",           // Integer: Max-width of the slideshow, in pixels
//         // navContainer: "",       // Selector: Where controls should be appended to, default is after the 'ul'
//         // manualControls: "",     // Selector: Declare custom pager navigation
//         // namespace: "rslides",   // String: Change the default namespace used
//         // before: function(){},   // Function: Before callback
//         // after: function(){}     // Function: After callback
//       });
//     }
//   }
// })(jQuery);
