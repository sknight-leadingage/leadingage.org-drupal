<?php

class ExampleFacetapiSelect extends FacetapiWidget {
   /**
   * Renders the form.
   */
  public function execute() {
    $element = &$this->build[$this->facet['field alias']];
    $name = explode(':', $this->build['#settings']->name);
    $name = array_pop($name);
    $element = drupal_get_form('example_facetapi_select_' .  $name, $element);
  }
}


/**
 * Generate form for facet.
 */
function example_facetapi_select($form, &$form_state, $elements) {

  // Build options from facet elements.
  $options = array('' => t('TYPE'));
  if( 'field_news_topics' === explode(":",array_values($form_state['build_info']['args'][0])[0]['#query']['f'][0])[0] ){
    $options = array('' => t('TOPIC'));
  }else if( 'field_news_areas' === explode(":",array_values($form_state['build_info']['args'][0])[0]['#query']['f'][0])[0] ){
    $options = array('' => t('AREA'));
  }
  foreach ($elements as $element) {
    if ($element['#active']) {
      continue;
    }
    $options[serialize($element['#query'])] = $element['#markup'] ;
  }
  if (count($options) == 1) {
    return;
  }
  $form['select'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#attributes' => array('class' => array('btn -main form-select ctools-auto-submit')),
    'default_value' => '',
    '#prefix' => '<div class="fieldset-wrapper"><nav class="dropdown -hover"><div class="form-item form-type-select form-item-type">',
    '#suffix' => '</div></nav></div>',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
    '#attributes' => array('class' => array('ctools-use-ajax', 'ctools-auto-submit-click')),
    '#submit' => array('example_facetapi_select_submit'),
  );

  // Lets add autosubmit js functionality from ctools.
  $form['#attached']['js'][] = drupal_get_path('module', 'ctools') . '/js/auto-submit.js';
  // Add javascript that hides Filter button.
  $form['#attached']['js'][] = drupal_get_path('module', 'example') . '/js/example-hide-submit.js';

  $form['#attributes']['class'][] = 'example-select-facet';

  return $form;
}

/**
 * Submit handler for facet form.
 */
function example_facetapi_select_submit($form, &$form_state) {
  $form_state['redirect'] = array($_GET['q'], array('query' => unserialize($form_state['values']['select'])));
}

function example_facetapi_title($variables) {
  return t('dont Filter by @title:', array('@title' => drupal_strtolower($variables['title'])));
}
