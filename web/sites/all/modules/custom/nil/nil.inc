<?php
/**
 * @file
 * Nil base class (required by the Zero theme).
 */

namespace Nil;

/**
 * Theme Class.
 */
class Theme
{

  /**
   * CMP Sorting function.
   */
    private static function cmp($a, $b)
    {
        if ( isset( $a['weight'] ) && isset( $b['weight'] ) ) {
            if ($a['weight'] === $b['weight']) {
                return 0;
            }
            return ($a['weight'] < $b['weight']) ? -1 : 1;
         } else {
            return 0;
        }
    }

  /**
   * Get menu.
   */
    public static function getMenu( $name )
    {
        $menu = menu_load_links( $name );
        $links = array();
        $children = array();

        foreach ($menu as $link) {
            $path = $link['link_path'];
            $id = $link['mlid'];
            $pid = $link['plid'];
            $title = $link['link_title'];
            $depth = $link['depth'];
            $has_children = $link['has_children'];
            $weight = $link['weight'];
            $nid = '';

            if ($path === '<front>' || $path === '') {
                $front_url = variable_get('site_frontpage', 'node');
                $front_url = drupal_get_normal_path($front_url);
                $nid = $front_url;
                $path = '/';
            } else {
                $alias = drupal_get_path_alias($path);
                $nid = $path;
                if ( $link['external'] ){
                    $path = $alias;
                } else {
                    $path = '/' . $alias;
                }
            }

            if ($title === '') {
                $title = 'UNTITLED';
            }

            $nid_split = explode('/', $nid);
            if ($nid_split[0] === 'node' && isset($nid_split[1])) {
                $nid = $nid_split[1];
            } else {
                $nid = null;
            }

            // Create array.
            $links[$id] = array(
              'key' => $id,
              'title' => $title,
              'path' => $path,
              'weight' => $weight,
              'nid' => $nid,
            );

            // Add second level link information.
            if ($depth === '1' && $has_children == '1') {
                $links[$id]['has_children'] = '1';
                $links[$id]['children'] = array();
                $links[$id]['below'] = array();
            }
            if ($depth !== '1') {
                $links[$id]['depth'] = $depth;
                $children[$id] = $pid;
            }
        }

        foreach ($children as $id => $pid) {
            $links[$pid]['children'][] = array(
              'id' => (string) $id,
              'pid' => (string) $pid,
            );
        }

        foreach ($links as $key => $value) {
            if (isset($links[$key]['children'])) {
                $child_ids = $links[$key]['children'];
                foreach ($child_ids as $child_id) {
                    $child_id = $child_id['id'];
                    $links[$key]['below'][] = $links[$child_id];
                }
            }
        }

        foreach ($links as $key => $value) {
            if (isset($links[$key]['depth'])) {
                unset($links[$key]);
            }
        }
        usort($links, array("Nil\Theme", "cmp"));
        $output = array(
          'links' => $links,
        );
        return $output;
    }

  /**
   * Get menus.
   */
    public static function getMenus( $names )
    {
        $menu = array();
        foreach ( $names as $key => $name ) {
            $menu[$name] = Theme::getMenu( 'menu-' . $name );
            $menu[$name]['title'] = ucwords( $name );
        }
        return $menu;
    }

    /**
     * Get Secondary menu.
     */
      public static function getSecondaryMenu( $name, $mlid )
      {
          $menu = menu_load_links( $name );
          $links = $secondary_menu = $children = array();
          foreach ( $menu as $link ) {
              if ( $mlid === $link['p1'] ){
                array_push( $secondary_menu, $link );
              }
          }
          foreach ($secondary_menu as $link) {
              $path = $link['link_path'];
              $id = $link['mlid'];
              $pid = $link['plid'];
              $title = $link['link_title'];
              $depth = $link['depth'];
              $has_children = $link['has_children'];
              $weight = $link['weight'];
              $nid = '';

              if ($path === '<front>' || $path === '') {
                  $front_url = variable_get('site_frontpage', 'node');
                  $front_url = drupal_get_normal_path($front_url);
                  $nid = $front_url;
                  $path = '/';
              } else {
                  $alias = drupal_get_path_alias($path);
                  $nid = $path;
                  if ( $link['external'] ){
                      $path = $alias;
                  } else {
                      $path = '/' . $alias;
                  }
              }

              if ($title === '') {
                  $title = 'UNTITLED';
              }

              $nid_split = explode('/', $nid);
              if ($nid_split[0] === 'node' && isset($nid_split[1])) {
                  $nid = $nid_split[1];
              } else {
                  $nid = null;
              }

              // Create array.
              $links[$id] = array(
                'key' => $id,
                'title' => $title,
                'path' => $path,
                'weight' => $weight,
                'nid' => $nid,
              );

              // Add second level link information.
              if ($depth === '1' && $has_children == '1') {
                  $links[$id]['has_children'] = '1';
                  $links[$id]['children'] = array();
                  $links[$id]['below'] = array();
              }
              if ($depth !== '1') {
                  $links[$id]['depth'] = $depth;
                  $children[$id] = $pid;
              }
          }

          foreach ($children as $id => $pid) {
              $links[$pid]['children'][] = array(
                'id' => (string) $id,
                'pid' => (string) $pid,
              );
          }

          foreach ($links as $key => $value) {
              if (isset($links[$key]['children'])) {
                  $child_ids = $links[$key]['children'];
                  foreach ($child_ids as $child_id) {
                      $child_id = $child_id['id'];
                      $links[$key]['below'][] = $links[$child_id];
                  }
              }
          }
          // foreach ($links as $key => $value) {
          //     if (isset($links[$key]['depth'])) {
          //         unset($links[$key]);
          //     }
          // }
          usort($links, array("Nil\Theme", "cmp"));
          $output = array(
            'links' => $links,
          );
          return $output;
      }

    /**
     * Get Secondary menus.
     */
      public static function getSecondaryMenus($names, $mlid)
      {
          $menu = array();
          foreach ($names as $key => $name) {
              $menu[$name] = Theme::getSecondaryMenu( $name, $mlid );
              $menu[$name]["title"] = ucwords($name);
          }
          return $menu;
      }

    /**
     * Get filter URLs.
     */
    public static function getFilterUrl( $term_id, $param_name )
    {
        $param_name = str_replace( '_', '-', $param_name );
        $query_params = drupal_get_query_parameters();
        $orig_path = request_path();
        $i = 0;
        $query_string = "";
        $replacing = false;

        foreach ( $query_params as $key => $value ) {
            if ( 'p' === $key ) {
                continue;
            }
            if ( $param_name !== $key ) {
                // Existing.
                $query_string .= ($i === 0) ? "?" : "&";
                $query_string .= $key . "=" . $value;
            } else {
                // Replacing.
                if ( $term_name !== 'all' ) {
                    $query_string .= ($i === 0) ? "?" : "&";
                    $query_string .= $param_name . "=" . $term_id;
                    $replacing = true;
                } else {
                    $i--;
                }
            }
            $i++;
        }

        if ( empty( $query_params ) ) {
            if ( $term_name === "all" ) {
                return "/" . $orig_path;
            }
            $url = "/" . $orig_path . "?" . $param_name . "=" . $term_id;
        } else {
            if ( $term_name === "all" ) {
                return "/" . $orig_path . $query_string;
            }
            // Add new one.
            if ( ! $replacing ) {
                $query_string .= ($i === 0) ? "?" : "&";
                $query_string .= $param_name . "=" . $term_id;
            }
            $url = "/" . $orig_path . $query_string;
        }
        return $url;
    }

    /**
     * Form Newsroom filter URLs.
     */
    public static function getNewsroomFilterUrl($term_name, $param_name)
    {
        $machine_name = str_replace(' ', '-', strtolower($term_name));
        $query_params = drupal_get_query_parameters();
        $orig_path = request_path();
        $i = 0;
        $query_string = "";
        $replacing = false;

        foreach ($query_params as $key => $value) {
            if ($key === "p") {
                continue;
            }
            if ($param_name !== $key) {
              // Existing.
                $query_string .= ($i === 0) ? "?" : "&";
                $query_string .= $key . "=" . $value;
            } else {
              // Replacing.
                if ($term_name !== "all") {
                    $query_string .= ($i === 0) ? "?" : "&";
                    $query_string .= $param_name . "=" . $machine_name;
                    $replacing = true;
                } else {
                    $i--;
                }
            }
            $i++;
        }

        if (empty($query_params)) {
            if ($term_name === "all") {
                return "/" . $orig_path;
            }
            $url = "/" . $orig_path . "?" . $param_name . "=" . $machine_name;
        } else {
            if ($term_name === "all") {
                return "/" . $orig_path . $query_string;
            }
            // Add new one.
            if (!$replacing) {
                $query_string .= ($i === 0) ? "?" : "&";
                $query_string .= $param_name . "=" . $machine_name;
            }
            $url = "/" . $orig_path . $query_string;
        }
        return $url;
    }

    /**
     * Return human readable stirng or a provided string.
     */
    public static function humanReadable($machine_name)
    {
        return ucwords(str_replace('-', ' ', $machine_name));
    }
}

/**
 * Get more out of entities.
 */
class Entity
{
    private $wrapper;
    private $info;

    /**
     * Get all field values for an entity.
     *
     * @param object $entity
     *   Entity Object (eg. node).
     * @param string $bundle
     *   Bundle (eg. content type).
     * @param string $entity_type
     *   Type of entity (eg. "node").
     */
    public function __construct($entity, $bundle, $entity_type = "node")
    {
        $this->wrapper = entity_metadata_wrapper($entity_type, $entity, array('bundle' => $bundle));
        $this->info = $this->wrapper->getPropertyInfo();
    }

  /**
   * Generate a fields array.
   */
    public function getFieldsArray($wrapper = null, $info = null)
    {
        $info = !is_null($info) ? $info : $this->info;
        $wrapper = !is_null($wrapper) ? $wrapper : $this->wrapper;
        $out = array();
        foreach ($info as $key => $value) {

            $info = $wrapper->$key->info();
            $type = $info["type"];
            if ($type === "list<field_collection_item>") {
                $out[$key] = $this->processLists($wrapper->$key);
            } elseif ($type === "field_item_viewfield") {
                $viewfield = $wrapper->$key->value();
                $out[$key] = !empty($viewfield) ? new View($viewfield["vname"], $viewfield["vargs"]) : null;
            } elseif ($type === "node") {
              // Return a EntityDrupalWrapper to do whatever with.
                $out[$key] = $wrapper->$key;
            } else {
                $out[$key] = $wrapper->$key->value();
            }
        }
        
        return $out;
    }

    /**
     * Return the wrapper.
     */
    public function getWrapper()
    {
        return $this->wrapper;
    }

    /**
     * Process fields with multiple values.
     */
    private function processLists($entity_list_wrapper)
    {
        $out = array();
        // Get how many values we are dealing with here.
        $count = $entity_list_wrapper->count();
        // Look through all items to get each item.
        for ($i = 0; $i < $count; $i++) {
          // Wrapper is an EntityDrupalWrapper.
            $wrapper = $entity_list_wrapper->get($i);
            $info = $wrapper->getPropertyInfo();
            $out[$i] = $this->getFieldsArray($wrapper, $info);
        }
        return $out;
    }
}

/**
 * Get more out of views.
 */
class View
{
    private $name;
    private $disp;
    private $args;
    private $isPaginated;
    private $totalRows;
    private $itemsPerPage;
    private $currentPage;

    /**
     * Constructor.
     */
    public function __construct($name, $args = null)
    {   

        $name_parts = explode("|", $name);
        $this->name = $name_parts[0];
        $this->disp = $name_parts[1];
        if (is_string($args)) {
            $this->args = explode(",", $args);
        } elseif (!is_null($args)) {
            $this->args = explode(",", $args);
        } else {
            $this->args = null;
        }
    }

    /**
     * Name getter.
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Args getter.
     */
    public function getArgs()
    {
        return $this->args;
    }

    /**
     * Modify Args.
     */
    public function modArgs($args)
    {
        foreach ($args as $pos => $arg) {
            if (!is_null($arg)) {
                $this->args[$pos] = $arg;
            } elseif (!isset($this->args[$pos])) {
                $this->args[$pos] = "all";
            }
        }
    }

    /**
     * Display getter.
     */
    public function getDisp()
    {
        return $this->disp;
    }

    /**
     * Getter for all view properties.
     */
    public function getViewInfo()
    {
        return array(
          "name" => $this->name,
          "disp" => $this->disp,
          "isPaginated" => $this->isPaginated,
          "totalRows" => $this->totalRows,
          "itemsPerPage" => $this->itemsPerPage,
          "currentPage" => $this->currentPage,
        );
    }

    /**
     * Get pagination items value => label array.
     */
    public function getPaginationItems($current_page)
    {
      // Get items depending on what the current page is...
        $out = array();
        $pitem_qty = ceil( $this->totalRows / $this->itemsPerPage );
        if ( $this->totalRows > 0 && $this->totalRows % $pitem_qty > 0 ){
            $remainder = ($this->totalRows - $this->itemsPerPage * $pitem_qty);
        } else {
            $remainder = 0;
        }
        for ($i = 0; $i < $pitem_qty; $i++) {
          // On last pass check final number in range.
            if( $i + 1 == $pitem_qty && $remainder !== 0 ){
                $endNumber = (($i * $this->itemsPerPage) + $this->itemsPerPage) + $remainder;
            }else{
                $endNumber = (($i * $this->itemsPerPage) + $this->itemsPerPage);
            }
            $out[$i] = sprintf('%02d', (($i * $this->itemsPerPage) + 1)) . " - " . sprintf('%02d', $endNumber);
        }
        return $out;
    }

    /**
     * Get a view.
     *
     * @param int $items
     *   Number of items to display per page.
     *
     * @return array
     *   A usable view.
     */
    public function getView($items = null, $current_page = null, $args_mod = null, $offset = 0)
    {
        $out = array();
        $name = $this->name;
        $disp = $this->disp;
        $args = is_null($args_mod) ? $this->args : $args_mod;
        $view = views_get_view($name);
        $test = views_get_view('news_topic');
        if (!$view) {
            return $out;
        }

        $view_pager = isset($view->display[$disp]->display_options['pager']) ? $view->display[$disp]->display_options['pager'] : $view->display["default"]->display_options['pager'];
        if ($view_pager['type'] !== 'none' && !$items) {
            $items = $view_pager['options']['items_per_page'];
        }
        $view->set_display($disp);
        $view->set_offset($offset);
        // Pass arguments.
        $view->set_arguments(array(12));
        if (isset($args)) {
            $view->set_arguments($args);
        }
        // Change the amount of items to show.
        if (isset($items)) {
            $view->set_items_per_page($items);
        }
        // Set the current page.
        if (isset($current_page)) {
            $view->set_current_page($current_page);
        }
        $view->pre_execute();
        $view->execute();
        $out = $view->result;
        // Set pagination information.
        $this->isPaginated = $view_pager["type"] !== "none";
        $this->totalRows = $view->total_rows;
        $this->itemsPerPage = $items;
        $this->currentPage = !is_null($current_page) ? $current_page : 0;
        return $out;
    }
}
