function myfunction() {
window.print();
}
jQuery(function($) {
    $(window).on('load',function(){
      var form_no;
      var url_path = location.pathname;
      // console.log(url_path);
      var term_no = url_path.split("/");
      term_no = term_no[term_no.length-1];
      //console.log("form no "+form_no);
      // if(term_no == 297 )
      if (url_path == '/cast-ehr-selection-tool')
        form_no = 31;
     else if (url_path == '/cast-medication-management-selection-tool')
        form_no = 37;
     else if (url_path == '/cast-shared-care-planning-and-coordination-online-selection-tool') {
        form_no = 154;
        $('body').addClass("shared-page");
      }
      else if (url_path == '/cast-telehealth-and-rpm-selection-tool') {
        form_no = 24;
        $('body').addClass("ehr-page");
      }
      else if (url_path == '/cast-functional-assessment-and-activity-monitoring-online-selection-tool')
          form_no = 110;
      else if (url_path == '/cast-safety-technology-online-selection-tool')
          form_no = 318;
      else if (url_path == '/cast-hie-selection-tool')
          form_no = 376;
      else if (url_path == '/cast-social-connectedness-and-engagement-technology-selection-tool')
          form_no = 184;

        $('.bef-tree-child').siblings('.form-item').find('input').css('display', 'none');
        $('<span class="fda-heading"><i>FDA Approval/Clearance/Listing (<u>select only one; if you need to change your selection, you must unselect your previous choice</u>): </i></span>').insertBefore(".form-item-edit-field-category-tid-801");
        $('<span class="fda-heading"><i>HIPAA and HITECH (if applicable):</i>').insertBefore(".form-item-edit-field-category-tid-804");
        $('<span class="sub-name-finacial">I also need the EHR/PHR to provide access to the following external parties:</span>').insertBefore(".form-item-edit-field-category-tid-775");
        $('<span class="sub-name-finacial">I also need Status Report and Access provided to the following external parties:</span>').insertBefore(".form-item-edit-field-category-tid-757");
        $('<span class="sub-name-finacial">I need a system that interfaces with these types of third-party software:</span>').insertBefore(".form-item-edit-field-category-tid-738");
        $('<p class="system-head">* Please see <a href="http://www.leadingage.org/white-papers/functional-assessment-and-activity-monitoring-technology-primer-and-provider-selection#3" target="_blank">Section 3</a> of the Whitepaper for additional information regarding System Types.</p>').insertBefore('.form-item-edit-field-category-tid-664');
        $('<span class="sub-name-finacial">I also need report access to the following external parties: </span>').insertBefore(".form-item-edit-field-category-tid-756");
        $('<span class="sub-name-finacial">I need a system that is for:</span>').insertBefore(".form-item-edit-field-category-tid-921");
        $('<span class="sub-name-finacial">I need the system to support the accessibility features for:</span>').insertBefore(".form-item-edit-field-category-tid-923");
        
        $('<span class="sub-name-finacial">I need the following social engagement modalities to be a:</span>').insertBefore(".form-item-edit-field-category-tid-839");
        $('<span class="sub-name-finacial">I need a system that is:</span>').insertBefore(".form-item-edit-field-category-tid-875");
        $('<span class="sub-name-finacial">I need a platform for the following application areas:</span>').insertBefore(".form-item-edit-field-category-tid-1063");
        $('<span class="sub-name-finacial">I need the system to provide the following Data Management and Engineering services:</span>').insertBefore(".form-item-edit-field-category-tid-1079");
        $('<span class="sub-name-finacial">If one or more of the technology options is selected (applies to both Low-Tech and High-Tech):</span>').insertBefore(".form-item-edit-field-category-tid-1123");
        var relValueHolder = document.querySelectorAll(".views-exposed-widget input[type='hidden']");
        var valArray, p = 0,
            grand = [],
            selectedServices = [],
            allProducts = [],
            grandTotal = 0, relVal, relLength = 0, selectedBox, unique, uniqueServices;

        for (var i = 0; i < relValueHolder.length; i++) {
            var strValue = $(relValueHolder[i]).attr("value");
            valArray = strValue.split("|");
            for (var j = 0; j < valArray.length; j++) {
                if ((valArray[j] == "")) {
                    continue;
                } else if ((valArray[j] == 0)) {
                    continue;
                } else {
                    grand[p] = valArray[j];
                    p++;
                }
            }
        }
        
        /*
        if (isMyGood()) {
          $('#edit-field-category-tid-wrapper').show();
          $('#edit-submit-taxonomy').show();
        }
        else {
          $('#edit-field-category-tid-wrapper').hide();
          $('#edit-submit-taxonomy').hide();
        }
        */

        var unique = grand.filter(function(itm, i, grand) {
            return i == grand.indexOf(itm);
        });
        var grandTotal = unique.length;

        //$(".bef-tree-child input.form-checkboxes").change(function() {
        //$(document).on('change', '.bef-tree-child input.form-checkboxes', function() {
        $(document).delegate('.bef-tree-child input.form-checkboxes', 'change', function() {
            var checkedTotal = $('.bef-tree-child input.form-checkboxes:checked').length;
            $("div.clickedVal").remove();
            //var legal_option = ['edit-field-category-tid-397','edit-field-category-tid-398','edit-field-category-tid-399']
            selectedBox = $(this).attr("id");
            for (i = 0; i < relValueHolder.length; i++) {
                if (($(relValueHolder[i]).attr("name")) == selectedBox) {
                    relVal = $(relValueHolder[i]).attr("value");
                    relVal = relVal.split("|");
                }
            }
            var removeItem = "";
            relVal = jQuery.grep(relVal, function(value) {
                return value != removeItem;
            });
            //console.log(relVal);
            relLength = relVal.length;
            if ($(this).is(":checked")) {
              if(selectedBox == "edit-field-category-tid-801"){
                      $("input#edit-field-category-tid-802").attr("disabled", true);
                      $("input#edit-field-category-tid-803").attr("disabled", true);
              }
              else if(selectedBox == "edit-field-category-tid-802"){
                  //console.log("if");
                  $("input#edit-field-category-tid-801").attr("disabled", "disabled");
                  $("input#edit-field-category-tid-803").attr("disabled", "disabled");
              }
              else{
                if(selectedBox == "edit-field-category-tid-803"){
                  $("input#edit-field-category-tid-801").attr("disabled", true);
                  $("input#edit-field-category-tid-802").attr("disabled", true);
                }
              }
              allProducts = allProducts.concat(relVal);
              if (checkedTotal == 1) {
                if(relVal[0] == 0)
                  uniqueServices = 0;
                else
                  uniqueServices = relLength;
              } else {
                  checkDuplicates();
              }
            } else {
                if(selectedBox == "edit-field-category-tid-801"){
                  $("input#edit-field-category-tid-802").removeAttr("disabled");
                  $("input#edit-field-category-tid-803").removeAttr("disabled");
                }
                else if(selectedBox == "edit-field-category-tid-802"){
                  //console.log("if");
                  $("input#edit-field-category-tid-801").removeAttr("disabled");
                  $("input#edit-field-category-tid-803").removeAttr("disabled");
                }
                else{
                  if(selectedBox == "edit-field-category-tid-803"){
                    $("input#edit-field-category-tid-801").removeAttr("disabled");
                    $("input#edit-field-category-tid-802").removeAttr("disabled");
                  }
                }

                if (checkedTotal < 1) {
                    uniqueServices = grandTotal;
                    allProducts = [];
                    //relVal = [];
                }
                else {
                    for (i = 0; i < relVal.length; i++) {
                        for (j = 0; j < allProducts.length; j++) {
                            if (allProducts[j] == relVal[i]) {
                                allProducts.splice(j, 1);
                                break;
                            }
                        }
                    }
                    if (checkedTotal == 1) {
                        uniqueServices = allProducts.length;
                    } else {
                        checkDuplicates();
                    }
                }

            }

            function checkDuplicates() {
                var count = 0;
                selectedServices = [];
                for (i = 0; i < allProducts.length; i++) {
                    for (j = i + 1; j < allProducts.length; j++) {
                        if (allProducts[i] == allProducts[j]) {
                            count++;
                            if (checkedTotal - 1 == count) {
                                selectedServices.push(allProducts[i]);
                            }
                        }
                    }
                  count = 0;
                }
                var unique = selectedServices.filter(function(itm, i, selectedServices) {
                    return i == selectedServices.indexOf(itm);
                });
                uniqueServices = unique.length;
            }

            if (uniqueServices < 1) {
                $(this).parents(".bef-tree-child").siblings(".form-type-bef-checkbox").find(".sub-names").before('<div class="clickedVal">Sorry, no results match your criteria. Please consider removing less important criteria to increase your results.</div>');
            } else if (uniqueServices == 1) {
                $(this).parents(".bef-tree-child").siblings(".form-type-bef-checkbox").find(".sub-names").before('<div class="clickedVal">There is ' + uniqueServices + ' product that matches your criteria.</div>');
            } else {
                $(this).parents(".bef-tree-child").siblings(".form-type-bef-checkbox").find(".sub-names").before('<div class="clickedVal">There are ' + uniqueServices + ' products that match your criteria.</div>');
            }

        });

        // $('.bef-tree-child').siblings('.form-item').find('input').css('display', 'none');

        $(document).delegate(".empty > span", "click", function() {
            $(".view-id-Taxonomy .print-button").css("display", "none");
            $("#edit-submit-taxonomy").css("display", "inherit");
            $(".view-id-Taxonomy .view-content").css("display", "none");
            $(".view-display-id-page_1 .view-footer .tool-section-footer").css("display","none");
            $("#edit-reset").css("display", "none");
            $(".empty").css("display", "none");
            $(".empty").toggleClass("collapsed");
            $("[id*='edit-field-category-tid-wrapper']").slideToggle();
            $('.bef-tree-child').siblings('.form-item').find('input').css('display', 'none');
            $(".view-display-id-page_1 .view-header .tool-header-page").css("display","inherit");
            $(".view-display-id-page_1 .view-header .tools-header-result-text").css("display","none");
            //console.log("uniqueServices:"+uniqueServices);
            if (uniqueServices < 1) {
                $("#" + selectedBox).parents(".bef-tree-child").prev().find(".sub-names").before('<div class="clickedVal">Sorry, no results match your criteria. Please consider removing less important criteria to increase your results.</div>');
            } else if (uniqueServices == 1) {
                $("#" + selectedBox).parents(".bef-tree-child").prev().find(".sub-names").before('<div class="clickedVal">There is ' + uniqueServices + ' product that matches your criteria.</div>');
            } else {

                $("#" + selectedBox).parents(".bef-tree-child").prev().find(".sub-names").before('<div class="clickedVal">There are ' + uniqueServices + ' products that match your criteria.</div>');
            }

        });

        $(document).ajaxComplete(function() {
         // alert("ajaxComplete");
            $("#edit-submit-taxonomy").css("display", "none");
            $(".view-id-Taxonomy .print-button").css("display", "block");
            $("#edit-reset").css("display", "inherit");
            $(".view-id-Taxonomy .view-content").css("display", "inherit");
            $(".empty").css("display", "inherit");
            $(".empty").insertBefore("#edit-field-category-tid-wrapper");
            $(".empty").addClass("collapsed");
            $("[id*='edit-field-category-tid-wrapper']").slideUp();
            $(".view-display-id-page_1 .view-footer .tool-section-footer").css("display","inherit");
            $(".view-display-id-page_1 .view-header .tools-header-result-text").css("display","inherit");
            $(".view-display-id-page_1 .view-header .tool-header-page").css("display","none");
            $(".view-display-id-page_1 .view-footer").css("display","inherit");

             $('<span class="fda-heading"><i>FDA Approval/Clearance/Listing (<u>select only one; if you need to change your selection, you must unselect your previous choice</u>): </i></span>').insertBefore(".form-item-edit-field-category-tid-801");
        $('<span class="fda-heading"><i>HIPAA and HITECH:</i>').insertBefore(".form-item-edit-field-category-tid-804");
        $('<span class="sub-name-finacial">I also need the EHR/PHR to provide access to the following external parties:</span>').insertBefore(".form-item-edit-field-category-tid-775");
        $('<span class="sub-name-finacial">I also need Status Report and Access provided to the following external parties:</span>').insertBefore(".form-item-edit-field-category-tid-757");
        $('<span class="sub-name-finacial">I need a system that interfaces with these types of third-party software:</span>').insertBefore(".form-item-edit-field-category-tid-738");
        $('<p class="system-head">* Please see <a href="http://www.leadingage.org/white-papers/functional-assessment-and-activity-monitoring-technology-primer-and-provider-selection#3" target="_blank">Section 3</a> of the Whitepaper for additional information regarding System Types.</p>').insertBefore('.form-item-edit-field-category-tid-664');
        $('<span class="sub-name-finacial">I also need report access to the following external parties: </span>').insertBefore(".form-item-edit-field-category-tid-756");
            runAfterReady();
        });

        function c(a) {
            var d = [],
            e = document.cookie.split(";");
            //console.log("e value:  "+e);
            a = RegExp("^\\s*" + a + "=\\s*(.*?)\\s*$");
            //console.log("a"+a);
            for (var b = 0; b < e.length; b++) {
                var f = e[b].match(a);
                f && d.push(f[1]);
            }
            return d;
        }

        function isMyGood() {
            // console.log("is my good");
            var cookiea = c("Attributes");
            var cookieb = c("SFID");
            if ((cookiea != null && cookiea != '' && $.type(cookiea) != 'undefined' && cookieb != null && cookieb != '' && $.type(cookieb) != 'undefined')) {
            //if($.type(cookiea) == 'array' && $.type(cookieb) == 'array'){
            //console.log("cookie true");
                return true;
            } else {
           // console.log("cookie false");
                return false;
            }
        }

        function isSearchComplete() {
            // console.log("is search complete");
            var searchResults = document.getElementsByClassName('view-header');
            if (typeof searchResults != 'undefined' && searchResults != null && searchResults != "undefined") {
              //  console.log("true");
                return true;

            } else {
             //   console.log("false");
                return false;

            }
        }

        function shouldLoadPopup() {
            // console.log("should load popup");
            //var ca = $.cookie;
            //console.log("ca value "+ $.cookie("markoehr"));
            // console.log("Term no" + term_no);
            
            var retVal = { 'loadPopup' : false, 'formid' : 0 };
            
            if (isSearchComplete() && $.cookie("seltoolehr") == null && !isMyGood() && url_path == '/cast-ehr-selection-tool') {
                retVal.loadPopup = true;
                retVal.formid = 'seltoolehr';
            }
            else if (isSearchComplete() && $.cookie("seltoolmed") == null && !isMyGood() && url_path == '/cast-medication-management-selection-tool') {
                retVal.loadPopup = true;
                retVal.formid = 'seltoolmed';
            }
            else if (isSearchComplete() && $.cookie("seltoolshared") == null && !isMyGood() && url_path == '/cast-shared-care-planning-and-coordination-online-selection-tool') {
                retVal.loadPopup = true;
                retVal.formid = 'seltoolshared';
            }
            else if (isSearchComplete() && $.cookie("seltooltel") == null && !isMyGood() && url_path == '/cast-telehealth-and-rpm-selection-tool') {
                retVal.loadPopup = true;
                retVal.formid = 'seltooltel';
            }
            else if (isSearchComplete() && $.cookie("seltoolfunc") == null && !isMyGood() && url_path == '/cast-functional-assessment-and-activity-monitoring-online-selection-tool') {
                retVal.loadPopup = true;
                retVal.formid = 'seltoolfunc';
            }
            else if (isSearchComplete() && $.cookie("seltoolsafety") == null && !isMyGood() && url_path == '/cast-safety-technology-online-selection-tool') {
                retVal.loadPopup = true;
                retVal.formid = 'seltoolsafety';
            }
            else if (isSearchComplete() && $.cookie("seltoolhie") == null && !isMyGood() && url_path == '/cast-hie-selection-tool') {
                retVal.loadPopup = true;
                retVal.formid = 'seltoolhie';
            }
            else if (isSearchComplete() && $.cookie("seltoolsc") == null && !isMyGood() && url_path == '/cast-social-connectedness-and-engagement-technology-selection-tool') {
                retVal.loadPopup = true;
                retVal.formid = 'seltoolsc';
            }
            else if (isSearchComplete() && $.cookie("seltooldata") == null && !isMyGood() && url_path == '/cast-data-analytics-selection-tool') {
                retVal.loadPopup = true;
                retVal.formid = 'seltooldata';
            }

            return retVal;
        }

        function runAfterReady() {
            // console.log("run after ready");
            var popCtrl = shouldLoadPopup();
            if (popCtrl.loadPopup == true) {
                var h = document.createElement("div");
                h.setAttribute('id', "access-pop-mask-amnr4");
                document.body.appendChild(h);
                
                var maskHeight = $(document).height();
                var maskWidth = $(window).width();
                $('#access-pop-mask-amnr4').css({'width':maskWidth,'height':maskHeight}).show();
            
                var ef = document.createElement("div");
                ef.setAttribute('id', "access-pop-amnr4-around");
                document.body.appendChild(ef);
            
                var f = document.createElement("div");
                f.setAttribute('id', "access-pop-amnr4");
                ef.appendChild(f);
                
                var g = document.createElement("iframe");
                g.setAttribute('id', "access-pop-iframe");
                g.setAttribute('type', "text/html");
                g.src = '//information.leadingage.org/l/874291/2021-02-20/mk6r6?Link_to_use=' + popCtrl.formid;
                g.width = 700;
                g.height = 500;
                g.frameBorder = 0;
                g.allowTransparency = true;
                g.style = 'border: 0;';
                f.appendChild(g);
                
                var winH = $(window).height();
                var winW = $(window).width();
                $("#access-pop-amnr4-around").css('top',  winH/2-$("#access-pop-amnr4").height()/2);
                $("#access-pop-amnr4-around").css('left', winW/2-$("#access-pop-amnr4").width()/2);
                
                $("#access-pop-amnr4").show();
            }
        }

        $(document).delegate('.view-id-Taxonomy .views-reset-button .form-submit', 'click', function(event) {
            window.location = window.location.href.split('?')[0];
            //$("[id*='edit-field-category-tid-wrapper']").slideDown();
            return false;
        });

    });
});
