<?php
/**

# If following line is added at the end of query server will return XML that can be saved as file.

 FOR XML PATH('Article'), ROOT ('Articles'), ELEMENTS

 *
 *
## Get all NEWS like taxonomy terms from Ekton ##

USE Ektron_From_RDS

	SELECT  DISTINCT taxonomy_tbl.taxonomy_id, taxonomy_tbl.taxonomy_name
      FROM [content] AS cont
	   JOIN [taxonomy_item_tbl] ON cont.content_id = [taxonomy_item_tbl].taxonomy_item_id
	   JOIN [taxonomy_tbl] ON [taxonomy_tbl].taxonomy_id =  [taxonomy_item_tbl].taxonomy_id
	   WHERE taxonomy_tbl.taxonomy_name LIKE '%news%'

## Get all News like articles from Ekton ##

 * for individual term use full term name example: LIKE '﻿News and Events'

﻿USE [Ektron_From_RDS]

SELECT   [content_id], [content_title], [content_html], [date_created]
FROM [content] AS cont
WHERE [content_id] IN
(
	SELECT  DISTINCT [content_id]
      FROM [content] AS cont
	   JOIN [taxonomy_item_tbl] ON cont.content_id = [taxonomy_item_tbl].taxonomy_item_id
	   WHERE cont.[template_id] = 36 AND [content_html] LIKE '<root><Article>%' AND taxonomy_item_tbl.taxonomy_id IN
	   (
	   	SELECT  DISTINCT taxonomy_tbl.taxonomy_id
		FROM [taxonomy_tbl]
		WHERE taxonomy_tbl.taxonomy_name LIKE '%news%'
	   )
)
ORDER BY content_id


## Get all Ads from Ektron ##


﻿USE Ektron_From_RDS

SELECT [content_id]
	  ,[content_title]
      ,[content_html]
      ,[date_created]
FROM [content]
WHERE [content_html] LIKE '<root><Ad><link>http%'
ORDER BY [content_id]

FOR XML PATH('Article'), ROOT ('Articles'), ELEMENTS


## GET all Magazine articles ##

﻿USE [Ektron_From_RDS]

SELECT   [content_id], [content_title], [content_html], [date_created]
FROM [content] AS cont
WHERE [content_id] IN
(
	SELECT  DISTINCT [content_id]
      FROM [content] AS cont
	   JOIN [taxonomy_item_tbl] ON cont.content_id = [taxonomy_item_tbl].taxonomy_item_id
	   JOIN [taxonomy_tbl] ON [taxonomy_tbl].taxonomy_id =  [taxonomy_item_tbl].taxonomy_id
	   WHERE taxonomy_parent_id = 5026 AND cont.[template_id] = 46 AND content_html LIKE '<root><title>%'
)
ORDER BY content_id
FOR XML PATH('Article'), ROOT ('Articles'), ELEMENTS


## GET all Magazine Issues ##

﻿USE Ektron_From_RDS
SELECT  cont.[content_id], cont.[content_title], cont.[content_html], cont.[date_created]
FROM [content] AS cont
WHERE cont.[content_id] IN
(
	SELECT  DISTINCT cont2.[content_id]
      FROM [content] AS cont2
	   JOIN [taxonomy_item_tbl] ON cont2.content_id = [taxonomy_item_tbl].taxonomy_item_id
	   JOIN [taxonomy_tbl] ON [taxonomy_tbl].taxonomy_id =  [taxonomy_item_tbl].taxonomy_id

	   WHERE cont2.[content_html] LIKE '<root><Issue>%' AND cont2.[template_id] LIKE '47' AND [taxonomy_tbl].taxonomy_parent_id = 5026
)
ORDER BY cont.content_id
FOR XML PATH('Article'), ROOT ('Articles'), ELEMENTS


*/